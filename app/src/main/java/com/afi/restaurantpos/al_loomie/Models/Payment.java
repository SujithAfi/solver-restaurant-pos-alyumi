package com.afi.restaurantpos.al_loomie.Models;

/**
 * Created by AFI on 10/22/2015.
 */
public class Payment {

    public String Cm_Collected_Counter;
    public String Cm_Bc_Amt;
    public String Cm_Counter;
    public String Cm_No;
    public String Cm_Collected_User;
    public String Collect_Sft_No;
    public String Collect_Sft_Dt;
    public String Cus_Cd;

}

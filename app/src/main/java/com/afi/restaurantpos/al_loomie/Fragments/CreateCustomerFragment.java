package com.afi.restaurantpos.al_loomie.Fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.CustomerSelectionListAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.SelectCustomerDialogFragment;
import com.afi.restaurantpos.al_loomie.Models.CreditAccount;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class CreateCustomerFragment extends Fragment implements CustomerSelectionListAdapter.CustomerSelectionListner, View.OnClickListener {


    private static final String[] impDateTypes = new String[]{"BirthDay", "Marriage Aniversary"};
    private int addDate = 1;
    private int add_Date2 = 2;


    private TextInputEditText edtTxtCustomerCode;
    private TextInputEditText edtTxtCustomerName;
    private TextInputEditText edtTxtCustomerAddress;
    private TextInputEditText edtTxtDiscount;
    private TextInputEditText edtTxtAllergicInformation;
    private TextInputEditText edtTxtHabit;



    private CheckBox addMoreDretails;

    private AppCompatSpinner dateTypeSpinner1;
    private AppCompatSpinner dateTypeSpinner2;
    private AppCompatSpinner dateTypeSpinner3;

    private TextView impDate1;
    private TextView impDate2;
    private TextView impDate3;

    private TextInputEditText tvFav1;
    private TextInputEditText tvFav2;
    private TextInputEditText tvFav3;

    private RadioGroup radioPaymentMode;
    private RadioButton radioCash;
    private RadioButton radioCredit;

    private AppCompatSpinner creditAccountName;
    private EditText creditRoomNumber;
    private TextView creditCheckInDate;

    private Button create;
    private Button addImpDate1;
    private Button addImpDate2;


    private Handler uiHandler = new Handler();
    private List<CreditAccount> creaditAccounts;
    private String creditAccId;
    private ArrayAdapter<String> Aadapter;
    private String oldQuery = "";

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            try {
                int action = intent.getIntExtra("action", -1);

                if (action == 0) {//Query changed
                    String query = intent.getStringExtra("query");
                    onQueryChanged(query);
                } else if (action == 1) {//has focus
                    Log.e("has focus", "has focus");
                } else if (action == 2) {//Lost focus
                    Log.e("Lost focus", "Lost focus");
                } else if (action == 3) {//refresh list
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    };

    private TextInputEditText etCustPhone1;
    private TextInputEditText etCustPhone2;
    private EditText etEmail;


    public CreateCustomerFragment() {
        // Required empty public constructor
    }


    public static CreateCustomerFragment newInstance() {
        CreateCustomerFragment fragment = new CreateCustomerFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoadCreditAccounts();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.create_customer_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        edtTxtCustomerCode = (TextInputEditText) getView().findViewById(R.id.edtTxtCustomerCode);
        etCustPhone1 = (TextInputEditText) getView().findViewById(R.id.tiet_phone1);
        etCustPhone2 = (TextInputEditText) getView().findViewById(R.id.tiet_phone2);
        edtTxtCustomerName = (TextInputEditText) getView().findViewById(R.id.edtTxtCustomerName);
        edtTxtCustomerAddress = (TextInputEditText) getView().findViewById(R.id.edtTxtCustomerAddress);
        edtTxtAllergicInformation = (TextInputEditText) getView().findViewById(R.id.editTextAlergicInformation);
        edtTxtHabit = (TextInputEditText) getView().findViewById(R.id.editTextHabit);
        etEmail = (TextInputEditText) getView().findViewById(R.id.et_email);

        radioPaymentMode = (RadioGroup) getView().findViewById(R.id.rPaymentMode);
        radioCash = (RadioButton) getView().findViewById(R.id.rButtonCashPayment);
        radioCredit = (RadioButton) getView().findViewById(R.id.rButtonCredit);

        creditAccountName=(AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName);
        creditRoomNumber=(EditText) getView().findViewById(R.id.paymentRoomNumb);
        creditCheckInDate=(TextView)getView().findViewById(R.id.PaymentCheckIn);

        addMoreDretails = (CheckBox) getView().findViewById(R.id.checkboxSelectCreatedAccount);

        dateTypeSpinner1 = (AppCompatSpinner) getView().findViewById(R.id.dateTypSpinner1);
        dateTypeSpinner2 = (AppCompatSpinner) getView().findViewById(R.id.dateTypSpinner2);
        dateTypeSpinner3 = (AppCompatSpinner) getView().findViewById(R.id.dateTypSpinner3);

        impDate1 = (TextView) getView().findViewById(R.id.txtDate1);
        impDate2 = (TextView) getView().findViewById(R.id.txtDate2);
        impDate3 = (TextView) getView().findViewById(R.id.txtDate3);

        tvFav1 = (TextInputEditText) getView().findViewById(R.id.edtTxtFavItem1);
        tvFav2 = (TextInputEditText) getView().findViewById(R.id.edtTxtFavItem2);
        tvFav3 = (TextInputEditText) getView().findViewById(R.id.edtTxtFavItem3);

        create = (Button) getView().findViewById(R.id.btnCreateAccount);
        addImpDate1 = (Button) getView().findViewById(R.id.btnAddImpDate2);
        addImpDate2 = (Button) getView().findViewById(R.id.btnAddImpDate3);


        dateTypeSpinner1.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, impDateTypes));
        dateTypeSpinner2.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, impDateTypes));
        dateTypeSpinner3.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, impDateTypes));

        impDate1.setOnClickListener(this);
        impDate2.setOnClickListener(this);
        impDate3.setOnClickListener(this);
        creditCheckInDate.setOnClickListener(this);


        addImpDate1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getView().findViewById(R.id.addImpDate2).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.btnAddImpDate2).setVisibility(View.GONE);

            }
        });

        addImpDate2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getView().findViewById(R.id.addImpDate3).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.btnAddImpDate3).setVisibility(View.GONE);


            }
        });

        radioCredit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getView().findViewById(R.id.ll_paymentmode).setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });


        getView().findViewById(R.id.btnCreateAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if(validate()){

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
                    SimpleDateFormat inputDateFormat = new SimpleDateFormat(("dd-MM-yyyy"));
                    final Customer customer = new Customer();
                    customer.setId(edtTxtCustomerCode.getText().toString());
                    customer.setCus_Phone1(etCustPhone1.getText().toString());
                    customer.setCus_Phone2(etCustPhone2.getText().toString());
                    customer.setName(edtTxtCustomerName.getText().toString());
                    customer.setAddress1(edtTxtCustomerAddress.getText().toString());
                    customer.setCus_Email(etEmail.getText().toString());
                    customer.setAllergicInfo(edtTxtAllergicInformation.getText().toString());
                    customer.setCusHabits(edtTxtHabit.getText().toString());

                        try {
                            customer.setImpDate1(addMoreDretails.isChecked() ? dateFormat.format(inputDateFormat.parse(impDate1.getText().toString())) : "");
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    try {
                        customer.setImpDate2(addMoreDretails.isChecked() ? dateFormat.format(inputDateFormat.parse(impDate2.getText().toString())) : "");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    try {
                        customer.setImpDate3(addMoreDretails.isChecked() ? dateFormat.format(inputDateFormat.parse(impDate3.getText().toString())) : "");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    customer.setImpDate1Desc(addMoreDretails.isChecked() ? impDateTypes[dateTypeSpinner1.getSelectedItemPosition()] : "");
                    customer.setImpDate2Desc(addMoreDretails.isChecked() ? impDateTypes[dateTypeSpinner2.getSelectedItemPosition()] : "");
                    customer.setImpDate3Desc(addMoreDretails.isChecked() ? impDateTypes[dateTypeSpinner3.getSelectedItemPosition()] : "");

                    customer.setCusFavorite1(tvFav1.getText().toString().trim());
                    customer.setCusFavorite2(tvFav2.getText().toString().trim());
                    customer.setCusFavorite3(tvFav3.getText().toString().trim());

                        int radioButtonID = radioPaymentMode.getCheckedRadioButtonId();
                        View radioButton = radioPaymentMode.findViewById(radioButtonID);
                        int idx = radioPaymentMode.indexOfChild(radioButton);

                        if(idx == 1){

                            customer.setRoomNo(creditRoomNumber.getText().toString());
                            SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                            Calendar c = Calendar.getInstance();
                            try {
                                c.setTime(sdfDate.parse(creditCheckInDate.getText().toString().trim()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            String checkinDate = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()) + " 00:00:00.000";
                            customer.setCheckinDate(checkinDate);

                            if(creditAccId != null)
                                customer.setAccId(creditAccId);
                            else if(creaditAccounts.size() > 0)
                                customer.setAccId(creaditAccounts.get(0).getId());

                            customer.setIsCredit(true);

                        }
                        else
                            customer.setIsCredit(false);

                    createCustomer(customer);


                }


            }
        });

        addMoreDretails.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                getView().findViewById(R.id.importantDates).setVisibility(isChecked ? View.VISIBLE : View.GONE);
                getView().findViewById(R.id.favFoodItems).setVisibility(isChecked ? View.VISIBLE : View.GONE);



            }
        });


    }


    private boolean validate() {

        int radioButtonID = radioPaymentMode.getCheckedRadioButtonId();
        View radioButton = radioPaymentMode.findViewById(radioButtonID);
        int idx = radioPaymentMode.indexOfChild(radioButton);

        if (edtTxtCustomerCode.getText().toString().trim().length() == 0){

            edtTxtCustomerCode.setError("Enter phone");
            edtTxtCustomerCode.requestFocus();
            return false;
        }

        else if(edtTxtCustomerName.getText().toString().trim().length() == 0){
            edtTxtCustomerName.setError("Enter Name");
            edtTxtCustomerName.requestFocus();
            return false;

        }


        /*else if (edtTxtCustomerAddress.getText().toString().trim().length() == 0){

            edtTxtCustomerAddress.setError("Enter Address");
            edtTxtCustomerAddress.requestFocus();
            return false;
        }*/

        else if(idx == 1){

            if(creditRoomNumber.getText().toString().trim().length() == 0){

                creditRoomNumber.setError("RoomNumber");
                creditRoomNumber.requestFocus();
                return false;

            }
            else if(creditCheckInDate.getText().toString().trim().length() == 0){

                creditCheckInDate.setError("CheckInDate");
                creditCheckInDate.requestFocus();
                return false;
            }





        }

        return true;
    }


    private void createCustomer(final Customer customer) {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Creating customer");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final Response<String> response = Utilities.getRetrofitWebService(getContext()).addCustomerN(customer).execute();
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (response != null) {

                                if (response.body() != null) {
                                    if (response.body().equals("ok")) {
                                        Toast.makeText(CreateCustomerFragment.this.getContext(), "Customer Created", Toast.LENGTH_SHORT).show();
                                        onCustomerSelected(customer);
                                    } else {
                                        Toast.makeText(CreateCustomerFragment.this.getContext(), "Error", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(CreateCustomerFragment.this.getContext(), "Error", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
                } catch (IOException e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Toast.makeText(CreateCustomerFragment.this.getContext(), "Network Error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();


    }


    private void onCustomerSelected(Customer customer) {


        try {
            Intent intent = new Intent("customerSelected");
            intent.putExtra("customer", customer);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    @Override
    public void onItemSelected(Customer customer) {
        try {
            Intent intent = new Intent("customerSelected");
            intent.putExtra("customer", customer);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(final View v) {

        final Calendar calendar = Calendar.getInstance();

        String date = ((TextView) v).getText().toString();
        if (!date.isEmpty()) {
            try {
                String[] dateParts = date.split("-");
                calendar.set(Calendar.YEAR, Integer.parseInt(dateParts[2]));
                calendar.set(Calendar.MONTH, Integer.parseInt(dateParts[1]) - 1);
                calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateParts[0]));
            } catch (Exception e) {

            }
        }


        new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                calendar.set(Calendar.YEAR, i);
                calendar.set(Calendar.MONTH, i1);
                calendar.set(Calendar.DAY_OF_MONTH, i2);
                ((TextView) v).setText(new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime()));


            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)).show();



    }

    public void LoadCreditAccounts(){

        final MaterialDialog dialogSave = new MaterialDialog.Builder(getActivity())
                .content("Account Details")
                .progress(true, 0)
                .cancelable(false)
                .build();

        if (!Utilities.isNetworkConnected(getActivity()))
            Utilities.createNoNetworkDialog(getActivity());
        else {

            dialogSave.show();
            Utilities.getRetrofitWebService(getActivity()).getCreditAccounts().enqueue(new Callback<List<CreditAccount>>() {
                @Override
                public void onResponse(Response<List<CreditAccount>> response, Retrofit retrofit) {
                    dialogSave.dismiss();
                    if (response.body() != null) {

                        creaditAccounts = response.body();
                        List<String> datas = new ArrayList<>();
                        if(creaditAccounts.size() > 0) {
                            for (CreditAccount creAccount : creaditAccounts) {
                                datas.add(creAccount.getName());

                            }
                        }


                        Aadapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas);

                        ((AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName)).setAdapter(Aadapter);

                        ((AppCompatSpinner) getView().findViewById(R.id.PaymentAccountName)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            try {
                                creditAccId  = creaditAccounts.get(position).getId();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                    } else
                        Utilities.showToast("creadit Account Failed" , getActivity());
                }

                @Override
                public void onFailure(Throwable t) {
                    dialogSave.dismiss();
                }
            });

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, new IntentFilter(SelectCustomerDialogFragment.CREATE_CUSTOMER_FRAGMENT_ACTION));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onQueryChanged(String query) {

        try {
            if (query.equals(oldQuery)) {
                return;
            } else {
                oldQuery = query;
            }


            Log.e("Query changed", query);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

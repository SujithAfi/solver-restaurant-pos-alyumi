package com.afi.restaurantpos.al_loomie.Dialogs;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afi.restaurantpos.al_loomie.Adapters.TableOptionsAdapter;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.flyco.tablayout.SlidingTabLayout;

import java.util.List;
import java.util.Map;


import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class TableOptionsDialog extends Fragment {
    private static final String ARG_PARAM_TABLE_ID = "param1";
    private SlidingTabLayout tabs;
    private ViewPager viewPagerTableOPtions;
    private String kotnumber;
    private Table table;


    public static TableOptionsDialog newInstance(Table table) {
        TableOptionsDialog fragment = new TableOptionsDialog();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TABLE_ID, table.Tab_Cd);
        fragment.setArguments(args);
        fragment.table = table;
        return fragment;
    }

    public TableOptionsDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            kotnumber = getArguments().getString(ARG_PARAM_TABLE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_table_options_dialog, container, false);
        viewPagerTableOPtions = (ViewPager) v.findViewById(R.id.viewPagerTableOPtions);
        viewPagerTableOPtions.setOffscreenPageLimit(3);
        tabs = (SlidingTabLayout) v.findViewById(R.id.tabs);

        return v;
    }

    public void loadKotAndBills() {
        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            Utilities.getRetrofitWebService(getContext()).getKotAndBillsInTable(kotnumber).enqueue(new Callback<Map<String, List<PendingKotItem>>>() {
                @Override
                public void onResponse(Response<Map<String, List<PendingKotItem>>> response, Retrofit retrofit) {
                    try {
                        if (response.body() != null && TableOptionsDialog.this.isVisible()) {
                            Map<String, List<PendingKotItem>> data = response.body();
                            TableOptionsAdapter tableOptionsAdapter = (TableOptionsAdapter) viewPagerTableOPtions.getAdapter();
                            if (tableOptionsAdapter == null) {
                                tableOptionsAdapter = new TableOptionsAdapter(getChildFragmentManager(), data);
                                viewPagerTableOPtions.setAdapter(tableOptionsAdapter);
                                tabs.setViewPager(viewPagerTableOPtions);
                            } else {
                                tableOptionsAdapter.refreshData(data);
                            }
                            try {
                                if (data.get("kot").size() == 0 && data.get("bill").size() == 0)
                                    viewPagerTableOPtions.setCurrentItem(0, false);
                                else if (data.get("kot").size() > 0 && data.get("bill").size() == 0)
                                    viewPagerTableOPtions.setCurrentItem(0, false);
                                else if (data.get("kot").size() == 0 && data.get("bill").size() > 0)
                                    viewPagerTableOPtions.setCurrentItem(1, false);
                                else
                                    viewPagerTableOPtions.setCurrentItem(0, false);
                            } catch (Exception e) {

                            }

                            getActivity().sendBroadcast(new Intent("refreshTableOptionsLists"));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadKotAndBills();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent("refreshTable");
        getActivity().sendBroadcast(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            getActivity().registerReceiver(reloadKotAndBillReceiver, new IntentFilter("reloadBillAndKot"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            getActivity().unregisterReceiver(reloadKotAndBillReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver reloadKotAndBillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                loadKotAndBills();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}

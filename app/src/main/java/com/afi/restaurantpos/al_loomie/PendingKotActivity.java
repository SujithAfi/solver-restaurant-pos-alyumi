package com.afi.restaurantpos.al_loomie;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.PendingKotAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.BillFragmentDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.CancelBillAndKotDialog;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillContainer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Load the pending KOTS
 */
public class PendingKotActivity extends AppCompatActivity {

    List<SelectedItemDetails> selectedItemDetailses;
    String newBillNumber;
    private Button btnSplitKot;
    private Button btnMergeKot;
    private Button btnCancelKot;
    private RecyclerView pendingKotView;
    private Toolbar toolbar;
    private TextView txtNoPendingBills;
    private boolean clickLock = false;
    private Handler clickHandler = new Handler();
    private Runnable clickLockRunnable = new Runnable() {
        @Override
        public void run() {
            clickLock = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utilities.initKitkatStatusbarTransparancy(this);
        setContentView(R.layout.activity_pending_kot);
        Utilities.initURL(this);
        btnSplitKot = (Button) findViewById(R.id.btnSplitKot);
        btnMergeKot = (Button) findViewById(R.id.btnMergeKot);
        btnCancelKot = (Button) findViewById(R.id.btnCancelKot);
        pendingKotView = (RecyclerView) findViewById(R.id.rvpendingKot);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pending KOT's");
        pendingKotView.setHasFixedSize(true);
        pendingKotView.setLayoutManager(new GridLayoutManager(this, 2));
        txtNoPendingBills = (TextView) findViewById(R.id.txtNoPendingBills);
        btnMergeKot.setEnabled(false);
        btnSplitKot.setEnabled(false);
        btnCancelKot.setEnabled(false);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!Utilities.isNetworkConnected(getApplicationContext()))
            Utilities.createNoNetworkDialog(getApplicationContext());
        else
            loadPendingKots();

    }

    /**
     * Initilize the click listners
     *
     * @param size
     */
    private void initCheckListner(int size) {
        getSupportActionBar().setSubtitle(size == 0 ? "" : String.format("%d items selected", size));

        if (size == 0) {
            btnMergeKot.setEnabled(false);
            btnSplitKot.setEnabled(false);
            btnCancelKot.setEnabled(false);
            toolbar.setSubtitle("");
        } else if (size == 1) {
            btnMergeKot.setEnabled(false);
            btnSplitKot.setEnabled(true);
            btnCancelKot.setEnabled(true);
            toolbar.setSubtitle("1 Kot selected");
        } else if (size > 1) {
            btnMergeKot.setEnabled(true);
            btnSplitKot.setEnabled(false);
            btnCancelKot.setEnabled(false);
            toolbar.setSubtitle(String.format("%d Kot selected", size));
        }

        //if ( )
    }

    /**
     * Function to load the pending kots
     */
    private void loadPendingKots() {

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Loading Pending Kot's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        dialog.show();
        Utilities.getRetrofitWebService(getApplicationContext()).getAllPendingKot().enqueue(new Callback<List<PendingKot>>() {
            @Override
            public void onResponse(Response<List<PendingKot>> response, Retrofit retrofit) {
                dialog.cancel();
                List<PendingKot> pendingKots = response.body() == null ? new ArrayList<PendingKot>() : response.body();



                pendingKotView.setAdapter(new PendingKotAdapter(pendingKots, new PendingKotAdapter.PendingKotSelectListner() {
                    @Override
                    public void onKotselected(String kotNumber) {//Todo direct click on item
                        if (!clickLock) {
                            clickLock = true;
                            clickHandler.postDelayed(clickLockRunnable, 1000);
                            List<String> kot = new ArrayList<String>();
                            kot.add(kotNumber);
                            loadKotDetails(kot, false, true);
                        }


                    }

                    @Override
                    public void onKotCheckChange() {
                        int size = getSelectedPendingKots().size();
                        if (size == 0) {
                            btnMergeKot.setEnabled(false);
                            btnSplitKot.setEnabled(false);
                            btnCancelKot.setEnabled(false);
                            toolbar.setSubtitle("");
                        } else if (size == 1) {
                            btnMergeKot.setEnabled(false);
                            btnSplitKot.setEnabled(true);
                            btnCancelKot.setEnabled(true);
                            toolbar.setSubtitle("1 Kot selected");
                        } else if (size > 1) {
                            btnMergeKot.setEnabled(true);
                            btnSplitKot.setEnabled(false);
                            btnCancelKot.setEnabled(false);
                            toolbar.setSubtitle(String.format("%d Kot selected", size));
                        }
                    }
                }));


                initCheckListner(((PendingKotAdapter) pendingKotView.getAdapter()).getAllSelectedKots().size());

                makeLoadingResult(pendingKots.isEmpty());
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.cancel();
                makeLoadingResult(true);
            }
        });


    }

    /**
     * Update UI according to the Loading result
     *
     * @param isListempty
     */
    private void makeLoadingResult(boolean isListempty) {
        if (isListempty) {
            txtNoPendingBills.setVisibility(View.VISIBLE);
            pendingKotView.setVisibility(View.GONE);
        } else {
            txtNoPendingBills.setVisibility(View.GONE);
            pendingKotView.setVisibility(View.VISIBLE);
            pendingKotView.setBackgroundColor(getResources().getColor(R.color.cardview_outer));
        }
    }

    /**
     * Split KOT action
     *
     * @param view
     */

    public void splitKot(View view) {
        List<PendingKot> kots = getSelectedPendingKots();
        if (!kots.isEmpty()) {
            List<String> kotStringList = new ArrayList<>();
            for (PendingKot pendingKot : kots)
                kotStringList.add(pendingKot.Kot_No);

            loadKotDetails(kotStringList, false, false);
        }
    }

    /**
     * Merge kot action
     *
     * @param view
     */
    public void mergeKot(View view) {
        {
            List<PendingKot> kots = getSelectedPendingKots();
            if (!kots.isEmpty()) {
                List<String> kotStringList = new ArrayList<>();
                for (PendingKot pendingKot : kots)
                    kotStringList.add(pendingKot.Kot_No);

                loadKotDetails(kotStringList, true, false);
            }
        }
    }

    /**
     * Cancel kot action
     *
     * @param view
     */
    public void cancelKot(View view) {
        List<PendingKot> kots = getSelectedPendingKots();
        if (!kots.isEmpty()) {
            PendingKot pendingKot = kots.get(0);
            CancelBillAndKotDialog.newInstance(CancelType.CANCEL_KOT)
                    .setPendingKot(pendingKot)
                    .setDialogListner(new CancelBillAndKotDialog.CancelBillAndKotDialogListner() {
                        @Override
                        public void onFinishedAction(CancelBillAndKotDialog cancelBillAndKotDialog) {
                            //TODO refresh items
                            cancelBillAndKotDialog.dismiss();
                            loadPendingKots();
                        }
                    })
                    .show(getSupportFragmentManager(), "Pending KOT");
        }
    }

    /**
     * Load the Kot details
     *
     * @param kotStringList
     * @param isMerge
     * @param isDirectSelection
     */
    private void loadKotDetails(final List<String> kotStringList, final boolean isMerge, final boolean isDirectSelection) {

        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .content("Loading Selected Kot's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        if (!Utilities.isNetworkConnected(this))
            Utilities.createNoNetworkDialog(this);
        else {

            dialog.show();
            Utilities.getRetrofitWebService(getApplicationContext()).getKotDetailsFromKotNumber(kotStringList).enqueue(new Callback<Map<String, List<PendingKotItem>>>() {
                @Override
                public void onResponse(Response<Map<String, List<PendingKotItem>>> response, Retrofit retrofit) {
                    dialog.dismiss();
                    if (response.body() != null) {
                        Map<String, List<PendingKotItem>> listMap = response.body();

                        if (isDirectSelection) {//todo go directly to bill
                            //prepare bill from here
                            try {
                                ApplicationSingleton.getInstance().setSelectedKotMap(listMap);
                                ApplicationSingleton.getInstance().setTemporaryPendingKotItems(listMap.get(kotStringList.get(0)));
                            } catch (Exception e) {
                                String t = e.getMessage();
                            }


                            //Pocess bill data
                            selectedItemDetailses = new ArrayList<>();
                            for (PendingKotItem item : ApplicationSingleton.getInstance().getTemporaryPendingKotItems()) {
                                SelectedItemDetails details = new SelectedItemDetails();
                                details.itemCode = item.Itm_Cd;
                                details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                                details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                                details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                                details.Itm_UCost = item.KotD_UCost;
                                details.itemName = item.Itm_Name;
                                details.Unit_Fraction = item.Unit_Fraction;
                                details.Unit_cd = item.Kot_MuUnit;
                                details.barcode = item.Barcode;
                                details.Kot_No = item.Kot_No;
                                details.Kot_No = item.Kot_No;
                                details.Menu_SideDish = item.menu_SideDish;
                                details.Cmd_IsBuffet = "0";

                                details.Cus_Name = item.Cus_Name;
                                details.Cus_Cd = item.Cus_Cd;
                                details.Disc_Per = item.Disc_Per;

                                details.Cm_Type = item.Cm_Type;
                                details.Cm_Covers = item.Cm_Covers;
                                details.Sman_Cd = item.Sman_Cd;
                                details.Tab_Cd = item.Tab_Cd;
                                details.CmD_Amt = item.CmD_Amt;
                                selectedItemDetailses.add(details);
                            }


                            final MaterialDialog dialog = new MaterialDialog.Builder(PendingKotActivity.this)
                                    .content("Creating new Bill")
                                    .progress(true, 1)
                                    .titleColorRes(R.color.colorAccentDark)
                                    .cancelable(false).build();
                            dialog.show();
                            Utilities.getRetrofitWebService(getApplicationContext()).getNewBillNumber(ApplicationSingleton.getInstance().getCounterNumber()).enqueue(new Callback<Map<String, String>>() {
                                @Override
                                public void onResponse(Response<Map<String, String>> response, Retrofit retrofit) {
                                    dialog.cancel();
                                    Map<String, String> stringStringMap = response.body();
                                    if (stringStringMap.get("bill_no") != null) {
                                        if (stringStringMap.get("bill_no").length() > 0) {
                                            ApplicationSingleton.getInstance().setNewBillNumber(Utilities.getKotFormat(stringStringMap.get("bill_no")));
                                        }
                                    }
                                    newBillNumber = String.format("Bill No : %s", Utilities.getKotFormat(stringStringMap.get("bill_no")));

                                    if (selectedItemDetailses != null) {
                                        if (selectedItemDetailses.size() > 0) {
                                            final BillContainer saveBillData = prepareData(selectedItemDetailses);
                                            new BillFragmentDialog()
                                                    .setBillItems(saveBillData.details)
                                                    .setNewBillNumber(newBillNumber)
                                                    .setBillListner(new BillFragmentDialog.BillListner() {
                                                        @Override
                                                        public void onPositive() {


                                                            if (!Utilities.isNetworkConnected(getApplicationContext()))
                                                                Utilities.createNoNetworkDialog(getApplicationContext());
                                                            else {

                                                                final MaterialDialog dialog = new MaterialDialog.Builder(PendingKotActivity.this)
                                                                        .content("Saving Bill")
                                                                        .progress(true, 1)
                                                                        .titleColorRes(R.color.colorAccentDark)
                                                                        .cancelable(false).build();
                                                                dialog.show();
                                                                Utilities.getRetrofitWebService(getApplicationContext()).saveBill(saveBillData).enqueue(new Callback<Map<String, Object>>() {
                                                                    @Override
                                                                    public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                                                                        dialog.dismiss();
                                                                        final boolean response_code = (Boolean) response.body().get("response_code");
                                                                        if (response_code)
                                                                            new MaterialDialog.Builder(PendingKotActivity.this)
                                                                                    .titleColorRes(R.color.colorAccentDark)
                                                                                    .title(response_code ? "Bill Saved" : "Can't save bill")
                                                                                    .positiveText("Ok")
                                                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                                        @Override
                                                                                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                                                            if (response_code) {
                                                                                                loadPendingKots();
                                                                                            }
                                                                                        }
                                                                                    }).build().show();
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Throwable t) {
                                                                        dialog.dismiss();

                                                                    }
                                                                });


                                                            }
                                                        }

                                                        @Override
                                                        public void onNegative() {

                                                        }
                                                    })
                                                    .show(getSupportFragmentManager(), "Billu");


                                        }
                                    }

                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    dialog.cancel();
                                }
                            });


                        } else {
                            //// TODO: 10/21/2015 remove needed
                            SharedPreferences preferences = Utilities.getSharedPreferences(PendingKotActivity.this);
                            if (preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_1_ENABLED, false))
                                ApplicationSingleton.getInstance().setTaxFraction1(Float.parseFloat(preferences.getString(Constants.SHARED_PREF_KEY_TAX_1, "0")));
                            if (preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_2_ENABLED, false))
                                ApplicationSingleton.getInstance().setTaxFraction2(Float.parseFloat(preferences.getString(Constants.SHARED_PREF_KEY_TAX_2, "0")));

                            Intent intent = new Intent(PendingKotActivity.this, SplitAndMergeActivity.class);
                            ApplicationSingleton.getInstance().setSelectedKotMap(listMap);
                            intent.putExtra("operation", isMerge);
                            startActivityForResult(intent, 67);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    dialog.dismiss();
                }
            });


        }

    }

    /**
     * get slected item details from the list
     *
     * @return
     */
    private List<PendingKot> getSelectedPendingKots() {
        if (pendingKotView.getAdapter() == null)
            return new ArrayList<PendingKot>();
        return ((PendingKotAdapter) pendingKotView.getAdapter()).getAllSelectedKots();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    /**
     * Prepare data to save the bill
     *
     * @param selectedItemDetailses
     * @return
     */
    private BillContainer prepareData(List<SelectedItemDetails> selectedItemDetailses) {
        String Tab_Cd = "";
        String Cm_Covers = "";

        Context context = this;
        ApplicationSingleton singleton = ApplicationSingleton.getInstance();
        SharedPreferences preferences = Utilities.getSharedPreferences(context);
        int index = 0;
        double Cm_TotAmt = 0;
        String Cus_Name = "";
        String Cus_Cd = "";
        String Disc_Per = "";
        BillContainer billContainer = new BillContainer();
        List<BillItem> billItems = new ArrayList<>();
        for (SelectedItemDetails details : selectedItemDetailses) {

            Cus_Name = details.Cus_Name;
            Cus_Cd = details.Cus_Cd;
            Disc_Per = details.Disc_Per;

            Tab_Cd = details.Tab_Cd;
            Cm_Covers = details.Cm_Covers;
            BillItem billItem = new BillItem();
            billItem.Itm_cd = details.itemCode;
            //put modifiers
            billItem.Menu_SideDish = details.Menu_SideDish;
            if (details.modify != null) {
                for (int i = 0; i < details.modify.size(); i++) {
                    ItemModifiers modifier = details.modify.get(i);
                    if (i + 1 == details.modify.size())
                        billItem.Menu_SideDish += modifier.name;
                    else if (i == 0)
                        billItem.Menu_SideDish += modifier.name;
                    else
                        billItem.Menu_SideDish += "," + modifier.name;
                }
            }
            billItem.Cmd_ItmName = details.itemName;
            billItem.BarCode = details.barcode;
            billItem.Unit_Fraction = details.Unit_Fraction;
            billItem.Cmd_Qty = details.itemQuantity + "";
            billItem.Unit_Cd = details.Unit_cd;
            billItem.Cmd_MuUnit = "1";//TODO
            billItem.Cmd_MuQty = details.itemQuantity + "";
            billItem.Cmd_MuFraction = "1";
            billItem.Cmd_MuConvToBase = "1";
            billItem.Cmd_MuUcost = "1";
            billItem.Cmd_Rate = details.itemUnitCost + "";//// TODO: 10/21/2015
            double Cmd_Amta = (details.itemTotalCost);//*(details.itemQuantity);
            String Cmd_DiscPers = billItem.Cmd_DiscPer;
            billItem.Cmd_DiscPer = details.Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
            if (Cmd_DiscPers != null) {
                billItem.Cmd_DiscAmt = Utilities.getPercentage(Cmd_Amta, Double.parseDouble(billItem.Cmd_DiscPer)) + "";
            } else {
                billItem.Cmd_DiscAmt = "0";
            }
            billItem.Cmd_Amt = (Cmd_Amta - Double.parseDouble(billItem.Cmd_DiscAmt)) + "";
            billItem.Cmd_Ctr = index++ + "";
            billItem.Cmd_SubCtr = "0";
            billItem.Cmd_RowType = "A";
            billItem.Cmd_NotCosted = "0";
            billItem.Cmd_Cancelled = "0";
            billItem.Cmd_IsLzQty = "1";
            billItem.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO, 1) + "";
            billItem.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
            billItem.Cmd_Status = "1";
            billItem.Cm_ThruDo = "0";
            billItem.IsModified = "0";
            billItem.Cm_Ref = "1";
            billItem.Cmd_DiscReason = "No";
            billItem.Kot_No = details.Kot_No == null ? "0" : details.Kot_No;
            billItem.Rcp_Cd = "1";
            billItem.Cmd_Ucost = details.itemUnitCost + "";
            billItem.Cmd_IsBuffet = details.Cmd_IsBuffet;//// TODO: 10/21/2015
            billItems.add(billItem);
            Cm_TotAmt += Double.parseDouble(billItem.Cmd_Amt);
        }
        billContainer.details = billItems;
        billContainer.Usr_id = preferences.getString(Constants.SHARED_PREF_KEY_ID, "");
        billContainer.Cm_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_No = singleton.getNewBillNumber();
        billContainer.Sman_Cd = singleton.getSalesman(this) == null ? "0" : singleton.getSalesman(this).Id + "";
        billContainer.Cus_Cd = Cus_Cd;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Id + "";
        billContainer.Cus_Name = Cus_Name;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Name + "";billContainer.Cm_TotAmt =
        billContainer.Cm_DiscPer = Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
        billContainer.Cm_DiscAmt = Utilities.getPercentage(Cm_TotAmt, Double.parseDouble(billContainer.Cm_DiscPer)) + "";

        billContainer.Cm_TotAmt = Cm_TotAmt + "";

        Cm_TotAmt -= Double.parseDouble(billContainer.Cm_DiscAmt);

        billContainer.Cm_Tax1_Per = Utilities.getTaxFraction1(context) + "";
        billContainer.Cm_Tax1_Amt = Utilities.findTax1(context, Cm_TotAmt) + "";
        billContainer.Cm_Tax2_Per = Utilities.getTaxFraction2(context) + "";
        billContainer.Cm_Tax2_Amt = Utilities.findTax2(context, Cm_TotAmt) + "";
        billContainer.Cm_NetAmt = (Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt)) + "";
        billContainer.Cm_Tips_Amt = "0";
        billContainer.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO, 0) + "";
        billContainer.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
        billContainer.Cm_BC_Settle_Amt = "0.000";
        billContainer.Cm_Paid_Amt = "0.000";
        billContainer.Cm_Cancel_Reason = "";
        billContainer.Cm_Cancelled = "0";
        billContainer.Cm_Cus_Phone = singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().getId() + "";
        billContainer.Cm_Type = "1";//TODO get order type
        billContainer.Tab_Cd = Tab_Cd;//singleton.getTable() == null ? "" : singleton.getTable().Tab_Cd;//TODO
        billContainer.Cm_Covers = Cm_Covers;//singleton.getGuestCount();
        billContainer.Srvr_Cd = "1";
        billContainer.Cm_Collected_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_Collected_User = preferences.getString(Constants.SHARED_PREF_KEY_USERNAME, "0");
        //(Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt) - Double.parseDouble( billContainer.Cm_DiscPer)) + "";
        // billContainer.Collect_Sft_No = "2";//// TODO: 10/21/2015
        //billContainer.Collect_Sft_Dt = "10-10-2015";//// TODO: 10/21/2015
        int i = 0;
        i++;
        billContainer.Srvr_Cd = i + "";

        return billContainer;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        initCheckListner(0);
    }
}

package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Models.Counter;
import com.afi.restaurantpos.al_loomie.Models.CreateCounterResponse;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class CounterIdSetupDialog extends DialogFragment {


    private CounterIdSetupDialogCallBack counterIdSetupDialogCallBack;

    private ViewGroup mContainer;
    private View mLayout;

    private TextView txtDeviceId;
    private EditText edtTxtCounterId;
    private Spinner spinnerBuisinessType;

    private Toolbar toolbar;

    public CounterIdSetupDialog() {
        // Required empty public constructor
    }


    public interface CounterIdSetupDialogCallBack {
        void onPositive(CounterIdSetupDialog counterIdSetupDialog, String counterId);
        void onNegative(CounterIdSetupDialog counterIdSetupDialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_counter_id_setup_dialog, container, false);
        txtDeviceId = (TextView) mLayout.findViewById(R.id.txtDeviceId);
        edtTxtCounterId = (EditText) mLayout.findViewById(R.id.edtTxtCounterId);
        spinnerBuisinessType = (Spinner) mLayout.findViewById(R.id.spinnerBuisinessType);
        toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.counter_id_setup);
        toolbar.setSubtitle(R.string.setting_counter_id);
        toolbar.setNavigationIcon(R.drawable.ic_smartphone_white_24dp);
        return mLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.array_buisiness_type)); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBuisinessType.setAdapter(spinnerArrayAdapter);
        txtDeviceId.setText(Utilities.getDeviceId(getContext()));
        this.setCancelable(false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setPositiveButton("OK" , null)
                .setCancelable(false)
                .setNegativeButton("Cancel", null)
                .create();
    }


    @Override
    public void onStart()
    {
        super.onStart();
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    validateFormData();

                }
            });

            Button negativeButton = d.getButton(Dialog.BUTTON_NEGATIVE);
            negativeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if ( counterIdSetupDialogCallBack != null ) {
                        counterIdSetupDialogCallBack.onNegative(CounterIdSetupDialog.this);
                    }

                }
            });

        }
    }

    private void validateFormData() {
        if(edtTxtCounterId.getText().toString().length() == 0){
            edtTxtCounterId.setError("Counter id must not be empty");
            return;
        }
        else if(Integer.parseInt(edtTxtCounterId.getText().toString()) == 0 ){
            edtTxtCounterId.setError("Counter id must be between 0 and 1");
            return;
        }
        else {
            edtTxtCounterId.setError(null);
            if (!Utilities.isNetworkConnected(getContext()))
                Utilities.createNoNetworkDialog(getContext());
            else
                startCounterRegistrationTask();
        }
    }

    private void startCounterRegistrationTask() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Registering Counter");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            Utilities.getRetrofitWebService(getContext()).insertCounter(new Counter()
                    .setType(spinnerBuisinessType.getSelectedItemPosition() == 0 ? "RESTAURANT" : "FOOD COURT")
                    .setName(txtDeviceId.getText().toString())
                    .setCounter(edtTxtCounterId.getText().toString())).enqueue(new Callback<CreateCounterResponse>() {
                @Override
                public void onResponse(Response<CreateCounterResponse> response, Retrofit retrofit) {
                    try {
                        progressDialog.dismiss();
                        showResult(response.body(), response.body().response_code, response.body().insertResponse);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    try {
                        progressDialog.dismiss();
                        showResult(null , false , "Network error...!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    private void showResult(CreateCounterResponse singleData, boolean b , final String cause) {

        if (b) {

            try {
                new AlertDialog.Builder(getContext())
                        .setCancelable(false)
                        .setTitle("Success")
                        .setMessage(String.format("Counter %s registered with this Device" , edtTxtCounterId.getText().toString()))
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Utilities.getSharedPreferences(getContext())
                                        .edit()
                                        .putString(Constants.COUNTER_NAME , edtTxtCounterId.getText().toString())
                                        .apply();
                                if( counterIdSetupDialogCallBack != null ) {
                                    counterIdSetupDialogCallBack.onPositive(CounterIdSetupDialog.this , edtTxtCounterId.getText().toString());
                                }

                            }
                        }).create().show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else {

            try {
                new AlertDialog.Builder(getContext())
                        .setCancelable(false)
                        .setTitle("Failed")
                        .setMessage(cause)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                edtTxtCounterId.setError(cause);
                            }
                        }).create().show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public CounterIdSetupDialog setCounterIdSetupDialogCallBack(CounterIdSetupDialogCallBack counterIdSetupDialogCallBack) {
        this.counterIdSetupDialogCallBack = counterIdSetupDialogCallBack;
        return this;
    }
}

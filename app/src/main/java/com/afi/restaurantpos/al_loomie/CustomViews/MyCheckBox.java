package com.afi.restaurantpos.al_loomie.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ToggleButton;

/**
 * Created by AFI on 10/20/2015.
 * <p>
 *     Checkbox to avoid internal callback
 * </p>
 * @author JITHIN
 *
 */
public class MyCheckBox extends ToggleButton {

    private MyCheckedChangedListner myChackedChangedListner;

    private boolean isDown;

    public interface MyCheckedChangedListner{
        void onCheckedChanged(boolean state, boolean isFromUser);
    }

    public MyCheckBox(Context context) {
        super(context);
    }

    public MyCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void setMyCheckedChangedListner(MyCheckedChangedListner myChackedChangedListner) {
        this.myChackedChangedListner = myChackedChangedListner;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                isDown = true;
                break;
            case MotionEvent.ACTION_UP:
                if(isDown){
                    super.toggle();
                    if(myChackedChangedListner != null)
                        myChackedChangedListner.onCheckedChanged(super.isChecked() , true);
                    isDown = false;
                    break;
                }
            default:break;
        }
        return true;
    }

    public void setMyCheckboxChecked(boolean state){
        if(super.isChecked() == state){
            return;
        }
        else
        {
            super.setChecked(state);
            if(myChackedChangedListner!= null)
                myChackedChangedListner.onCheckedChanged(super.isChecked(), false);
        }
    }
    public boolean getMyCheckboxState(){
        return super.isChecked();
    }
}

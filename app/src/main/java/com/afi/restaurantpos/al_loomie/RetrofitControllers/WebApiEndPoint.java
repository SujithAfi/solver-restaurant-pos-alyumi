package com.afi.restaurantpos.al_loomie.RetrofitControllers;

import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Models.CreditAccount;
import com.afi.restaurantpos.al_loomie.Models.CancelKotOrBill;
import com.afi.restaurantpos.al_loomie.Models.Counter;
import com.afi.restaurantpos.al_loomie.Models.CreateCounterResponse;
import com.afi.restaurantpos.al_loomie.Models.History;
import com.afi.restaurantpos.al_loomie.Models.KotAlert;
import com.afi.restaurantpos.al_loomie.Models.KotContainer;
import com.afi.restaurantpos.al_loomie.Models.Payment;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.Models.ReservationContainer;
import com.afi.restaurantpos.al_loomie.Models.StartShift;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillContainer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.CreateUser;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Floor;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemRecipeDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.KotSaveResponse;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Login;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Maincategory;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBillItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotOnTable;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PreviousBill;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Salesman;
import com.afi.restaurantpos.al_loomie.RetrofitModels.SearchResult;
import com.afi.restaurantpos.al_loomie.RetrofitModels.SubCategory;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;



public interface WebApiEndPoint {


    /**
     * Login webservice
     * @param credentials The user credentials as Generic map
     * */
    @POST("login")
    Call<Login> getLoginData(@Body Map<String,String> credentials);

    /**
     * Get Floor details webservice
     * @return List of Floors
     * */
    @GET("floor")
    Call<List<Floor>> getFloors();

    /**
     * Get Customer List
     * @return List of customers
     * */
    @GET("customer")
    Call<List<Customer>> getCustomers();

    /**
     * Get salesman List
     * @return List of salesman's
     * */
    @GET("salesman")
    Call<List<Salesman>> getSalesManList();

    /**
     * Create a user
     *
     * @param phone phone number
     * @param name name
     * @param addres address
     * @param addres2 second address
     * @param discount discount
     * @param distance distance
     * @return instance of CreateUser which is details about the created user
     */
    @GET("insert_customer")
    Call<CreateUser> createCustomer(
            @Query("code") String phone ,
            @Query("name") String name ,
            @Query("address1") String addres ,
            @Query("address2") String addres2 ,
            @Query("discount") String discount ,
            @Query("distance") String distance
    );



    /**
     * Get table details
     * @param floorId id of the floor
     * @return List of floors
     */
    @GET("table_details")
    Call<List<Table>> getTablesData( @Query("floor") String floorId , @Query("current_date") String currentDate);


    /**
     * Get all main categories
     * @param counter counter number
     * @return List of main category items in the counter
     */
    @GET("maincategory")
    Call<List<Maincategory>> getmainCategories(@Query("counter") String counter , @Query("dayofweek") String dayofweek);



    /**
     * Get sub category from the maincategoryId id
     * @param maincategoryid the main category id
     * @param counter counte number
     * @return List of sub categories
     */
    @GET("subcategory")
    Call<List<SubCategory>> getSubCategories(@Query("maincategoryid") String maincategoryid  , @Query("counter") String counter ,@Query("dayofweek") String dayofweek);


    /**
     * Get all items from the subCategoryId
     * @param subcategoryid Subcategry id
     * @return List of items in the subcategory
     */
    @GET("items")
    Call<List<Item>> getItems(@Query("subcategoryid") String subcategoryid ,@Query("dayofweek") String dayofweek);


    /**
     * Get Item details from itemid and barcode
     * @param itemCode
     * @param barCode
     * @return
     */
    @GET("itemsdetails")
    Call<ItemDetails> getItemDetails(@Query("itemcode") String itemCode , @Query("Barcode") String barCode);



    /**
     * Get recipe and itemdetails from itemid
     * @param itemcode item code
     * @return Item recipie details
     */
    @GET("recipedetails")
    Call<ItemRecipeDetails> getItemRecipeDetails(@Query("itemcode") String itemcode);


    /**
     * Save new kot
     * @param kotContainer
     * @return The insert response
     */
    @POST("insert_kotH")
    Call<KotSaveResponse> saveKot(@Body KotContainer kotContainer);


    /**
     * Get all pending kot'// STOPSHIP: 10/16/2015
     * @param tableId the id of the table
     * @return List of PendingKotOnTable
     */
    @GET("modifykot")
    Call<List<PendingKotOnTable>> getPendingKotsOnTable(@Query("tableId") String tableId);


    /**
     * Modify the kot
     * @param kotContainer
     * @return The insert response
     */
    @POST("update_kotH")
    Call<KotSaveResponse> updateKot(@Body KotContainer kotContainer);


    /**
     * get all pending kot'// STOPSHIP: 10/20/2015
     * @return List of PendingKot
     */
    @GET("PendingKOTH")
    Call<List<PendingKot>> getAllPendingKot();


    /**
     * Get kot details from kot nummbers
     * @param kotNumbers list of KOT numbers
     * @return Map of kot to kot details
     */
    @POST("getKotDetailsFromKotNumber")
    Call<Map<String , List<PendingKotItem>>> getKotDetailsFromKotNumber(@Body List<String> kotNumbers);



    /**
     * Get Bill details from Bill nummbers
     * @param BillNumbers list of bill numbers
     * @return List of PendingBillItem
     */
    @POST("PendingBillD")
     List<PendingBillItem> getBillDetailsFromBillNumber(@Body List<String> BillNumbers);


    /**
     * Get a new Bill number
     * @param counter counter number
     * @return Map of the bill numbers
     */
    @GET("newbillnumber")
    Call<Map<String , String>> getNewBillNumber(@Query("counter") String counter);


    /**
     * get all pending kot'// STOPSHIP: 10/20/2015
     * @return List of PendingBills
     */
    @GET("PendingBill")
    Call<List<PendingBill>> getAllPendingBill();

    /**
     * Save a bill
     * @param billContainer the instance of the object BillContainer
     * @return Save response
     */
    @POST("insert_bill")
    Call<Map<String, Object>> saveBill(@Body BillContainer billContainer);

    /**
     * Insert a payment against a bill
     * @param payment  instance of Payment object
     * @return Map of reponse
     */
    @POST("insert_payment")
    Call<Map<String, Object>> payBill(@Body Payment payment);


    /**
     * Search by item name
     * @param query query to be searched
     * @param counter counter number
     * @return List of items matches to the search query
     */
    @GET("itemsearch")
    Call<List<SearchResult>> searchWithKey(@Query("name")String query , @Query("counter") String counter);



    /**
     * Create a counter
     * @param counter counter name
     * @return Response of the operation
     */
    @POST("set_counter")
    Call<CreateCounterResponse> insertCounter(@Body Counter counter);



    /**
     * Start counter schedule
     * @param startShift
     * @return
     */
    @POST("start_shift")
    Call<Map<String , Object >> startShift(@Body StartShift startShift);


    /**
     * Stop shift
     * @param shiftId
     * @return
     */
    @GET("stop_shift")
    Call<Map<String , Object>> stopShift(@Query("id") String shiftId, @Query("float_cash") String cash);


    /**
     * Cancel a Kot
     * @param cancelKotOrBill
     * @return
     */
    @POST("cancel_kot")
    Call<Map<String , Object>> cancelKot(@Body CancelKotOrBill cancelKotOrBill);


    /**
     * Cancel a Bill
     * @param cancelKotOrBill
     * @return
     */
    @POST("cancel_bill")
    Call<Map<String , Object>> cancelBill(@Body CancelKotOrBill cancelKotOrBill);


    /**
     * Test the webservice
     * @return
     */
    @GET("test")
    Call<Boolean> testWebService();


    /**
     * get the preious Bills
     * @return
     */
    @GET("previousbill")
    Call<List<PreviousBill>> getPreiousBills();


    /**
     * Get kot and Bills in table
     * @param tableId
     * @return
     */
    @GET("tabledetails")
    Call<Map<String , List<PendingKotItem>>> getKotAndBillsInTable(@Query("tableId") String tableId);


    /**
     * GetBill Details from bill number
     * @param stringStringMap
     * @return
     */
    @POST("PendingBillInTable")
    Call<List<PendingBill>> getPendingBillFromBillNumber(@Body Map<String , String> stringStringMap);


    @POST("changeconfic")
    Call<Map<String,Object>> saveServerSetings(@Body Map<String , String> stringStringMap);

    @GET("PendingBillNumber")
    Call<List<Map<String,String>>> getBillAndKotCounts();

    /**
     * Get the server settings
     * @return
     */
    @GET("server")
    Call<List<Map<String , String>>> getCurrentServerSettings();


    @GET("connectioncheck")
    Call<Map<String , Object>> getDatabaseDetails();

    /**
     * Get customer history from customer id
     *
     * @return
     */
    @GET("getCustomerHistory")
    Call<List<History>> getUserHistory(@Query("cusCd") String customerId);

    @GET("getTableReservations")
    Call<List<Reservation>> getReservation(@Query("date") String reservationDate);

    @POST("addReservation")
    Call<String> addReservation(@Body ReservationContainer reservationContainer);

    @GET("newbillnumberN")
    Call<Map<String, String>> getNewBillNumberN(@Query("counter") String counter);

    @POST("cancel_reservation")
    Call<Map<String , Object>> cancelReservation(@Body CancelKotOrBill cancelKotOrBill);

    /**
     * Save new kot
     * @param kotContainer
     * @return The insert response
     */
    @POST("insert_resv_items")
    Call<KotSaveResponse> saveResvItems(@Body KotContainer kotContainer);

    @GET("getAllCustomersN")
    Call<List<Customer>> getAllCustomersN();

    @POST("addCustomersN")
    Call<String> addCustomerN(@Body Customer customer);

    @POST("updateCustomerN")
    Call<Customer> updateCustomerN(@Body Customer customer);

    @GET("getAllAccounts")
    Call<List<CreditAccount>> getCreditAccounts();

    @GET("multicourseitems")
    Call<List<Item>> getMuticourseItems(@Query("muticorseID") String barcode);

    @GET("mcMappedItems")
    Call<List<Item>> getMcMappedItems(@Query("itemCode") String ItemCode);

    @GET("test1")
    Call<ResponseBody> downloadFileWithFixedUrl();
    @POST("kot_alert")
    Call<Map<String , Object>> kotAlert(@Body KotAlert kotAlert);

    @GET("getkotforreorder")
    Call<List<PendingKotItem>> getKotItemsForReorder(@Query("kotNo") String kotNo);

    @GET("newKotnumber")
    Call<Map<String, String>> getNewKotNumber(@Query("counter") String counter);

}

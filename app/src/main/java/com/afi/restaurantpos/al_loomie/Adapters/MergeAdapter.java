package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.SplitAndMergeActivity;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by AFI on 10/20/2015.
 */
public class MergeAdapter extends RecyclerView.Adapter< RecyclerView.ViewHolder>{

    List<SplitAndMergeActivity.KotAdaterData> kotAdaterDatas;

    public MergeAdapter(List<SplitAndMergeActivity.KotAdaterData> kotAdaterDatas) {
        this.kotAdaterDatas = kotAdaterDatas;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            return new SplitAndMegeAdapterItemVH(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.child_merge_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final SplitAndMegeAdapterItemVH vh = ((SplitAndMegeAdapterItemVH)holder);
            PendingKotItem data = kotAdaterDatas.get(position).item;
            vh.txtItemName.setText(data.Itm_Name);
            vh.txtItemQuantity.setText(Utilities.getItemQuantityFormat(Double.parseDouble(data.KotD_Qty)));
            vh.txtItemRate.setText(Utilities.getDefaultCurrencyFormat(data.KotD_Rate + "", vh.txtItemAmount.getContext()));
            vh.txtItemAmount.setText(Utilities.getDefaultCurrencyFormat(data.KotD_Amt + "", vh.txtItemAmount.getContext()));
    }


    @Override
    public int getItemCount() {
        return kotAdaterDatas.size();
    }


    public static class SplitAndMegeAdapterItemVH extends RecyclerView.ViewHolder{

        private TextView txtItemName;
        private TextView txtItemQuantity;
        private TextView txtItemRate;
        private TextView txtItemAmount;
        public SplitAndMegeAdapterItemVH(View itemView) {
            super(itemView);
            txtItemName = (TextView) itemView.findViewById(R.id.txtItemName);
            txtItemQuantity = (TextView) itemView.findViewById(R.id.txtItemQuantity);
            txtItemRate = (TextView) itemView.findViewById(R.id.txtItemRate);
            txtItemAmount = (TextView) itemView.findViewById(R.id.txtItemAmount);
        }
    }

    public List<PendingKotItem> getSelectedItemDetails(){
        List<PendingKotItem> kotItems = new ArrayList<>();
        for(SplitAndMergeActivity.KotAdaterData kotAdaterData :kotAdaterDatas ){
            kotItems.add(kotAdaterData.item);
        }
        return kotItems;
    }


}

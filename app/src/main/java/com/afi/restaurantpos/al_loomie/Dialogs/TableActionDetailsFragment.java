package com.afi.restaurantpos.al_loomie.Dialogs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.TableDetailsFragmentPagerAdapter;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class TableActionDetailsFragment extends DialogFragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Table table;
    private List<Reservation> reservations;
    private TextView tableNo;
    private String selectedDate;
    private static final String ARG_PARAM_TABLE_ID = "param1";
    private String kotnumber;
    public static DialogFragment dialog;
    private Button btnNewReservation;
    private Button btnNewKot;
    private List<Table> tables;

    public TableActionDetailsFragment() {
        // Required empty public constructor
    }


    public static TableActionDetailsFragment newInstance(Table table, List<Reservation> reservations , List<Table> tables) {
        TableActionDetailsFragment fragment = new TableActionDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM_TABLE_ID, table.Tab_Cd);
        fragment.setArguments(args);
        fragment.table = table;
        fragment.reservations = reservations;
        fragment.tables = tables;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = this;

        if (getArguments() != null) {
            kotnumber = getArguments().getString(ARG_PARAM_TABLE_ID);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return inflater.inflate(R.layout.table_action_details, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        viewPager = (ViewPager) getView().findViewById(R.id.ViewPager);
        tabLayout = (TabLayout) getView().findViewById(R.id.tabs);
        tableNo = (TextView) getView().findViewById(R.id.tv_table);
        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbarContainer);
        btnNewReservation = (Button) getView().findViewById(R.id.btnNewReservation);
        btnNewKot = (Button) getView().findViewById(R.id.btnNewkot);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TableActionDetailsFragment.this.dismiss();
            }
        });

        if(table != null){

            if(table.getName() != null)
                tableNo.setText("Table " + table.getCaption());
        }


        if(ApplicationSingleton.getInstance().getCurrentDate() != null)
             selectedDate =  new SimpleDateFormat("dd-MM-yyyy").format(ApplicationSingleton.getInstance().getCurrentDate());
        final String currDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());

        getView().findViewById(R.id.btnNewkot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
                if(ApplicationSingleton.getInstance().getCurrentDate() != null)
                if(selectedDate.equalsIgnoreCase(currDate))
                    ((OrderDetails) getActivity()).onTableSelected(table);
                else
                    Toast.makeText(getActivity() , "Select current date for create KOT" , Toast.LENGTH_LONG).show();
            }
        });

        if(!Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_ADDORSAVE, false)){

            btnNewKot.setVisibility(View.GONE);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            btnNewReservation.setLayoutParams(param);

        }

        getView().findViewById(R.id.btnNewReservation).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                ((OrderDetails) getActivity()).onReservationSelected(table);

            }
        });



        if(!Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_RESERVATION_ADDORSAVE, false)){

            btnNewReservation.setVisibility(View.GONE);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            btnNewKot.setLayoutParams(param);

        }

        loadKotAndBills();
    }

    public void loadKotAndBills() {
        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            Utilities.getRetrofitWebService(getContext()).getKotAndBillsInTable(kotnumber).enqueue(new Callback<Map<String, List<PendingKotItem>>>() {
                @Override
                public void onResponse(Response<Map<String, List<PendingKotItem>>> response, Retrofit retrofit) {
                    try {
                        if (response.body() != null && TableActionDetailsFragment.this.isVisible()) {
                            Map<String, List<PendingKotItem>> data = response.body();

                            TableDetailsFragmentPagerAdapter tableOptionsAdapter = (TableDetailsFragmentPagerAdapter) viewPager.getAdapter();
                            if (tableOptionsAdapter == null) {
                                viewPager.setAdapter(new TableDetailsFragmentPagerAdapter(getChildFragmentManager() , table , reservations , data , tables));
                                tabLayout.setupWithViewPager(viewPager);
                            } else {
                                tableOptionsAdapter.refreshData(data);
                            }


                            try {
                                if (data.get("kot").size() == 0 && data.get("bill").size() == 0)
                                    viewPager.setCurrentItem(0, false);
                                else if (data.get("kot").size() > 0 && data.get("bill").size() == 0)
                                    viewPager.setCurrentItem(1, false);
                                else if (data.get("kot").size() == 0 && data.get("bill").size() > 0)
                                    viewPager.setCurrentItem(2, false);
                                else
                                    viewPager.setCurrentItem(0, false);
                            } catch (Exception e) {

                            }

                            getActivity().sendBroadcast(new Intent("refreshTableOptionsLists"));

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent("refreshTable");
        getActivity().sendBroadcast(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(reloadKotAndBillReceiver, new IntentFilter("reloadBillAndKot"));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(reloadKotAndBillReceiver);
    }

    private BroadcastReceiver reloadKotAndBillReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadKotAndBills();
        }
    };


}

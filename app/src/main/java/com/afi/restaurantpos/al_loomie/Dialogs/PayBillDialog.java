package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Map;

import com.afi.restaurantpos.al_loomie.Models.Payment;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class PayBillDialog extends DialogFragment {

    private PendingKot pendingKot;
    private PendingBill pendingBill;
    private Toolbar toolbar;
    double total=0.000;
    private ViewGroup mContainer;
    private View mLayout;

    private EditText tip,tentered;
    private TextView counter,bill_no,totalamt,balance;

    private TextView txtBill;
    private TextView txtHotelName;

    private CancelType cancelType;

    private PayBillDialogListner dialogListner;

    public interface PayBillDialogListner {
        void onFinishedAction(PayBillDialog PayBillDialog);
    }

    public static PayBillDialog newInstance(CancelType cancelType) {
        PayBillDialog fragment = new PayBillDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.cancelType = cancelType;
        return fragment;
    }

    public PayBillDialog() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_payment, container, false);
        tip = (EditText) mLayout.findViewById(R.id.txtTotalAmount);
        tentered = (EditText) mLayout.findViewById(R.id.txtDiscountAmount);
        counter = (TextView) mLayout.findViewById(R.id.txtCounter);
        bill_no = (TextView) mLayout.findViewById(R.id.txtbill);
        totalamt = (TextView) mLayout.findViewById(R.id.txtAmount);
        balance = (TextView) mLayout.findViewById(R.id.txtNetAmount);
        txtBill = (TextView) mLayout.findViewById(R.id.txtBill);
        txtHotelName = (TextView) mLayout.findViewById(R.id.txtHotelName);
        return mLayout;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_PAYMENT_ADDORSAVE, false))
            return new AlertDialog.Builder(getActivity())
                    .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                    .setPositiveButton("Pay Bill", null)
                    .setNegativeButton("Cancel", null)
                    .create();

        else
            return new AlertDialog.Builder(getActivity())
                    .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
//                    .setPositiveButton("Pay Bill", null)
                    .setNegativeButton("Cancel", null)
                    .create();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(true);
        tentered.addTextChangedListener(textWatcher);
        tip.addTextChangedListener(textWatcher);

        switch (cancelType) {
            case CANCEL_BILL:
                counter.setText(pendingBill.Cm_Counter);
                bill_no.setText(pendingBill.Cm_No.replace("##" ,""));
                totalamt.setText(pendingBill.Cm_NetAmt);
                total=Double.parseDouble(pendingBill.Cm_NetAmt);
                break;
        }

        Typeface myTypeface = Typeface.createFromAsset(getActivity().getAssets(), "cac_champagne.ttf");
        txtBill.setTypeface(myTypeface);
        txtHotelName.setTypeface(myTypeface);

    }

    public PayBillDialog setPendingKot(PendingKot pendingKot) {
        this.pendingKot = pendingKot;
        return this;
    }

    public PayBillDialog setPendingBill(PendingBill pendingBill) {
        this.pendingBill = pendingBill;
        return this;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        final AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Utilities.isNetworkConnected(getContext()))
                        Utilities.createNoNetworkDialog(getContext());
                    else {
                        if (!tentered.getText().toString().equals("")) {
                            double tipValue = tip.getText().toString().isEmpty() ? 0 : Double.parseDouble(tip.getText().toString());
                            if (total + tipValue <= Double.parseDouble(tentered.getText().toString()) ) {
                                SharedPreferences preferences = Utilities.getSharedPreferences(getContext());
                                Payment payment = new Payment();
                                payment.Cm_Bc_Amt = pendingBill.Cm_NetAmt;
                                payment.Cm_No = pendingBill.Cm_No;
                                payment.Cm_Counter = pendingBill.Cm_Counter;
                                payment.Cm_Collected_Counter = preferences.getString(Constants.COUNTER_NAME, "");
                                payment.Cm_Collected_User = preferences.getString(Constants.SHARED_PREF_KEY_USERNAME, "");
                                payment.Collect_Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO, 0) + "";
                                payment.Collect_Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
                                payment.Cus_Cd = pendingBill.Cus_Cd;

                                Log.e("Cust Code===>>" , pendingBill.Cus_Cd);

                                final MaterialDialog payBillDialogs = new MaterialDialog.Builder(getContext())
                                        .content("Paying Bill")
                                        .progress(true, 1)
                                        .titleColorRes(R.color.colorAccentDark)
                                        .cancelable(false).build();
                                payBillDialogs.show();
                                Utilities.getRetrofitWebService(getContext()).payBill(payment).enqueue(new Callback<Map<String, Object>>() {
                                    @Override
                                    public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                                        payBillDialogs.cancel();
                                        Map<String, Object> map = response.body();
                                        if (((Boolean) map.get("response_code"))) {
                                            new MaterialDialog.Builder(getContext())
                                                    .content("Bill Payed successfully")
                                                    .titleColorRes(R.color.colorAccentDark)
                                                    .positiveText("Ok")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                            if (dialogListner != null)
                                                                dialogListner.onFinishedAction(PayBillDialog.this);
                                                        }
                                                    })
                                                    .cancelable(false).build().show();
                                        } else {
                                            new MaterialDialog.Builder(getContext())
                                                    .content("Error")
                                                    .titleColorRes(R.color.colorAccentDark)
                                                    .positiveText("Ok")
                                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                        @Override
                                                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                            // loadPendingBills();
                                                        }
                                                    })
                                                    .cancelable(false).build().show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        payBillDialogs.cancel();
                                    }
                                });



                            } else {
                                Utilities.showToast(getResources().getString(R.string.bill_not_saved), getContext());
                            }
                        } else {

                            Utilities.showToast(getResources().getString(R.string.bill_not_saved), getContext());
                        }
                    }
                }
            });
        }
    }


    public PayBillDialog setDialogListner(PayBillDialogListner dialogListner) {
        this.dialogListner = dialogListner;
        return this;
    }

    TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(!tip.getText().toString().equals(".")&!tip.getText().toString().equals("")&!tentered.getText().toString().equals(".")&!tentered.getText().toString().equals("")){
                double paid=Double.parseDouble(tentered.getText().toString());
                double tips=Double.parseDouble(tip.getText().toString());
                if(paid>total){
                    Double balance_amt=paid-total-tips;
                    balance.setText(Utilities.getDefaultCurrencyFormat(balance_amt.toString() , getContext()));
                }
                else {
                    balance.setText("");
                }
            }
            else{
                balance.setText("0.000");
            }

        }
    };


}

package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.SearchResult;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by AFI on 11/3/2015.
 */
public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultAdapterVH>{

    private List<SearchResult> searchResults;
    SharedPreferences preferences;

    private onClickListner onClickListner;


    public interface onClickListner{
        void onClick(SearchResult searchResult);
    }
    @Override
    public SearchResultAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if(preferences == null)
            preferences = Utilities.getDefaultSharedPref(viewGroup.getContext());
        return new SearchResultAdapterVH(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_search_result_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(SearchResultAdapterVH holder, int position) {
        final SearchResult searchResult = searchResults.get(position);
        holder.txtItemTitle.setText(searchResult.Item_Name);
        holder.txtItemCategory.setText(searchResult.main_group + " -> " + searchResult.sub_group);
        holder.txtItemRate.setText(Html.fromHtml(String.format("Rate : <b> %s </b>" ,  Utilities.getDefaultCurrencyFormat( searchResult.Rate, holder.itemView.getContext()))));

        if(preferences.getBoolean("key_show_item_image", true)) {

            Picasso.with(holder.imgItemIcon.getContext())
                    .load(
                            Utilities.getImageUrl(searchResult.ItemCd, searchResult.BarCd)
                    )
                    .placeholder(R.drawable.logo)
                    .fit()
                    .centerCrop()
                    .into(holder.imgItemIcon);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickListner != null ) {
                    onClickListner.onClick(searchResult);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if(searchResults == null)
            return 0;
        else
           return searchResults.size();
    }

    public void setSearchResults(List<SearchResult> searchResults) {
        this.searchResults = searchResults;
        this.notifyDataSetChanged();
    }

    public static class SearchResultAdapterVH extends RecyclerView.ViewHolder{

        private ImageView imgItemIcon;
        private TextView txtItemTitle;
        private TextView txtItemCategory;
        private TextView txtItemRate;

        public SearchResultAdapterVH(View itemView) {
            super(itemView);
            imgItemIcon = (ImageView) itemView.findViewById(R.id.imgItemIcon);
            txtItemTitle = (TextView) itemView.findViewById(R.id.txtItemTitle);
            txtItemCategory = (TextView) itemView.findViewById(R.id.txtItemCategory);
            txtItemRate = (TextView) itemView.findViewById(R.id.txtItemRate);
        }
    }

    public void setOnClickListner(SearchResultAdapter.onClickListner onClickListner) {
        this.onClickListner = onClickListner;
    }
}

package com.afi.restaurantpos.al_loomie.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Salesman;

/**
 * Created by AFI on 9/30/2015.
 */
public class SelectSalesmanDialogFragment extends DialogFragment {


    private ViewGroup mContainer;
    private View mLayout;

    private List<Salesman> salesmans;

    private List<String> salesManNames;

    private DialogCallback dialogCallback;

    private int selectedItemIndex;

    private Toolbar toolbar;

    public interface DialogCallback{
        void onPossitiveButtonClick(int indexOfData);
        void onNegativeButtonClick();
    }

    public SelectSalesmanDialogFragment() {
    }

    public void setSalesmans(List<Salesman> salesmans) {
        this.salesmans = salesmans;
        salesManNames = new ArrayList<String>();
        if(this.salesManNames != null)
            for(Salesman salesman : this.salesmans)
                salesManNames.add(salesman.Name);
    }

    ListView mylist;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.dialog_select_sales_man, container, false);
        mylist = (ListView)mLayout.findViewById(R.id.listSalesMan);
        toolbar = (Toolbar)mLayout.findViewById(R.id.toolbar);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, salesManNames);
        mylist.setAdapter(adapter);
        mylist.setOnItemClickListener(onItemClickListener);

        toolbar.setTitle("Select Sales-Man");
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SelectSalesmanDialogFragment.this.dismiss();
            }
        });
        return mLayout;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .create();
    }

    public void setDialogCallback(DialogCallback dialogCallback) {
        this.dialogCallback = dialogCallback;
    }

    private OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(dialogCallback != null)
                dialogCallback.onPossitiveButtonClick(position);
            getDialog().dismiss();
        }
    };

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(true);
    }
}

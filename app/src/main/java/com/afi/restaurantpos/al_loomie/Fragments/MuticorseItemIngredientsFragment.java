package com.afi.restaurantpos.al_loomie.Fragments;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.McItemIngredientsAdapter;
import com.afi.restaurantpos.al_loomie.Models.Ingredients;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Item;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ingredient_temp;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.View.DividerItemDecorationWithMargin;

import java.util.ArrayList;
import java.util.List;


public class MuticorseItemIngredientsFragment extends DialogFragment {

    private static final String NAME_KEY = "item_name";
    private static final String ITEM_KEY = "mappeditem";
    private static final String MAINQTY = "main_qty";
    public boolean clickLock = false;
    private View mLayout;
    private Toolbar toolbar;
    private ItemDdetailsListCallBack callBack;
    private RecyclerView recyclerViewMuticorseItems;
    private Handler uiHandler = new Handler();
    private List<Item> mainItemList;
    private Item actualItem;
    private String mainQty;
    private ImageView ivSaveBtn;
    private boolean changedFlag = false;


    public MuticorseItemIngredientsFragment() {
    }

    public static MuticorseItemIngredientsFragment newInstance( String itemName , Item ingredients , String qty) {
        MuticorseItemIngredientsFragment fragment = new MuticorseItemIngredientsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ITEM_KEY, ingredients);
        args.putString(NAME_KEY, itemName);
        args.putString(MAINQTY, qty);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            actualItem = (Item) getArguments().getSerializable(ITEM_KEY);
            mainQty = getArguments().getString(MAINQTY);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLayout = inflater.inflate(R.layout.fragment_multicorse_ingredients, container, false);
        toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);
//        tvToolbarTitle = (TextView) mLayout.findViewById(R.id.toolbar_title);
        recyclerViewMuticorseItems = (RecyclerView) mLayout.findViewById(R.id.recyclerViewMuticorseItems);
        ivSaveBtn = (ImageView) mLayout.findViewById(R.id.iv_save);
        return mLayout;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(false);

        recyclerViewMuticorseItems.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewMuticorseItems.addItemDecoration(new DividerItemDecorationWithMargin(getActivity()));


        toolbar.setNavigationIcon(R.drawable.back_butt);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MuticorseItemIngredientsFragment.this.dismiss();
            }
        });


       for(int i = 0; i < actualItem.getIngredients().size() ; i++){

           actualItem.getIngredients().get(i).setQuantity(Integer.parseInt(mainQty));

       }

        loadIngradients();

        ivSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Item tempItem  = actualItem;
                String removeIngredient = "";
                String[] a = new String[Integer.parseInt(mainQty)];
                if(actualItem != null){


                    for(int i = 0 ; i < actualItem.getIngredients().size() ; i++){

                        if(actualItem.getIngredients().get(i).getQuantity() != Integer.parseInt(mainQty)){

                            if(actualItem.getIngredients().get(i).getQuantity() == 0){

                                if(removeIngredient.matches(""))
                                    removeIngredient = actualItem.getIngredients().get(i).getName();
                                else
                                    removeIngredient = removeIngredient + "  " + actualItem.getIngredients().get(i).getName();

                            }
                            else{


                                for(int k = 0 ; k < Integer.parseInt(mainQty) ; k++){

                                    if(tempItem.getIngredients().get(i).getQuantity() != 0) {

                                        if(a[k] == null)
                                            a[k] = tempItem.getIngredients().get(i).getName();
                                        else
                                            a[k] = a[k] + "," + tempItem.getIngredients().get(i).getName();

                                        tempItem.getIngredients().get(i).setQuantity(tempItem.getIngredients().get(i).getQuantity() - 1);
                                    }

                                }




                            }

                        }
                    }

                if(!removeIngredient.matches("")) {
                    if(actualItem.getModificationRemarks() != null && !actualItem.getModificationRemarks().matches(""))
                        actualItem.setModificationRemarks(actualItem.getModificationRemarks() + "," + "Remove " + removeIngredient + " Completely");
                    else
                        actualItem.setModificationRemarks("Remove " + removeIngredient + " Completely");

                }

                    ingredient_temp temp = new ingredient_temp();

                    for(int j = 0; j< Integer.parseInt(mainQty) ; j ++){

                        if(a[j] != null) {

                            if(actualItem.getModificationRemarks() != null && !actualItem.getModificationRemarks().matches(""))
                                actualItem.setModificationRemarks(actualItem.getModificationRemarks() + "," + "1 portion without " + a[j] );
                            else
                                actualItem.setModificationRemarks(" 1 portion without " + a[j]);

                        }

                    }

                }



                callBack.onItemSelected(actualItem , changedFlag);
                dismiss();

            }
        });

    }

    private void loadIngradients(){

        recyclerViewMuticorseItems.setAdapter(new McItemIngredientsAdapter(new McItemIngredientsAdapter.onItemClickCallBack() {
            @Override
            public void onItemSelected(int position) {

                ViewDialog alert = new ViewDialog();
                alert.showDialog(getActivity(), actualItem.getIngredients().get(position).getName() , position);


            }
        }, (ArrayList<Ingredients>) actualItem.getIngredients(), mainQty));

    }




    public void setCallBack(ItemDdetailsListCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onStop() {
        super.onStop();
        ApplicationSingleton.getInstance().setLockFlag(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface ItemDdetailsListCallBack {
        void onItemSelected(Item selectedItemDetailses, boolean isSuccess);

        void onCanceled();
    }


    public class ViewDialog {


        private EditText edtTxtQuantity;
        private ImageView btnPlus;
        private ImageView btnMinus;

        public void showDialog(Activity activity, String msg , final int pos){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_dialog);



             edtTxtQuantity = (EditText) dialog.findViewById(R.id.et_quantity);
             btnPlus = (ImageView) dialog.findViewById(R.id.iv_add);
             btnMinus = (ImageView) dialog.findViewById(R.id.iv_sub);

             edtTxtQuantity.setText(mainQty);

            TextView title = (TextView) dialog.findViewById(R.id.tv_title);
            title.setText(msg);



            Button btnSave = (Button) dialog.findViewById(R.id.btn_save);
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    actualItem.getIngredients().get(pos).setQuantity(Integer.parseInt(mainQty) - Integer.parseInt(edtTxtQuantity.getText().toString()));
                    loadIngradients();
//                    if(Integer.parseInt(edtTxtQuantity.getText().toString()) != 0) {
                        ivSaveBtn.setVisibility(View.VISIBLE);
                        changedFlag = true;
                    /*}
                    else
                        ivSaveBtn.setVisibility(View.GONE);*/

                    dialog.dismiss();
                }
            });

            Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    if(Integer.parseInt(edtTxtQuantity.getText().toString()) != 0) {
                        ivSaveBtn.setVisibility(View.VISIBLE);
                        changedFlag = true;
                    /*}
                    else
                        ivSaveBtn.setVisibility(View.GONE);*/

                    dialog.dismiss();
                }
            });



            btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Integer.parseInt(edtTxtQuantity.getText().toString()) < Integer.parseInt(mainQty)) {
                        edtTxtQuantity.setText(
                                edtTxtQuantity.getText().toString().length() > 0 ? String.valueOf(Integer.parseInt(edtTxtQuantity.getText().toString()) + 1) : ""

                        );
                    }
                }
            });

            btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    edtTxtQuantity.setText(
                            edtTxtQuantity.getText().toString().length() > 0 ?
                                    (Integer.parseInt(edtTxtQuantity.getText().toString()) > 0 ? String.valueOf(Integer.parseInt(edtTxtQuantity.getText().toString()) - 1)
                                            : edtTxtQuantity.getText().toString()) :
                                    edtTxtQuantity.getText().toString()
                    );
                }
            });


            dialog.show();

        }


    }


}



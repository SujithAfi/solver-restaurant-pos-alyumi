package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;

/**
 * Created by afi on 10/21/2015.
 */
public class PendingBillAdapter extends RecyclerView.Adapter<PendingBillAdapter.PendingBillAdapterVH> {

private PendingBillSelectListner pendingBillSelectListner;

private List<PendingBill> pendingBillList;
public interface PendingBillSelectListner{
    void onBillselected(String BillNumber , PendingBill pendingBill);
    void onBilllongselected(String BillNumber , PendingBill pendingBill);
    void onBillCheckChange();
}

    public PendingBillAdapter(List<PendingBill> pendingBillList , PendingBillSelectListner pendingBillSelectListner) {
        this.pendingBillList = pendingBillList;
        this.pendingBillSelectListner = pendingBillSelectListner;
    }

    @Override
    public PendingBillAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_pending_bill, viewGroup, false);
        PendingBillAdapterVH vh = new PendingBillAdapterVH(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(PendingBillAdapterVH holder, final int position) {
        final PendingBill pendingBill = pendingBillList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pendingBillSelectListner != null)
                    pendingBillSelectListner.onBillselected(pendingBill.Cm_No , pendingBill);
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (pendingBillSelectListner != null)
                    pendingBillSelectListner.onBilllongselected(pendingBill.Cm_No, pendingBill);
                return  false;
                        }
        });
        holder.txtBillNumber.setText(pendingBill.Cm_No.replace("#" , ""));
        holder.txtCounter.setText(pendingBill.Cm_Counter);





        holder.txtCustomerCode.setText(pendingBill.Cus_Cd);
        holder.txtTotalAmount.setText(pendingBill.Cm_TotAmt);
        holder.txtDiscountAmount.setText(pendingBill.Cm_DiscAmt);
        holder.txtNetAmount.setText(pendingBill.Cm_NetAmt);


        if (!(pendingBill.Cus_name == null)) {
            holder.txtCustomerCodeOrTableName.setText("Customer Code");
            holder.txtCustomerCode.setText(pendingBill.Cus_name);
        }
        else  {
            holder.txtCustomerCodeOrTableName.setText("Table Code");
            holder.txtCustomerCode.setText(pendingBill.Tab_Cd);
        }


    }

    @Override
    public int getItemCount() {
        return pendingBillList.size();
    }

public static class PendingBillAdapterVH extends RecyclerView.ViewHolder{

    private TextView txtBillNumber;
    private TextView txtCounter;
    private TextView txtCustomerCode;
    private TextView txtTotalAmount;
    private TextView txtDiscountAmount;
    private TextView txtNetAmount;
    private TextView txtCustomerCodeOrTableName;

    public PendingBillAdapterVH(View itemView) {
        super(itemView);
        txtCounter = (TextView) itemView.findViewById(R.id.txtCounter);
        txtCustomerCode = (TextView) itemView.findViewById(R.id.txtCustomerCode);
        txtTotalAmount = (TextView) itemView.findViewById(R.id.txtTotalAmount);
        txtDiscountAmount = (TextView) itemView.findViewById(R.id.txtDiscountAmount);
        txtNetAmount = (TextView) itemView.findViewById(R.id.txtNetAmount);
        txtBillNumber = (TextView) itemView.findViewById(R.id.txtBillNumber);
        txtCustomerCodeOrTableName = (TextView) itemView.findViewById(R.id.txtCustomerCodeOrTableName);
    }
}

    public List<PendingBill> getAllSelectedBills(){
        List<PendingBill> selectedPendingBills = new ArrayList<>();
        for(PendingBill pendingBill : pendingBillList)
                selectedPendingBills.add(pendingBill);
        return selectedPendingBills;
    }

    public void setPendingBillSelectListner(PendingBillSelectListner pendingBillSelectListner) {
        this.pendingBillSelectListner = pendingBillSelectListner;
    }
}

package com.afi.restaurantpos.al_loomie.Fragments;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.afi.restaurantpos.al_loomie.Models.ReservationContainer;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationFragment extends Fragment implements View.OnClickListener {


    private TextView txtreservationNumber;
    private TextView txtdate;
    private TextView txtTime;

    private EditText edtTxtGuestNumber;
    private TextView txtTableName;
    private TextView txtCustomerCode;
    private TextView txtCustomerName;

    private Table table;
    private Customer customer;

    private Handler uiHandler = new Handler();

    private Date selectedReservationDate = null;
    private Date selectedReservationTime = null;


    private TextView tvHabits;
    private CardView cvHabits;
    private TextView tvDate3Desc;
    private TextView tvdate3;
    private LinearLayout lldate3;
    private TextView tvDate2Desc;
    private TextView tvdate2;
    private LinearLayout lldate2;
    private TextView tvDate1Desc;
    private TextView tvdate1;
    private LinearLayout lldate1;
    private CardView cvImptDates;
    private TextView tvFav3;
    private LinearLayout llFav3;
    private TextView tvFav2;
    private LinearLayout llFav2;
    private TextView tvFav1;
    private LinearLayout llFav1;
    private CardView cvFavorites;
    private TextView tvAllergy;
    private CardView cvAllergy;

    public ReservationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            table = (Table) getArguments().getSerializable("table");
            customer = (Customer) getArguments().getSerializable("customer");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reservation, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txtreservationNumber = (TextView) getView().findViewById(R.id.txtreservationNumber);
        txtdate = (TextView) getView().findViewById(R.id.txtdate);
        txtTime = (TextView) getView().findViewById(R.id.txtTime);

        txtTableName = (TextView) getView().findViewById(R.id.txtTableName);
        txtCustomerCode = (TextView) getView().findViewById(R.id.txtCustomerCode);
        txtCustomerName = (TextView) getView().findViewById(R.id.txtCustomerName);

        edtTxtGuestNumber = (EditText) getView().findViewById(R.id.edtTxtGuestNumber);

        tvHabits = (TextView) getView().findViewById(R.id.tv_habit);
        cvHabits = (CardView) getView().findViewById(R.id.cv_habits);

        tvAllergy = (TextView) getView().findViewById(R.id.tv_allergy);
        cvAllergy = (CardView) getView().findViewById(R.id.cv_allergy);

        tvDate3Desc = (TextView) getView().findViewById(R.id.tv_date3desc);
        tvdate3 = (TextView) getView().findViewById(R.id.tv_date3);
        lldate3 = (LinearLayout) getView().findViewById(R.id.ll_date3);
        tvDate2Desc = (TextView) getView().findViewById(R.id.tv_date2desc);
        tvdate2 = (TextView) getView().findViewById(R.id.tv_date2);
        lldate2 = (LinearLayout) getView().findViewById(R.id.ll_date2);
        tvDate1Desc = (TextView) getView().findViewById(R.id.tv_date1disc);
        tvdate1 = (TextView) getView().findViewById(R.id.tv_date1);
        lldate1 = (LinearLayout) getView().findViewById(R.id.ll_date1);
        cvImptDates = (CardView) getView().findViewById(R.id.cv_impdates);


        tvFav3 = (TextView) getView().findViewById(R.id.tv_favorite3);
        llFav3 = (LinearLayout) getView().findViewById(R.id.ll_fav3);
        tvFav2 = (TextView) getView().findViewById(R.id.tv_favorite2);
        llFav2 = (LinearLayout) getView().findViewById(R.id.ll_fav2);
        tvFav1 = (TextView) getView().findViewById(R.id.tv_favorite1);
        llFav1 = (LinearLayout) getView().findViewById(R.id.ll_fav1);
        cvFavorites = (CardView) getView().findViewById(R.id.cv_favourites);

        getView().findViewById(R.id.btnAdd).setOnClickListener(this);
        getView().findViewById(R.id.btnSubstract).setOnClickListener(this);

        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_create_customer_fragment);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (item.getItemId() == R.id.menu_option_done) {

                    if(Utilities.isNetworkConnected(getActivity())) {
                        if (validateForm()) {
                            addReservation();
                        }
                    }
                    else
                        Utilities.showSnackBar(getActivity().getResources().getString(R.string.internet_connection_error) , getActivity());

                }

                return false;
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        if(ApplicationSingleton.getInstance().getCurrentDate() != null) {
            txtdate.setText(new SimpleDateFormat("dd-MM-yyyy").format(ApplicationSingleton.getInstance().getCurrentDate()));
            selectedReservationDate = ApplicationSingleton.getInstance().getCurrentDate();
        }



        txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                final Calendar calendar = Calendar.getInstance();

                String date = txtdate.getText().toString();
                if (!date.isEmpty()) {
                    try {
                        String[] dateParts = date.split("-");
                        calendar.set(Calendar.YEAR, Integer.parseInt(dateParts[2]));
                        calendar.set(Calendar.MONTH, Integer.parseInt(dateParts[1]) - 1);
                        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateParts[0]));
                    } catch (Exception e) {

                    }
                }


                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                        calendar.set(Calendar.YEAR, i);
                        calendar.set(Calendar.MONTH, i1);
                        calendar.set(Calendar.DAY_OF_MONTH, i2);
                        txtdate.setText(new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime()));

                        ApplicationSingleton.getInstance().setCurrentDate(calendar.getTime());
                        selectedReservationDate = calendar.getTime();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)).show();
            }
        });

        txtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeBundle timeBundle = null;
                final Calendar calendar = Calendar.getInstance();
                if (view.getTag() == null) {
                    timeBundle = new TimeBundle();

                    timeBundle.hour = calendar.get(Calendar.HOUR_OF_DAY);
                    timeBundle.minute = calendar.get(Calendar.MINUTE);
                } else {
                    timeBundle = (TimeBundle) view.getTag();
                }

                final TimeBundle finalTimeBundle = timeBundle;
                new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {

                        finalTimeBundle.hour = i;
                        finalTimeBundle.minute = i1;
                        txtTime.setTag(finalTimeBundle);
                        calendar.set(Calendar.HOUR_OF_DAY, i);
                        calendar.set(Calendar.MINUTE, i1);
                        txtTime.setText(new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
                        selectedReservationTime = calendar.getTime();
                    }
                }, timeBundle.hour, timeBundle.minute, false).show();
            }
        });

        initReservationSummary();


    }

    private void addReservation() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Reserving table");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        progressDialog.show();


        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    final ReservationContainer reservationContainer = new ReservationContainer();
                    reservationContainer.setDocumentId("0");
                    reservationContainer.setDocumentDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " 00:00:00.000");
                    reservationContainer.setDocumentYear(new SimpleDateFormat("yyyy").format(new Date()));
                    reservationContainer.setCounter(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_COUNTER, ""));
                    reservationContainer.setCashierId(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_ID, ""));
                    reservationContainer.setCustomerCode(customer.getId());
                    reservationContainer.setCustomerName(customer.getName());
                    reservationContainer.setGuestCount(edtTxtGuestNumber.getText().toString());
                    reservationContainer.setReservationDate(new SimpleDateFormat("yyyy-MM-dd").format(ApplicationSingleton.getInstance().getCurrentDate()) + " 00:00:00.000");
                    reservationContainer.setReservationTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(selectedReservationTime));
                    reservationContainer.setTableCode(table.getTab_Cd());
                    reservationContainer.setUserId(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_ID, ""));
                    final Response<String> response = Utilities.getRetrofitWebService(getContext()).addReservation(reservationContainer).execute();
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (response != null && response.body() != null) {
                                if (!response.body().equals("error")) {
                                    Utilities.showSnackBar("Reservation added" , getActivity());
//                                    Toast.makeText(ReservationFragment.this.getContext(), "Reservation added", Toast.LENGTH_SHORT).show();

                                    new AlertDialog.Builder(getContext())
                                            .setTitle("Reservation successful")
                                            .setMessage("Do you want to add Items for this reservation ?")
                                            .setPositiveButton("Add ITEMS", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                                                    ((OrderDetails) getActivity()).onAddItemForReservation(response.body(), customer  , new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                                                }
                                            })
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    getActivity().getSupportFragmentManager().popBackStackImmediate();

                                                }
                                            })
                                            .setCancelable(false)
                                            .create().show();
                                } else {
                                    Utilities.showSnackBar("Failed to add reservation" , getActivity());

                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error" , getActivity());

                        }
                    });
                }
            }
        }).start();
    }

    private boolean validateForm() {

        boolean result = true;

        if( txtdate.getText().toString().trim().length() == 0 ){
            Utilities.showSnackBar("Select Reservation Date" , getActivity());
            result =  false;
        }
        else if( txtTime.getText().toString().trim().length() == 0 ){
            Utilities.showSnackBar("Select Reservation Time" , getActivity());
            result =  false;
        }

        return result; //TODO
    }

    private void initReservationSummary() {


        if (table != null && customer != null) {
            txtCustomerCode.setText(customer.getId());
//            txtCustomerCode.setText(customer.getCus_Phone());
            txtCustomerName.setText(customer.getName());
            txtTableName.setText(table.Caption);


            if(customer != null){

                if(customer.getCusFavorite1() != null && !customer.getCusFavorite1().matches("")){

                    cvFavorites.setVisibility(View.VISIBLE);
                    tvFav1.setText(customer.getCusFavorite1());

                    if(customer.getCusFavorite2() != null && !customer.getCusFavorite2().matches("")){

                        llFav2.setVisibility(View.VISIBLE);
                        tvFav2.setText(customer.getCusFavorite2());
                    }

                    if(customer.getCusFavorite3() != null && !customer.getCusFavorite3().matches("")){

                        llFav3.setVisibility(View.VISIBLE);
                        tvFav3.setText(customer.getCusFavorite3());
                    }



                }

                if(customer.getImpDate1() != null && !customer.getImpDate1().matches("") && !customer.getImpDate1().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                    Calendar calDateImp = Calendar.getInstance();
                    SimpleDateFormat sdfDateImp = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());

                    cvImptDates.setVisibility(View.VISIBLE);

                    List<String> items = Arrays.asList(customer.getImpDate1().split("\\s+"));

                    if(items.size() > 0) {
                        try {

                            calDateImp.setTime(sdfDateImp.parse(items.get(0)));
                            tvdate1.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    tvDate1Desc.setText(customer.getImpDate1Desc());

                    if(customer.getImpDate2() != null && !customer.getImpDate2().matches("") && !customer.getImpDate2().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                        lldate2.setVisibility(View.VISIBLE);
                        List<String> items1 = Arrays.asList(customer.getImpDate2().split("\\s+"));

                        if(items1.size() > 0) {
                            try {

                                calDateImp.setTime(sdfDateImp.parse(items1.get(0)));
                                tvdate2.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                        tvDate2Desc.setText(customer.getImpDate2Desc());
                    }

                    if(customer.getImpDate3() != null && !customer.getImpDate3().matches("") && !customer.getImpDate3().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                        lldate3.setVisibility(View.VISIBLE);

                        List<String> items2 = Arrays.asList(customer.getImpDate3().split("\\s+"));

                        if(items2.size() > 0) {
                            try {

                                calDateImp.setTime(sdfDateImp.parse(items2.get(0)));
                                tvdate3.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }

                        tvDate3Desc.setText(customer.getImpDate3Desc());
                    }



                }

                if(customer.getCusHabits() != null && !customer.getCusHabits().matches("")){

                    cvHabits.setVisibility(View.VISIBLE);
                    tvHabits.setText(customer.getCusHabits());
                }

                if(customer.getAllergicInfo() != null && !customer.getAllergicInfo().matches("")){

                    cvAllergy.setVisibility(View.VISIBLE);
                    tvAllergy.setText(customer.getAllergicInfo());
                }



            }
        }

        loadResrvationNumber();

    }

    private void loadResrvationNumber() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading reservation details");
        progressDialog.setIndeterminate(true);

        progressDialog.show();


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final Response<Map<String, String>> res = Utilities.getRetrofitWebService(getContext()).getNewBillNumberN(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_COUNTER, "")).execute();

                    if (res != null && res.body() != null) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                progressDialog.dismiss();
                                onReservationDetailsLoaded(res.body());

                            }
                        });


                    }

                } catch (Exception e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error" , getActivity());
                        }
                    });
                }
            }
        }).start();

    }

    private void onReservationDetailsLoaded(Map<String, String> reservationDetails) {

        txtreservationNumber.setText(new DecimalFormat("##000000").format(Double.parseDouble(reservationDetails.get("bill_no"))));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnAdd) {
            try {
                edtTxtGuestNumber.setText((Integer.parseInt((edtTxtGuestNumber.getText().toString())) + 1) + "");
            } catch (Exception e) {

            }
        } else if (view.getId() == R.id.btnSubstract) {
            try {
                edtTxtGuestNumber.setText(Integer.parseInt((edtTxtGuestNumber.getText().toString())) > 1 ? ((Integer.parseInt((edtTxtGuestNumber.getText().toString()))) - 1) + "" : edtTxtGuestNumber.getText().toString());
            } catch (Exception e) {

            }
        }
    }

    private class TimeBundle {
        int hour;
        int minute;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent("refreshTable");
        getActivity().sendBroadcast(intent);
    }
}

package com.afi.restaurantpos.al_loomie.RetrofitModels;

import java.io.Serializable;

/**
 * Created by AFI on 9/30/2015.
 * <p>
 *     POJO class for Customer
 * </p>
 */
public class Customer implements Serializable /*implements Comparator<Customer>*/ {

    public String Id;
    public String Name;
    public String Address1;
    public double Discount;



    public String Email;
    public String Address2;
    public Integer Distance;
    private String Cus_Favorite1;
    private String Cus_Favorite2;
    private String Cus_Favorite3;
    private String Imp_Date1;
    private String Imp_Date1_Desc;
    private String Imp_Date2;
    private String Imp_Date2_Desc;
    private String Imp_Date3;
    private String Imp_Date3_Desc;
    private String Allergic_Info;
    private String Cus_Habits;
    private String Acc_Id;
    private String Acc_Name;
    private String Room_No;
    private String Checkin_Date;
    private Boolean Is_Credit;
    private String Cus_Phone1;
    private String Cus_Phone2;
    private String Cus_Email;

    public String getCus_Email() {
        return Cus_Email;
    }

    public void setCus_Email(String cus_Email) {
        Cus_Email = cus_Email;
    }


    public String getCus_Phone2() {
        return Cus_Phone2;
    }

    public void setCus_Phone2(String cus_Phone2) {
        Cus_Phone2 = cus_Phone2;
    }

    public String getCus_Phone1() {
        return Cus_Phone1;
    }

    public void setCus_Phone1(String cus_Phone1) {
        Cus_Phone1 = cus_Phone1;
    }


    public String getId() {
        return Id;
    }


    public void setId(String id) {
        this.Id = id;
    }

    public String getName() {
        return Name;
    }


    public void setName(String name) {
        this.Name = name;
    }
    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAddress1() {
        return Address1;
    }


    public void setAddress1(String address1) {
        this.Address1 = address1;
    }


    public double getDiscount() {
        return Discount;
    }


    public void setDiscount(double discount) {
        this.Discount = discount;
    }

    public String getAddress2() {
        return Address2;
    }


    public void setAddress2(String address2) {
        this.Address2 = address2;
    }


    public Integer getDistance() {
        return Distance;
    }


    public void setDistance(Integer distance) {
        this.Distance = distance;
    }


    public String getCusFavorite1() {
        return Cus_Favorite1;
    }


    public void setCusFavorite1(String cusFavorite1) {
        this.Cus_Favorite1 = cusFavorite1;
    }


    public String getCusFavorite2() {
        return Cus_Favorite2;
    }


    public void setCusFavorite2(String cusFavorite2) {
        this.Cus_Favorite2 = cusFavorite2;
    }


    public String getCusFavorite3() {
        return Cus_Favorite3;
    }


    public void setCusFavorite3(String cusFavorite3) {
        this.Cus_Favorite3 = cusFavorite3;
    }


    public String getImpDate1() {
        return Imp_Date1;
    }


    public void setImpDate1(String impDate1) {
        this.Imp_Date1 = impDate1;
    }


    public String getImpDate1Desc() {
        return Imp_Date1_Desc;
    }


    public void setImpDate1Desc(String impDate1Desc) {
        this.Imp_Date1_Desc = impDate1Desc;
    }


    public String getImpDate2() {
        return Imp_Date2;
    }


    public void setImpDate2(String impDate2) {
        this.Imp_Date2 = impDate2;
    }


    public String getImpDate2Desc() {
        return Imp_Date2_Desc;
    }


    public void setImpDate2Desc(String impDate2Desc) {
        this.Imp_Date2_Desc = impDate2Desc;
    }


    public String getImpDate3() {
        return Imp_Date3;
    }


    public void setImpDate3(String impDate3) {
        this.Imp_Date3 = impDate3;
    }


    public String getImpDate3Desc() {
        return Imp_Date3_Desc;
    }


    public void setImpDate3Desc(String impDate3Desc) {
        this.Imp_Date3_Desc = impDate3Desc;
    }


    public String getAllergicInfo() {
        return Allergic_Info;
    }


    public void setAllergicInfo(String allergicInfo) {
        this.Allergic_Info = allergicInfo;
    }


    public String getCusHabits() {
        return Cus_Habits;
    }


    public void setCusHabits(String cusHabits) {
        this.Cus_Habits = cusHabits;
    }


    public String getAccId() {
        return Acc_Id;
    }


    public void setAccId(String accId) {
        this.Acc_Id = accId;
    }


    public String getAccName() {
        return Acc_Name;
    }


    public void setAccName(String accName) {
        this.Acc_Name = accName;
    }


    public String getRoomNo() {
        return Room_No;
    }


    public void setRoomNo(String roomNo) {
        this.Room_No = roomNo;
    }


    public String getCheckinDate() {
        return Checkin_Date;
    }


    public void setCheckinDate(String checkinDate) {
        this.Checkin_Date = checkinDate;
    }


    public Boolean getIsCredit() {
        return Is_Credit;
    }


    public void setIsCredit(Boolean isCredit) {
        this.Is_Credit = isCredit;
    }
}

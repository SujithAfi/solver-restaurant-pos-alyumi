package com.afi.restaurantpos.al_loomie.Dialogs;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Models.CancelKotOrBill;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


/**
 * A simple {@link Fragment} subclass.
 */
public class TableReservationDetailsDialogFragment extends Fragment {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private Table table;
    private List<Reservation> reservations;
    private TextView txtNoPendingBillsOrKot;
    private ReservationsAdapter resvAdapter;

    public TableReservationDetailsDialogFragment() {
        // Required empty public constructor
    }


    public static TableReservationDetailsDialogFragment create(Table table, List<Reservation> reservations) {

        TableReservationDetailsDialogFragment tableReservationDetailsDialogFragment = new TableReservationDetailsDialogFragment();
        tableReservationDetailsDialogFragment.table = table;
        tableReservationDetailsDialogFragment.reservations = reservations;
        return tableReservationDetailsDialogFragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_table_reservation_details_dialog, container, false);
        txtNoPendingBillsOrKot = (TextView) rootview.findViewById(R.id.txtNoPendingBillsOrKot);
        return rootview;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext() , 2));

        resvAdapter = new ReservationsAdapter(reservations);
        recyclerView.setAdapter(resvAdapter);


    }


    private class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.ViewHolder> {


        private List<Reservation> reservationsTable = new ArrayList<>();

        public ReservationsAdapter(List<Reservation> reservations) {

            for (Reservation reservation : reservations) {
                if (reservation.getTableCount().equals(table.getTab_Cd())) {
                    reservationsTable.add(reservation);
                }
            }

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_list_content, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            //check permission for enable delete
            if(Utilities.getSharedPreferences(getContext()).getBoolean(Constants.SHARED_PRE_UR_RESERVATION_DELETE, false))
                holder.ivDelete.setVisibility(View.VISIBLE);
            else
                holder.ivDelete.setVisibility(View.GONE);

                if(reservationsTable.get(position).getReservationTimeFrom() != null) {

                List<String> items = Arrays.asList(reservationsTable.get(position).getReservationTimeFrom().split("\\s+"));

                if(items.size() > 0) {

                    holder.tvTime.setText(items.get(1) + " " + items.get(2));

                    }

            }

            if(reservationsTable.get(position).getResDate() != null) {
                List<String> items = Arrays.asList(reservationsTable.get(position).getResDate().split("\\s+"));

                if(items.size() > 0){

                    holder.tvDate.setText(items.get(0));

                }
            }
            if(reservationsTable.get(position).getDocNumber() != null)
                holder.tvResNo.setText(reservationsTable.get(position).getDocNumber().replace("##" , ""));
            if(reservationsTable.get(position).getCustomer() != null){
                if(reservationsTable.get(position).getCustomer().getName() != null)
                    holder.tvCustName.setText(reservationsTable.get(position).getCustomer().getName());
            }
            if(reservationsTable.get(position).getGuestCount() != null)
                holder.tvGuest.setText(reservationsTable.get(position).getGuestCount());

            //Check permission to enable Reservation view
            if(Utilities.getSharedPreferences(getContext()).getBoolean(Constants.SHARED_PRE_UR_RESERVATION_VIEW, false)) {

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TableActionDetailsFragment.dialog.dismiss();
                        ((OrderDetails) getActivity()).onReservationDetails(reservationsTable.get(position));

                    }
                });
            }
            else
                Toast.makeText(getContext() , "You don't have permission to access" , Toast.LENGTH_LONG).show();


            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    new AlertDialog.Builder(getContext())
                            .setTitle("Info")
                            .setMessage("Cancel Reservation")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    try {
                                        onCancel(reservationsTable.get(position).getDocNumber() , reservationsTable.get(position).getResDate() , position);
                                        reservationsTable.remove(position);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            })
                            .setNegativeButton("cancel" , new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create().show();




                }
            });
        }

        @Override
        public int getItemCount() {
            return reservationsTable.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final TextView tvResNo;
            private final TextView tvDate;
            private final TextView tvGuest;
            private final ImageView ivDelete;
            private final TextView tvCustName;
            private TextView tvTime;
            public ViewHolder(View itemView) {
                super(itemView);

                tvResNo = (TextView) itemView.findViewById(R.id.tv_reservation_no);
                tvTime = (TextView)  itemView.findViewById(R.id.tv_time);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                tvGuest = (TextView) itemView.findViewById(R.id.tv_guest);
                ivDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
                tvCustName = (TextView) itemView.findViewById(R.id.tv_cutname);
            }
        }

    }

    public void onCancel(String docNo , String resDate , final int pos){

        CancelKotOrBill cancelKotOrBill = new CancelKotOrBill();
        cancelKotOrBill.id = docNo ;
        cancelKotOrBill.date = resDate;
        cancelKotOrBill.reson = "";

        Utilities.getRetrofitWebService(getContext()).cancelReservation(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
            @Override
            public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {

                if (response != null) {
                    if ((Boolean) response.body().get("response_code")) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.success)
                                .setMessage("Reservation Cancelled")
                                .setCancelable(false)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        reservations.remove(pos);
                                        getActivity().sendBroadcast(new Intent("refreshTableOptionsLists"));

                                    }
                                })
                                .create().show();
                    } else {
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.error)
                                .setMessage("Canot delete")
                                .setCancelable(false)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .create().show();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            getActivity().registerReceiver(refreshActionReciever, new IntentFilter("refreshTableOptionsLists"));
            txtNoPendingBillsOrKot.setVisibility(reservations.size() == 0 ? View.VISIBLE : View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            getActivity().unregisterReceiver(refreshActionReciever);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver refreshActionReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ReservationsAdapter tableKotOptionsListAdapter = (ReservationsAdapter) recyclerView.getAdapter();
            tableKotOptionsListAdapter.notifyDataSetChanged();
            txtNoPendingBillsOrKot.setVisibility(reservations.size() == 0 ? View.VISIBLE : View.GONE);
        }
    };

}

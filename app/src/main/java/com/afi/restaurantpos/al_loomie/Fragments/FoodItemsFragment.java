package com.afi.restaurantpos.al_loomie.Fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.BillAdapter;
import com.afi.restaurantpos.al_loomie.Adapters.MainCategoryAdater;
import com.afi.restaurantpos.al_loomie.Adapters.SubCategoryAdapter;
import com.afi.restaurantpos.al_loomie.CallBacks.ItemClickCallback;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Maincategory;
import com.afi.restaurantpos.al_loomie.RetrofitModels.SubCategory;
import com.afi.restaurantpos.al_loomie.SelectItemsActivity;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


/**
 * Fragment to display the FoodItems
 */
public class FoodItemsFragment extends Fragment {

    private RecyclerView recyclerViewMainCategory, recyclerViewSubCategory;
    private ProgressBar pBMaincategory;
    private ProgressBar pBSubCategory;
    private ProgressBar pBItems;
    private TextView txtTapToretrymainCategory;
    private RecyclerView rVSubBill;
    private TextView total;
    private BillAdapter billAdapter;

    private FloatingActionButton fabBillUp;
    private Reservation reservation;
    private String kotDate;

    public FoodItemsFragment() {
        billAdapter = new BillAdapter(getContext());
    }

    public static FoodItemsFragment newInstance(String kotDate) {
        FoodItemsFragment fragment = new FoodItemsFragment();
        Bundle b = new Bundle();
        b.putString("kotDate" , kotDate);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){

            kotDate = getArguments().getString("kotDate");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_food_items, container, false);
        pBMaincategory = (ProgressBar) view.findViewById(R.id.pBmainCategory);
        pBSubCategory = (ProgressBar) view.findViewById(R.id.pBSubCategry);
        pBItems = (ProgressBar) view.findViewById(R.id.pBItems);
        txtTapToretrymainCategory = (TextView) view.findViewById(R.id.txtMainCategortyRetry);
        recyclerViewMainCategory = (RecyclerView) view.findViewById(R.id.rv_main_ctegory);
        recyclerViewSubCategory = (RecyclerView) view.findViewById(R.id.rv_sub_ctegory);
        rVSubBill = (RecyclerView) view.findViewById(R.id.rv_bub_bill);
        total = (TextView) view.findViewById(R.id.txtBillTotal);
        fabBillUp = (FloatingActionButton) view.findViewById(R.id.fabBillUp);
        fabBillUp.setVisibility(View.INVISIBLE);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rVSubBill.setHasFixedSize(true);
        rVSubBill.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rVSubBill.setAdapter(billAdapter);
        txtTapToretrymainCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshMainCategory();
            }
        });
        ViewCompat.animate(fabBillUp)
                .scaleX(0)
                .scaleY(0)
                .setDuration(100)
                .start();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fabBillUp.setVisibility(View.VISIBLE);
                fabBillUp.clearAnimation();
                ViewCompat.animate(fabBillUp)
                        .scaleX(1f)
                        .scaleY(1f)
                        .setDuration(300)
                        .setInterpolator(new DecelerateInterpolator())
                        .start();

            }
        }, 2000);
        fabBillUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabBillUp.clearAnimation();

                ((SelectItemsActivity) getActivity()).setUpperFabAnimation(true, 1);
                ((SelectItemsActivity) getActivity()).animateBillFragment();


            }
        });
        initilizeRecyclerViews();

        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else
            refreshMainCategory();
    }


    private void initilizeRecyclerViews() {

        try {
            recyclerViewMainCategory.setHasFixedSize(true);
            recyclerViewSubCategory.setHasFixedSize(true);
            recyclerViewMainCategory.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            recyclerViewSubCategory.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void refreshMainCategory() {

       /* SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);*/

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(kotDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        Log.e("dayofWeek=====>>" , String.valueOf(dayOfWeek) );



        try {
            pBMaincategory.setVisibility(View.VISIBLE);
            Utilities.getRetrofitWebService(getContext()).getmainCategories(ApplicationSingleton.getInstance().getCounterNumber() , String.valueOf(dayOfWeek)).enqueue(new Callback<List<Maincategory>>() {
                @Override
                public void onResponse(Response<List<Maincategory>> response, Retrofit retrofit) {
                    pBMaincategory.setVisibility(View.GONE);
                    if (response.body() != null && FoodItemsFragment.this.isVisible()) {
                        try {
                            final List<Maincategory> data = response.body();

                            txtTapToretrymainCategory.setVisibility(View.GONE);
                            recyclerViewMainCategory.setAdapter(new MainCategoryAdater(data, new ItemClickCallback() {
                                @Override
                                public void onItemSelected(int position, String id) {
                                    if (data != null)
                                        if (data.size() > 0)
                                            refresSubCategory(id);
                                }
                            }));
                            if (data != null)
                                if (data.size() > 0)
                                    refresSubCategory(data.get(0).GrpCd);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    pBMaincategory.setVisibility(View.GONE);
                    txtTapToretrymainCategory.setVisibility(View.VISIBLE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void refresSubCategory(String mainategoryId) {


       /* Calendar c = Calendar.getInstance();
        c.setTime(Calendar.getInstance().getTime());
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        Log.e("dayofWeek=====>>" , String.valueOf(dayOfWeek) );*/

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(kotDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);


        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            try {

                pBSubCategory.setVisibility(View.VISIBLE);
                Utilities.getRetrofitWebService(getContext()).getSubCategories(mainategoryId, ApplicationSingleton.getInstance().getCounterNumber(),String.valueOf(dayOfWeek))
                        .enqueue(new Callback<List<SubCategory>>() {
                            @Override
                            public void onResponse(Response<List<SubCategory>> response, Retrofit retrofit) {
                                if (response.body() != null && FoodItemsFragment.this.isVisible()) {
                                    try {
                                        pBSubCategory.setVisibility(View.GONE);
                                        final List<SubCategory> data = response.body();
                                        recyclerViewSubCategory.setAdapter(new SubCategoryAdapter(data, new ItemClickCallback() {
                                            @Override
                                            public void onItemSelected(int position, String id) {

                                                if (!ApplicationSingleton.getInstance().isSubCategoryClickLock()) {

                                                    ApplicationSingleton.getInstance().setSubCategoryClickLock(true);
                                                    ((SelectItemsActivity) getActivity()).showItemFragment(id, data.get(position).SubGrp_Name);

                                                }
                                            }
                                        }));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                pBSubCategory.setVisibility(View.GONE);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    public void addToSubBillData(SelectedItemDetails itemDetails) {
        billAdapter.addToBillDetails(itemDetails);
        total.setText(ApplicationSingleton.getInstance().getTotalOrderCost() + "");

    }


}



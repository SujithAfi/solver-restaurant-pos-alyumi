package com.afi.restaurantpos.al_loomie;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Dialogs.ServerCheckerDialogFragment;
import com.afi.restaurantpos.al_loomie.RetrofitControllers.WebApiEndPoint;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Activity to manage the server settings
 */
public class ServerSettingsActivity extends AppCompatActivity {

    private EditText edtTxtServerUrl;
    private EditText editTextCurrentServerUrl;
    String inputUrl;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_settings);
        context=this;
        Utilities.initKitkatStatusbarTransparancy(this);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Server Configuration");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        edtTxtServerUrl = (EditText) findViewById(R.id.editText);
        editTextCurrentServerUrl = (EditText) findViewById(R.id.editTextCurrentServerUrl);
        findViewById(R.id.btnSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeybord(ServerSettingsActivity.this);
                String url = edtTxtServerUrl.getText().toString();
                if (url.trim().replace("http://", "").isEmpty()  ) {
                    Utilities.showSnackBar("Url can't be empty", ServerSettingsActivity.this);
                    return;
                }
                else if (! validate())
                    return;



                final ProgressDialog progressDialog = new ProgressDialog(ServerSettingsActivity.this);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.setTitle("Saving settings");
                progressDialog.setMessage("Pleas wait...!");

                Map<String, String> variables = new HashMap<>();
                variables.put("server", ((EditText) findViewById(R.id.edtTxtServer)).getText().toString());
                variables.put("u_name", ((EditText) findViewById(R.id.edtTxtUsername)).getText().toString());
                variables.put("password", ((EditText) findViewById(R.id.edtTxtPassword)).getText().toString());
                variables.put("path", ((EditText) findViewById(R.id.edtTxtImageDir)).getText().toString());
                variables.put("table_name", ((EditText) findViewById(R.id.edtTxtTableName)).getText().toString());
                progressDialog.show();

                new Retrofit.Builder()
                        .baseUrl(edtTxtServerUrl.getText().toString().endsWith("/") ? edtTxtServerUrl.getText().toString() : edtTxtServerUrl.getText().toString() + "/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                        .create(WebApiEndPoint.class).saveServerSetings(variables).enqueue(new Callback<Map<String, Object>>() {
                    @Override
                    public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                        if (response.code() != 200) {
                            onFailure(new Throwable("Invalid Url"));
                            return;
                        }
                        Map<String, Object> stringObjectMap = response.body();
                        String singleData = "";
                        boolean response_code = (boolean) stringObjectMap.get("response_code");
                        if (response_code) {
                            singleData = "Success";
                            final String finalSingleData = singleData;
                            progressDialog.show();

                            new Retrofit.Builder()
                                    .baseUrl(edtTxtServerUrl.getText().toString().endsWith("/") ? edtTxtServerUrl.getText().toString() : edtTxtServerUrl.getText().toString() + "/")
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build()
                                    .create(WebApiEndPoint.class).testWebService().enqueue(new Callback<Boolean>() {
                                @Override
                                public void onResponse(Response<Boolean> response, Retrofit retrofit) {
                                    progressDialog.cancel();
                                    boolean result = response.body();

                                    if (result) {
                                        Utilities.getSharedPreferences(ServerSettingsActivity.this)
                                                .edit()
                                                .putString(Constants.SHARED_PREF_WEB_API_URL, edtTxtServerUrl.getText().toString().endsWith("/") ? edtTxtServerUrl.getText().toString() : edtTxtServerUrl.getText().toString() + "/")
                                                .putBoolean(Constants.SHARED_PREF_WEB_API_URL_CONFIGURED, true)
                                                .apply();
                                        showCurrentServerUrl();
                                        Utilities.showToast(finalSingleData, ServerSettingsActivity.this);
                                    } else {
                                        Utilities.showToast("Server settings incorrect", ServerSettingsActivity.this);
                                    }
                                }

                                @Override
                                public void onFailure(Throwable t) {
                                    progressDialog.cancel();

                                }
                            });

                        } else {
                            Utilities.showToast("URL not valid", ServerSettingsActivity.this);
                        }

                    }

                    @Override
                    public void onFailure(Throwable t) {
                        progressDialog.cancel();
                        Utilities.showToast("URL not valid", ServerSettingsActivity.this);
                    }
                });


            }
        });
    }

    private boolean validate() {

        if (((EditText)findViewById(R.id.edtTxtServer)).getText().toString().isEmpty()){
            ((EditText)findViewById(R.id.edtTxtServer)).requestFocus();
            Utilities.showSnackBar("Server name is empty" , this);
        }
        else if (((EditText)findViewById(R.id.edtTxtUsername)).getText().toString().isEmpty()){
            ((EditText)findViewById(R.id.edtTxtUsername)).requestFocus();
            Utilities.showSnackBar("User name is empty" , this);
        }
        else if (((EditText)findViewById(R.id.edtTxtPassword)).getText().toString().isEmpty()){
            ((EditText)findViewById(R.id.edtTxtPassword)).requestFocus();
            Utilities.showSnackBar("Password is empty" , this);
        }
        else if (((EditText)findViewById(R.id.edtTxtTableName)).getText().toString().isEmpty()){
            ((EditText)findViewById(R.id.edtTxtTableName)).requestFocus();
            Utilities.showSnackBar("Table name is empty" , this);
        }
        else if (((EditText)findViewById(R.id.edtTxtImageDir)).getText().toString().isEmpty()){
            ((EditText)findViewById(R.id.edtTxtImageDir)).requestFocus();
            Utilities.showSnackBar("Image directory is empty" , this);
        }
        else
            return true;
        return false;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();
        else if (item.getItemId() == R.id.menu_option_configure_server){
            Toast.makeText(ServerSettingsActivity.this, "skdfksf", Toast.LENGTH_SHORT).show();
            new ServerCheckerDialogFragment().show(getSupportFragmentManager() , "dsff");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_server_settings , menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        showCurrentServerUrl();
        loadCurrentServerSettings();
    }

    private void showCurrentServerUrl() {
        SharedPreferences pref = Utilities.getSharedPreferences(ServerSettingsActivity.this);
        editTextCurrentServerUrl.setText(pref.getBoolean(Constants.SHARED_PREF_WEB_API_URL_CONFIGURED, false) ? pref.getString(Constants.SHARED_PREF_WEB_API_URL, Constants.API_BASE_URL_1) : Constants.API_BASE_URL_1);
    }

    private void loadCurrentServerSettings() {
        Call<List<Map<String , String>>> call = Utilities.getRetrofitWebService(ServerSettingsActivity.this).getCurrentServerSettings();
        call.enqueue(new Callback<List<Map<String , String>>>() {
            @Override
            public void onResponse(Response<List<Map<String , String>>> response, Retrofit retrofit) {
                if (response.body() != null) {
                    try{
                        ((EditText) findViewById(R.id.edtTxtServer)).setText(response.body().get(0).get("server"));
                        ((EditText) findViewById(R.id.edtTxtUsername)).setText(response.body().get(0).get("username"));
                        ((EditText) findViewById(R.id.edtTxtPassword)).setText(response.body().get(0).get("password"));
                        ((EditText) findViewById(R.id.edtTxtTableName)).setText(response.body().get(0).get("dbname"));
                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}

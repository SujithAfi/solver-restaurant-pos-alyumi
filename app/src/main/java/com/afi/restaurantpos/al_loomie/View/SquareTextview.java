package com.afi.restaurantpos.al_loomie.View;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by AFI on 1/15/2016.
 */
public class SquareTextview extends TextView {
    public SquareTextview(Context context) {
        super(context);
    }

    public SquareTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareTextview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = getMeasuredWidth();
        setMeasuredDimension(width, width);
    }
}

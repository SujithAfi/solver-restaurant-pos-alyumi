package com.afi.restaurantpos.al_loomie.Adapters;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.List;


/**
 * Created by AFI on 11/25/2015.
 */
public class TableKotOptionsListAdapter extends RecyclerView.Adapter<TableKotOptionsListAdapter.TableKotOptionsListAdapterVH>{

    private final String itemType;
    List<PendingKotItem> kotNumbers;

    private boolean clickLock = false;


    private OnItemCLickListner onItemCLickListner;

    public interface OnItemCLickListner {
        void onItemClicked(int position);
        void onDeleteItem(int position);
        void onAlertClicked(int position);
        void onReorderClicked(int position);
    }



    public void setKotNumbers(List<PendingKotItem> kotNumbers) {
        this.kotNumbers = kotNumbers;
    }

    public TableKotOptionsListAdapter(List<PendingKotItem> kotNumbers , String kotorBilltype , OnItemCLickListner onItemCLickListner) {
        this.kotNumbers = kotNumbers;
        this.onItemCLickListner = onItemCLickListner;
        this.itemType = kotorBilltype;
    }

    @Override
    public TableKotOptionsListAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.simple_list_item, viewGroup, false);

        return new TableKotOptionsListAdapterVH(v);
    }

    @Override
    public void onBindViewHolder(TableKotOptionsListAdapterVH holder, final int position) {

        if(itemType.equalsIgnoreCase("KOT")) {
            holder.ivAlert.setVisibility(View.VISIBLE);
            holder.ivReorder.setVisibility(View.VISIBLE);

            if(Utilities.getSharedPreferences(holder.imgDeleteItem.getContext()).getBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_DELETE, false))
                holder.imgDeleteItem.setVisibility(View.VISIBLE);
        }
        else{

            if(Utilities.getSharedPreferences(holder.imgDeleteItem.getContext()).getBoolean(Constants.SHARED_PRE_UR_BILLING_DELETE, false))
                holder.imgDeleteItem.setVisibility(View.VISIBLE);

        }


            if(kotNumbers.get(position).Kot_No != null)
            holder.text1.setText(kotNumbers.get(position).Kot_No.replace("##" , ""));

        if(kotNumbers.get(position).Cus_Name != null && !kotNumbers.get(position).Cus_Name.matches("")) {
            holder.trCust.setVisibility(View.VISIBLE);
            holder.tvCustName.setText(kotNumbers.get(position).Cus_Name);
        }
        if(kotNumbers.get(position).Cm_Counter != null && !kotNumbers.get(position).Cm_Counter.matches("")) {
            holder.trCounter.setVisibility(View.VISIBLE);
            holder.tvCounter.setText(kotNumbers.get(position).Cm_Counter);
        }
        if(kotNumbers.get(position).Kot_Covers != null && !kotNumbers.get(position).Kot_Covers.matches("")) {
            holder.trGuest.setVisibility(View.VISIBLE);
            holder.tvGuest.setText(kotNumbers.get(position).Kot_Covers);
        }
        if(kotNumbers.get(position).Sman_Cd != null && !kotNumbers.get(position).Sman_Cd.matches("")) {
            holder.trSmanCode.setVisibility(View.VISIBLE);
            holder.tvSalesMan.setText(kotNumbers.get(position).Sman_Cd);
        }
        if(kotNumbers.get(position).Kot_TotAmt != null && !kotNumbers.get(position).Kot_TotAmt.matches("")) {
            holder.trAmount.setVisibility(View.VISIBLE);
            holder.tvTotalAmt.setText(kotNumbers.get(position).Kot_TotAmt);
        }
        if(kotNumbers.get(position).Tab_cd != null && !kotNumbers.get(position).Tab_cd.matches("")) {
            holder.trTable.setVisibility(View.VISIBLE);
            holder.tvTable.setText(kotNumbers.get(position).Tab_cd);

        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!clickLock) {
                    clickLock = true;
                    clickHandler.postDelayed(clickLockRunnable, 1000);
                    if (onItemCLickListner != null)
                        onItemCLickListner.onItemClicked(position);
                }

            }
        });

        holder.ivAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!clickLock) {
                    clickLock = true;
                    clickHandler.postDelayed(clickLockRunnable, 1000);
                    if (onItemCLickListner != null)
                        onItemCLickListner.onAlertClicked(position);
                }
            }
        });

        holder.imgDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( !clickLock ) {
                    clickLock = true;
                    clickHandler.postDelayed(clickLockRunnable , 1000);
                    if (onItemCLickListner != null )
                        onItemCLickListner.onDeleteItem(position);
                }
            }
        });

        holder.ivReorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( !clickLock ) {
                    clickLock = true;
                    clickHandler.postDelayed(clickLockRunnable , 1000);
                    if (onItemCLickListner != null )
                        onItemCLickListner.onReorderClicked(position);
                }
            }
        });


    }

    public void setOnItemCLickListner(OnItemCLickListner onItemCLickListner) {
        this.onItemCLickListner = onItemCLickListner;
    }

    @Override
    public int getItemCount() {
        return kotNumbers.size();
    }

    public static class TableKotOptionsListAdapterVH extends RecyclerView.ViewHolder {

        private final TextView tvTotalAmt;
        private final TextView tvSalesMan;
        private final TextView tvGuest;
        private final TextView tvCounter;
        private final TextView tvTable;
        private final TextView tvCustName;
        private final TableRow trTable;
        private final TableRow trAmount;
        private final TableRow trSmanCode;
        private final TableRow trGuest;
        private final TableRow trCounter;
        private final TableRow trCust;
        private final ImageView ivAlert;
        private final ImageView ivReorder;
        //        private final ImageView ivGenerateBill;
        public TextView text1;
        public ImageView imgDeleteItem;
        public TableKotOptionsListAdapterVH(View itemView) {
            super(itemView);
            text1 = (TextView) itemView.findViewById(R.id.tv_kotno);
            imgDeleteItem = (ImageView) itemView.findViewById(R.id.imgDeleteItem);
            tvTotalAmt = (TextView) itemView.findViewById(R.id.tv_totalamount);
            tvSalesMan = (TextView) itemView.findViewById(R.id.tv_salesman);
            tvGuest = (TextView) itemView.findViewById(R.id.tv_guest);
            tvCounter = (TextView) itemView.findViewById(R.id.tv_counter);
            tvTable = (TextView) itemView.findViewById(R.id.tv_table);
            tvCustName = (TextView) itemView.findViewById(R.id.tv_cutname);

            trTable = (TableRow) itemView.findViewById(R.id.tr_table);
            trAmount = (TableRow) itemView.findViewById(R.id.tr_amount);
            trSmanCode = (TableRow) itemView.findViewById(R.id.tr_salesamn);
            trGuest = (TableRow) itemView.findViewById(R.id.tr_guest);
            trCounter = (TableRow) itemView.findViewById(R.id.tr_counter);
            trCust = (TableRow) itemView.findViewById(R.id.tr_cust);


            ivAlert = (ImageView) itemView.findViewById(R.id.iv_alert);
            ivReorder = (ImageView) itemView.findViewById(R.id.iv_reorder);

         }
    }

    private Handler clickHandler = new Handler();

    private Runnable clickLockRunnable = new Runnable() {
        @Override
        public void run() {
            clickLock = false;
        }
    };

}

package com.afi.restaurantpos.al_loomie.Utills;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Models.SelectedBillDetails;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Floor;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Login;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBillItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotOnTable;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Salesman;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;


public class ApplicationSingleton {
    private static ApplicationSingleton ourInstance = new ApplicationSingleton();
    private String floor;
    private String orderType;
    private int kotType = Constants.ORDER_TYPE_DINE_IN;


    private String _API_BASE_URL_;

    private boolean lockFlag = false;

    private OrderType kotOrderType = OrderType.NEW_KOT;

    public static ApplicationSingleton getInstance() {
        return ourInstance;
    }
    private PendingKotOnTable pendingKot;
    private PendingKotOnTable tempKot;

    private boolean subCategoryClickLock;

    /**
     * Private constructor
     */
    private ApplicationSingleton() {
    }

    private String guestCount = "1";


    private  Table table;

    private List<SelectedItemDetails> mSelectedItemDetailses;
    private List<SelectedBillDetails> mSelectedBillDetailsesTemp;
    private List<SelectedItemDetails> mSelectedItemDetailsesTemp;

    private Customer customer;

    private  double mOrderTotal = 0;
    private double mOrderTotalTemp = 0;

    private Salesman salesman;

    private double tax1;
    private double tax2;
    private double tax1Temp;
    private double tax2Temp;

    private float taxFraction1 = 0;
    private float taxFraction2 = 0;

    private double grandTotal;
    private double grandTotalTemp;
    private double grandTotalWithoutTax ;


    private String kotNumber = "";
    private String newBillNumber = "";
    private Map<String, List<PendingKotItem>> selectedKotMap;
    private Map<String, List<PendingBillItem>> selectedBillMap;

    List<PendingKotItem> temporaryPendingKotItems;
    List<PendingBillItem> temporaryPendingBillItems;

    private List<Floor> floors;

    private String counterNumber;

    private Login login;

    public Date getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(Date currentDate) {
        this.currentDate = currentDate;
    }

    private Date currentDate;

    private Boolean isReservation = true;

    public Boolean getReservation() {
        return isReservation;
    }

    public void setReservation(Boolean reservation) {
        isReservation = reservation;
    }

    /**
     * Returns  the current table of the Dine in
     * @return instnce of the Table object
     */
    public  Table getTable() {
        return table;
    }

    /**
     *Save the current selected table
     * @param table table to be saved
     */
    public  void setTable(Table table) {
        this.table = table;
    }

    /**
     * Save selectedItemDetails
     * @param selectedItemDetails
     */
    public void addToSelectedItems(SelectedItemDetails selectedItemDetails){
        if(mSelectedItemDetailses == null)
            mSelectedItemDetailses = new ArrayList<>();
        mSelectedItemDetailses.add(selectedItemDetails);
        calculateTotalCost();
    }

    /**
     * Set selected item detail from list
     * @param mSelectedItemDetailses list of selected  item details
     */
    public void setSelectedItemDetailses(List<SelectedItemDetails> mSelectedItemDetailses) {
        if(mSelectedItemDetailses != null)
            this.mSelectedItemDetailses = mSelectedItemDetailses;
    }

    /**
     * Get the list of selected Item details
     * @return List of selected item details
     */
    public List<SelectedItemDetails> getSelectedItemDetails(){
        if(mSelectedItemDetailses == null)
        {
            mSelectedItemDetailses = new ArrayList<>();
            return mSelectedItemDetailses;
        }
        return  mSelectedItemDetailses;
    }

    /**
     * Delete all entries from the selected item details
     */
    public void clearSelectedItemDetails(){
        if(mSelectedItemDetailses == null)
            mSelectedItemDetailses = new ArrayList<>();
        mSelectedItemDetailses.clear();
        calculateTotalCost();
    }


    /**
     * Remove a seleected item from thee given position
     * @param position postion of the item to be deleted
     */
    public void removeFromSelectedItemDetails(int position){
        if(mSelectedItemDetailses != null){
            mSelectedItemDetailses.remove(position);
            calculateTotalCost();
        }
    }


    /**
     *
     * calculate the total iem cost from the list of the seletc item details
     *
     */
    public void calculateTotalCost(){
        mOrderTotal = 0;
        for(SelectedItemDetails details : mSelectedItemDetailses)
            mOrderTotal += details.itemTotalCost;

        tax1 = Utilities.getPercentage(mOrderTotal , taxFraction1);
        tax2 = Utilities.getPercentage(mOrderTotal , taxFraction2);

        this.grandTotalWithoutTax = mOrderTotal;
        this.grandTotal = mOrderTotal + tax1 + tax2;
    }

    /**
     * Calculate the ttal item cost from the temporary items
     */
    private void calculateTotalCostTemp(){
        mOrderTotalTemp = 0;
        for(SelectedItemDetails details : mSelectedItemDetailsesTemp)
            mOrderTotalTemp += details.itemTotalCost;

        tax1Temp = Utilities.getPercentage(mOrderTotalTemp , taxFraction1);
        tax2Temp = Utilities.getPercentage(mOrderTotalTemp , taxFraction2);

        this.grandTotalTemp = mOrderTotalTemp + tax1Temp + tax2Temp;
    }

    /**
     * Convert the pendingKot to Selected item details
     * @param pendingKot object of the pending kot
     */
    public void convertPendingKotsToSelectedItemDetails(PendingKotOnTable pendingKot ){
        this.clearSelectedItemDetails();
        List<SelectedItemDetails> selectedItemDetailses = new ArrayList<>();
        for( PendingKotItem item : pendingKot.details){
            try {
                SelectedItemDetails details = new SelectedItemDetails();
                details.itemCode = item.Itm_Cd;
                details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                details.Itm_UCost = item.KotD_UCost;
                details.itemName = item.Itm_Name;
                details.Unit_Fraction = item.Unit_Fraction;
                details.Unit_cd = item.Kot_MuUnit;
                details.barcode = item.Barcode;
                selectedItemDetailses.add(details);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        this.setSelectedItemDetailses(selectedItemDetailses);

        //TODO set rest of details
        ApplicationSingleton.getInstance().setKotNumber(pendingKot.Kot_No);
        this.setKotType(Integer.parseInt(pendingKot.Kot_Type));
        Table table = new Table();
        table.Tab_Cd = pendingKot.Tab_cd;
        this.setTable(table);
        this.setGrandTotal(Double.parseDouble(pendingKot.Kot_TotAmt));
        Customer customer = new Customer();
        customer.setId(pendingKot.Cus_Cd);
        customer.setName(pendingKot.Cus_Name);
        this.setCustomerDetails(customer);
        Salesman salesman = new Salesman();
        salesman.Id = pendingKot.Sman_Cd;
        this.setSalesman(salesman);
        this.setGuestCount(pendingKot.Kot_Covers);
        this.calculateTotalCost();
    }


    public double getTotalOrderCost(){
        return mOrderTotal;
    }
    public double getTotalOrderCostTemp(){
        return mOrderTotalTemp;
    }

    public  void setCustomerDetails(Customer customer){
        this.customer = customer;
    }
    public Customer getCustomerDetails(){
        return  this.customer;
    }

    public String getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(String guestCount) {
        this.guestCount = guestCount;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * get Currently aved salesman
     * @param context current context of the application
     * @return instance of the current salesman
     */
    public Salesman getSalesman(Context context) {
        if(this.salesman == null){
            Salesman salesman = new Salesman();
            salesman.Id = Utilities.getSharedPreferences(context).getString(Constants.SHARED_PREF_KEY_ID, "");
            salesman.Name = Utilities.getSharedPreferences(context).getString(Constants.SHARED_PREF_KEY_USERNAME, "");
            return salesman;
        }
        else
            return salesman;
    }

    public void setSalesman(Salesman salesman) {
        this.salesman = salesman;
    }

    public int getKotType() {
        return kotType;
    }

    public void setKotType(int kotType) {
        this.kotType = kotType;
    }

    public PendingKotOnTable getPendingKot() {
        return pendingKot;
    }

    public void setPendingKot(PendingKotOnTable pendingKot) {
        this.pendingKot = pendingKot;
    }

    public OrderType getKotOrderType() {
        return kotOrderType;
    }

    public void setKotOrderType(OrderType kotOrderType) {
        this.kotOrderType = kotOrderType;
    }

    public double getTax1() {
        return tax1;
    }

    public void setTax1(double tax1) {
        this.tax1 = tax1;
    }

    public double getTax2() {
        return tax2;
    }

    public void setTax2(double tax2) {
        this.tax2 = tax2;
    }

    public float getTaxFraction1() {
        return taxFraction1;
    }

    public void setTaxFraction1(float taxFraction1) {
        this.taxFraction1 = taxFraction1;
    }

    public float getTaxFraction2() {
        return taxFraction2;
    }

    public void setTaxFraction2(float taxFraction2) {
        this.taxFraction2 = taxFraction2;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getKotNumber() {
        return kotNumber;
    }

    public void setKotNumber(String kotNumber) {
        this.kotNumber = kotNumber;
    }

    public Map<String, List<PendingKotItem>> getSelectedKotMap() {
        return selectedKotMap;
    }

    public void setSelectedKotMap(Map<String, List<PendingKotItem>> selectedKotMap) {
        this.selectedKotMap = selectedKotMap;
    }
    public Map<String, List<PendingBillItem>> getSelectedBillMap() {
        return selectedBillMap;
    }
    public void setSelectedBillMap(Map<String, List<PendingBillItem>> selectedBillMap) {
        this.selectedBillMap = selectedBillMap;
    }
    public PendingKotOnTable getTempKot() {
        return tempKot;
    }

    public void setTempKot(PendingKotOnTable tempKot) {
        this.tempKot = tempKot;
    }


    public List<PendingKotItem> getTemporaryPendingKotItems() {
        return temporaryPendingKotItems;
    }

    /**
     * Save temorary pending kot items
     * <p>
     *     This item is consumed when saving the bill
     * </p>
     * @param temporaryPendingKotItems generic List of PendingKotItem
     */
    public void setTemporaryPendingKotItems(List<PendingKotItem> temporaryPendingKotItems) {
        this.temporaryPendingKotItems = temporaryPendingKotItems;

        List<SelectedItemDetails> selectedItemDetailses = new ArrayList<>();

        for(PendingKotItem item : temporaryPendingKotItems){
            try {
                SelectedItemDetails details = new SelectedItemDetails();
                details.itemCode = item.Itm_Cd;
                details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                details.Itm_UCost = item.KotD_UCost;
                details.itemName = item.Itm_Name;
                details.Unit_Fraction = item.Unit_Fraction;
                details.Unit_cd = item.Kot_MuUnit;
                details.barcode = item.Barcode;
                details.Kot_No = item.Kot_No;

                details.Cus_Name = item.Cus_Name;
                details.Cus_Cd = item.Cus_Cd;
                details.Cm_Type = item.Cm_Type;
                details.Cm_Covers = item.Cm_Covers;
                details.Sman_Cd = item.Sman_Cd;
                details.Tab_Cd = item.Tab_Cd;
                details.CmD_Amt = item.CmD_Amt;
                details.Disc_Per = item.Disc_Per;


                selectedItemDetailses.add(details);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        this.setmSelectedItemDetailsesTemp(selectedItemDetailses);



        calculateTotalCostTemp();
    }

    public List<PendingBillItem> getTemporaryPendingBillItems() {
        return temporaryPendingBillItems;
    }




    public double getTax2Temp() {
        return tax2Temp;
    }

    public void setTax2Temp(double tax2Temp) {
        this.tax2Temp = tax2Temp;
    }

    public double getGrandTotalTemp() {
        return grandTotalTemp;
    }

    public void setGrandTotalTemp(double grandTotalTemp) {
        this.grandTotalTemp = grandTotalTemp;
    }

    public double getTax1Temp() {
        return tax1Temp;
    }

    public void setTax1Temp(double tax1Temp) {
        this.tax1Temp = tax1Temp;
    }

    public double getmOrderTotalTemp() {
        return mOrderTotalTemp;
    }

    public void setmOrderTotalTemp(double mOrderTotalTemp) {
        this.mOrderTotalTemp = mOrderTotalTemp;
    }

    public void setmSelectedItemDetailsesTemp(List<SelectedItemDetails> mSelectedItemDetailsesTemp) {
        this.mSelectedItemDetailsesTemp = mSelectedItemDetailsesTemp;
    }

    public void setmSelectedBillDetailsesTemp(List<SelectedBillDetails> mSelectedBillDetailsesTemp) {
        this.mSelectedBillDetailsesTemp = mSelectedBillDetailsesTemp;
    }

    public List<SelectedItemDetails> getmSelectedItemDetailsesTemp() {
        return mSelectedItemDetailsesTemp;
    }

    public String getNewBillNumber() {
        return newBillNumber;
    }

    public void setNewBillNumber(String newBillNumber) {
        this.newBillNumber = newBillNumber;
    }


    public String getCounterNumber() {
        return counterNumber;
    }

    public void setCounterNumber(String counterNumber) {
        this.counterNumber = counterNumber;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public boolean isLockFlag() {
        return lockFlag;
    }

    public void setLockFlag(boolean lockFlag) {
        this.lockFlag = lockFlag;
    }

    public double getGrandTotalWithoutTax() {
        return grandTotalWithoutTax;
    }

    public String get_API_BASE_URL_() {
        return _API_BASE_URL_;
    }

    public void set_API_BASE_URL_(String _API_BASE_URL_) {
        this._API_BASE_URL_ = _API_BASE_URL_;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public void setFloors(List<Floor> floors) {
        this.floors = floors;
    }

    public boolean isSubCategoryClickLock() {
        return subCategoryClickLock;
    }

    public void setSubCategoryClickLock(boolean subCategoryClickLock) {
        this.subCategoryClickLock = subCategoryClickLock;
    }
}

package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Models.KotAlert;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Alert kitchen to modify Items dialog
 */
public class KotAlertDialog extends DialogFragment implements View.OnClickListener {

    private PendingKot pendingKot;
    private Toolbar toolbar;
    private ViewGroup mContainer;
    private View mLayout;
//    private EditText edtTxtReason;
    private CancelType cancelType;
    private CancelBillAndKotDialogListner dialogListner;
    private TextView tv5Min;
    private TextView tv10Min;
    private TextView tv15Min;
    private static String FIRE = "FIRE.......!!";
    private static String STOP = "PENDING ITEMS CANCELED!!";
   // private static String DELAY5MIN = "DELAY 5 MINUTES!!";
    private static String DELAY10MIN = "DELAY 10 MINUTES!!";
    private static String DELAY15MIN = "DELAY 15 MINUTES!!";
    private String alertText;
    private TextView tvFire;
    private TextView tvStop;


    public interface CancelBillAndKotDialogListner {
        void onFinishedAction(KotAlertDialog cancelBillAndKotDialog);
    }

    public static KotAlertDialog newInstance(CancelType cancelType) {
        KotAlertDialog fragment = new KotAlertDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.cancelType = cancelType;
        return fragment;
    }

    public KotAlertDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.kot_alert_dialog, container, false);
        toolbar = (Toolbar)mLayout.findViewById(R.id.toolbar);
        tvFire = (TextView) mLayout.findViewById(R.id.tv_fire);
        tvStop = (TextView) mLayout.findViewById(R.id.tv_stop);
        //tv5Min = (TextView) mLayout.findViewById(R.id.tv_5min);
        tv10Min = (TextView) mLayout.findViewById(R.id.tv_10min);
        tv15Min = (TextView) mLayout.findViewById(R.id.tv_15min);
//        edtTxtReason = (EditText) mLayout.findViewById(R.id.EditTextReason);


        //Register Onclick Listeners
        tvFire.setOnClickListener(this);
        tvStop.setOnClickListener(this);
       // tv5Min.setOnClickListener(this);
        tv10Min.setOnClickListener(this);
        tv15Min.setOnClickListener(this);

        switch (cancelType) {
            case ALERT:
                toolbar.setTitle(R.string.kot_alert);
                toolbar.setSubtitle(String.format("KOT number: %s", pendingKot.Kot_No.replace("##" , "")));
                break;
        }
        return mLayout;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setPositiveButton("OK" , null)
                .setNegativeButton("Cancel", null)
                .create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(false);
    }

    public KotAlertDialog setPendingKot(PendingKot pendingKot) {
        this.pendingKot = pendingKot;
        return this;
    }


    @Override
    public void onStart()
    {
        super.onStart();
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!Utilities.isNetworkConnected(getContext()))
                        Utilities.createNoNetworkDialog(getContext());
                    else
                        PrintAlert();

                }
            });
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){


            case R.id.tv_fire:
                alertText = FIRE;
                tvFire.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.grey));
                tvStop.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv5Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv10Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv15Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                break;
            case R.id.tv_stop:
                alertText = STOP;
                tvFire.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tvStop.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.grey));
                tv5Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv10Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv15Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                break;
        /*    case R.id.tv_5min:
                alertText = DELAY5MIN;
                tvFire.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tvStop.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv5Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.grey));
                tv10Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv15Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                break;*/
            case R.id.tv_10min:
                alertText = DELAY10MIN;
                tvFire.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tvStop.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv5Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv10Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.grey));
                tv15Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                break;
            case R.id.tv_15min:
                alertText = DELAY15MIN;
                tvFire.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tvStop.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv5Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv10Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.white));
                tv15Min.setBackgroundColor(ContextCompat.getColor(getActivity().getApplicationContext() ,R.color.grey));
                break;
            default:
                break;
        }

    }

    /**
     * Send the Print action to previous activity
     */
    private void PrintAlert() {
        if(Utilities.isNetworkConnected(getActivity())){

            if(alertText != null) {

                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setCancelable(true);
                progressDialog.setIndeterminate(true);
                progressDialog.setTitle(String.format("Informing Alert"));
                progressDialog.setMessage(getContext().getResources().getString(R.string.please_wait));
                progressDialog.show();


                KotAlert cancelKotOrBill = new KotAlert();
                cancelKotOrBill.id = pendingKot.Kot_No;
                cancelKotOrBill.reson = alertText;
                cancelKotOrBill.cus_name = pendingKot.Cus_Name;
                cancelKotOrBill.table_No = pendingKot.Tab_cd;

                if (cancelType == CancelType.ALERT) {
                    Utilities.getRetrofitWebService(getContext()).kotAlert(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
                        @Override
                        public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                            progressDialog.dismiss();
                            if (response != null) {
                                if ((Boolean) response.body().get("response_code")) {
                                    new AlertDialog.Builder(getContext())
                                            .setTitle(R.string.success)
                                            .setMessage(String.format(String.format("Alert Informed Successfully")))
                                            .setCancelable(false)
                                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (dialogListner != null)
                                                        dialogListner.onFinishedAction(KotAlertDialog.this);
                                                }
                                            })
                                            .create().show();
                                } else {
                                    new AlertDialog.Builder(getContext())
                                            .setTitle(R.string.error)
                                            .setMessage("Canot make Alert")
                                            .setCancelable(false)
                                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (dialogListner != null)
                                                        dialogListner.onFinishedAction(KotAlertDialog.this);
                                                }
                                            })
                                            .create().show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            progressDialog.dismiss();
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.error)
                                    .setMessage("Network error")
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (dialogListner != null)
                                                dialogListner.onFinishedAction(KotAlertDialog.this);
                                        }
                                    })
                                    .create().show();
                        }
                    });
                }
            }
            else
                Utilities.showToast("Please select a Alert" , getActivity());
    }
        else
            Utilities.showToast(getResources().getString(R.string.internet_connection_error) , getActivity());

    }

    public KotAlertDialog setDialogListner(CancelBillAndKotDialogListner dialogListner) {
        this.dialogListner = dialogListner;
        return this;
    }
}

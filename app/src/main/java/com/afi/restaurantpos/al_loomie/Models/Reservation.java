package com.afi.restaurantpos.al_loomie.Models;

import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by afi-mac-001 on 16/06/16.
 */
public class Reservation implements Serializable {

    @SerializedName("Usr_Id")
    private String userId;

    @SerializedName("Doc_No")
    private String docNumber;

    @SerializedName("Doc_Dt")
    private String docDate;

    @SerializedName("Doc_Year")
    private String docYear;

    @SerializedName("Doc_Counter")
    private String docCounter;

    @SerializedName("Cashier_Id")
    private String cashierId;

    @SerializedName("Loc_Cd")
    private String locCode;

    @SerializedName("Div_Cd")
    private String divisionCode;

    @SerializedName("Wh_Cd")
    private String wharehouseCode;

    @SerializedName("Guest_Count")
    private String guestCount;

    @SerializedName("Res_Date")
    private String resDate;

    @SerializedName("Res_TimeFrom")
    private String reservationTimeFrom;

    @SerializedName("Res_TimeTo")
    private String reservationTimeTo;

    @SerializedName("Table_type")
    private String tableType;

    @SerializedName("Table_Cd")
    private String tableCount;

    @SerializedName("SMan_Cd")
    private String salesManCode;

    @SerializedName("Chef_Cd")
    private String chefCode;

    @SerializedName("Res_Remarks")
    private String reservationRemarks;

    @SerializedName("itemdetails")
    private List<KotItem> itemdetails = new ArrayList<KotItem>();

    @SerializedName("customer")
    private Customer customer;


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<KotItem> getItemdetails() {
        return itemdetails;
    }

    public void setItemdetails(List<KotItem> itemdetails) {
        this.itemdetails = itemdetails;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getDocYear() {
        return docYear;
    }

    public void setDocYear(String docYear) {
        this.docYear = docYear;
    }

    public String getDocCounter() {
        return docCounter;
    }

    public void setDocCounter(String docCounter) {
        this.docCounter = docCounter;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public String getLocCode() {
        return locCode;
    }

    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    public String getDivisionCode() {
        return divisionCode;
    }

    public void setDivisionCode(String divisionCode) {
        this.divisionCode = divisionCode;
    }

    public String getWharehouseCode() {
        return wharehouseCode;
    }

    public void setWharehouseCode(String wharehouseCode) {
        this.wharehouseCode = wharehouseCode;
    }



    public String getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(String guestCount) {
        this.guestCount = guestCount;
    }

    public String getResDate() {
        return resDate;
    }

    public void setResDate(String resDate) {
        this.resDate = resDate;
    }

    public String getReservationTimeFrom() {
        return reservationTimeFrom;
    }

    public void setReservationTimeFrom(String reservationTimeFrom) {
        this.reservationTimeFrom = reservationTimeFrom;
    }

    public String getReservationTimeTo() {
        return reservationTimeTo;
    }

    public void setReservationTimeTo(String reservationTimeTo) {
        this.reservationTimeTo = reservationTimeTo;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getTableCount() {
        return tableCount;
    }

    public void setTableCount(String tableCount) {
        this.tableCount = tableCount;
    }

    public String getSalesManCode() {
        return salesManCode;
    }

    public void setSalesManCode(String salesManCode) {
        this.salesManCode = salesManCode;
    }

    public String getChefCode() {
        return chefCode;
    }

    public void setChefCode(String chefCode) {
        this.chefCode = chefCode;
    }

    public String getReservationRemarks() {
        return reservationRemarks;
    }

    public void setReservationRemarks(String reservationRemarks) {
        this.reservationRemarks = reservationRemarks;
    }
}

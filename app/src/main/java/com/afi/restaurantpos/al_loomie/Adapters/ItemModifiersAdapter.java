package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.CallBacks.RecyclerViewCallBack;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

public class ItemModifiersAdapter extends RecyclerView.Adapter<ItemModifiersAdapter.ItemModifiersAdapterViewHolder>{
Context context;

    private List<ItemModifiers> modifiers;


    private onItemClickCallBack itemClickCallback;



    public interface onItemClickCallBack{
        void onItemSelected(ItemModifiers itemModifiers);
    }

    public ItemModifiersAdapter(onItemClickCallBack itemClickCallback) {
        this.modifiers = new ArrayList<>();
        this.itemClickCallback = itemClickCallback;
    }

    public ItemModifiersAdapter(onItemClickCallBack itemClickCallback, List<ItemModifiers> modifiers) {
        this.modifiers = modifiers;
        this.itemClickCallback = itemClickCallback;
    }

    @Override
    public ItemModifiersAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        context=viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_item_modifiers, viewGroup, false);
        ItemModifiersAdapterViewHolder viewHolder = new ItemModifiersAdapterViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemModifiersAdapterViewHolder holder, int position) {
        holder.closeBtn.setOnClickListener(new Listner(position));
        if(!modifiers.get(position).rate.trim().equals("0"))
        holder.itemName.setText(modifiers.get(position).name+" ("+ Utilities.getDefaultCurrencyFormat(Double.parseDouble(modifiers.get(position).rate),context)+")");
        else
            holder.itemName.setText(modifiers.get(position).name);
    }

    @Override
    public int getItemCount() {
        if(modifiers == null)
            return 0;
        else
            return modifiers.size();
    }


    public void addToItemModifiers(ItemModifiers itemModifiers){
        if(modifiers == null)
            modifiers = new ArrayList<>();
        modifiers.add(itemModifiers);
    }

    public static class ItemModifiersAdapterViewHolder extends RecyclerView.ViewHolder{


        private TextView itemName;
        private ImageView closeBtn;
        public ItemModifiersAdapterViewHolder(View itemView) {
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.txtModifierName);
            closeBtn = (ImageView) itemView.findViewById(R.id.closeBtn);
        }
    }

    private class Listner extends RecyclerViewCallBack{

        public Listner(int position) {
            super(position);
        }

        @Override
        public void onItemClick(int position) {
            ItemModifiers data = modifiers.get(position);
            removeFromList(position);
            if(itemClickCallback != null)
                itemClickCallback.onItemSelected(data);

        }
    }

    private void removeFromList(int position) {
        modifiers.remove(position);
        this.notifyDataSetChanged();
    }

    public List<ItemModifiers> getModifiers() {
        return modifiers;
    }
}

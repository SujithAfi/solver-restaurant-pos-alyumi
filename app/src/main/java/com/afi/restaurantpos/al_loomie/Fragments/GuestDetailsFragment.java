package com.afi.restaurantpos.al_loomie.Fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.CustomerSearchAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.CreateCustomerDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.SelectSalesmanDialogFragment;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Salesman;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.DatabaseHandler;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Fragment to display Guest details
 */
public class GuestDetailsFragment extends Fragment {

    List<String> usernames;
    List<Customer> customerList;
    FloatingActionButton fabNewCustomer;
    private AutoCompleteTextView txtSearchBox;
    private Button btnSelectFromList , btnNewCustomer , btnPlus , btnMinus;
    private TextView txtUserName , txtUserAddress , txtUserDiscount;
    AdapterView.OnItemClickListener itemClickListner = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            modifyCUstomerDetails((Customer) txtSearchBox.getAdapter().getItem(position));

        }
    };
    private Button btnBarcode;
    private Button btnCamera;
    private Button btnProceed;
    private EditText  edtTxtGuestNumber;
    private TextView txtId;
    private TextView txtName;
    private SelectSalesmanDialogFragment.DialogCallback selectSalesmanDialogCalback = new SelectSalesmanDialogFragment.DialogCallback() {
        @Override
        public void onPossitiveButtonClick(int indexOfData) {
            Salesman salesman = ((OrderDetails) getActivity()).getSalesmans().get(indexOfData);
            txtId.setText(salesman.Id);
            txtName.setText(salesman.Name);
            ApplicationSingleton.getInstance().setSalesman(salesman);
        }

        @Override
        public void onNegativeButtonClick() {

        }
    };
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                if (v == btnSelectFromList) {
                    android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    SelectSalesmanDialogFragment fragment = new SelectSalesmanDialogFragment();
                    fragment.setSalesmans(((OrderDetails) getActivity()).getSalesmans());
                    fragment.setDialogCallback(selectSalesmanDialogCalback);
                    fragment.show(fragmentManager, "sfsdfdsdsfds");
                } else if (v == fabNewCustomer) {
                    android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    CreateCustomerDialog dialog = new CreateCustomerDialog();
                    dialog.setCreateCustomerListner(new CreateCustomerDialog.CreateCustomerListner() {
                        @Override
                        public void onCustomerSelection(boolean isInternal, String id) {
                            loadCustomerNames(isInternal, id);
                        }
                    });
                    dialog.show(fragmentManager, "sel");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private View.OnClickListener buttonAddSubListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                if (v == btnMinus && edtTxtGuestNumber.getText().toString().length() != 0) {
                    if (Integer.parseInt(edtTxtGuestNumber.getText().toString()) > 1)
                        edtTxtGuestNumber.setText((Integer.parseInt(edtTxtGuestNumber.getText().toString()) - 1) + "");
                } else if (v == btnPlus && edtTxtGuestNumber.getText().toString().length() != 0) {
                    edtTxtGuestNumber.setText((Integer.parseInt(edtTxtGuestNumber.getText().toString()) + 1) + "");
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    };

    public GuestDetailsFragment() {

    }

    public static GuestDetailsFragment newInstance() {
        GuestDetailsFragment fragment = new GuestDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_guest_details, container, false);
        txtSearchBox = (AutoCompleteTextView)v.findViewById(R.id.EditTextSearch);
        txtUserName = (TextView)v.findViewById(R.id.txtNameSelectedCustomer);
        txtUserAddress = (TextView)v.findViewById(R.id.txtAddressSelecteddCustomer);
        txtUserDiscount = (TextView)v.findViewById(R.id.txtDiscountSelectedCustomer);
        fabNewCustomer = ((FloatingActionButton) v.findViewById(R.id.fabNewCustomer));
        fabNewCustomer.setOnClickListener(onClickListener);
        btnProceed = (Button) v.findViewById(R.id.btnProceed);

        btnBarcode =(Button)v.findViewById(R.id.btnBarcode);
        btnCamera =(Button)v.findViewById(R.id.btnCamera);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Table table = new Table();
                table.Name = "";
                table.Type_Index = 0;
                table.Caption = "";
                table.Guests = 0;
                table.book = 0;
                table.bill = 0;
                table.Tab_Cd = "";
                ((OrderDetails) getActivity()).onTableSelected(table);
            }
        });
        prepareCustomerNames(false, null);

        SharedPreferences preferences = Utilities.getDefaultSharedPref(getContext());
        if(preferences.getString("key_scan_type" , "1").equals("1")){
            btnCamera.setVisibility(View.GONE);
            btnBarcode.setVisibility(View.GONE);
        }
        else if(preferences.getString("key_scan_type" , "1").equals("2")){
            btnCamera.setVisibility(View.VISIBLE);
            btnBarcode.setVisibility(View.GONE);
        }
        else if(preferences.getString("key_scan_type" , "1").equals("3")){
            btnCamera.setVisibility(View.GONE);
            btnBarcode.setVisibility(View.VISIBLE);
        }

    }

    public void prepareCustomerNames(boolean needAutoselect , String id) {
        this.customerList = new DatabaseHandler(getContext()).getCustomers();
        if ( needAutoselect) {

            Customer customer = null;
            for( Customer customer1 : customerList){
                if(customer1.getId().equals(id)) {
                    customer = customer1;
                    break;
                }
            }

            if( customer != null) {
                ApplicationSingleton.getInstance().setCustomerDetails(customer);
                modifyCUstomerDetails(customer);
            }
            else  {
                Customer emptyCustomer = new Customer();
                emptyCustomer.setId("");
                emptyCustomer.setName("");
                emptyCustomer.setAddress1("");
                emptyCustomer.setDiscount(0);

                ApplicationSingleton.getInstance().setCustomerDetails(emptyCustomer);
            }
        }
        else  {
            Customer emptyCustomer = new Customer();

            ApplicationSingleton.getInstance().setCustomerDetails(emptyCustomer);
        }


        if(this.customerList != null ){
            usernames = new ArrayList<>();
            CustomerSearchAdapter searchAdapter = new CustomerSearchAdapter(getContext(),customerList);
            txtSearchBox.setAdapter(searchAdapter);
            txtSearchBox.setThreshold(0);
            txtSearchBox.setOnItemClickListener(itemClickListner);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    /**
     * Modify the form values
     * @param data
     */
    private void modifyCUstomerDetails(Customer data) {
//        txtUserName.setText(data.name);
        /*txtUserDiscount.setText(Utilities.getItemQuantityFormat(data.discount)+ "");
        txtUserAddress.setText(data.address1 );
*/
        saveCustomerDetails(data);
    }

    /**
     * Save the customer details
     * @param data
     */
    private void saveCustomerDetails(Customer data) {
        ApplicationSingleton.getInstance().setCustomerDetails(data);
    }

    /**
     * Find index of the selced item from auto complete textview
     *
     * @param name
     * @param selectedIndexOfList
     * @return
     */
    private int findIndexOfData(String name , int selectedIndexOfList) {
        int apparentIndex = 0;
        int realIndex = -1;
        if(usernames == null)
            return -1;
        else{
            for(int i = 0 ; i < usernames.size() ; i ++){
                String data = usernames.get(i);
                if(data.equals(name))
                {
                    apparentIndex = i;
                    break;
                }
                else
                    continue;
            }
            //get all match

            List<Integer> sameNamesInList = new ArrayList<>();

            for(int i = 0 ; i < usernames.size() ; i++)
            {
                String nameIn = usernames.get(i);
                if(nameIn.equals(name)){
                    sameNamesInList.add(i);
                }
            }
            //if only 1 result apparent Index will be the real index

            if(sameNamesInList.size() == 1)
                realIndex = apparentIndex;
            else
            {
                realIndex = sameNamesInList.get(selectedIndexOfList);
            }
            return realIndex;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            loadCustomerNames(false, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    /**
     * Load the customer details
     *
     * @param needAutoSelect
     * @param id
     */
    public void loadCustomerNames(final boolean needAutoSelect, final String id) {

        if (!Utilities.isNetworkConnected(this.getContext()))
            Utilities.createNoNetworkDialog(this.getContext());
        else {
            try {
                Utilities.getRetrofitWebService(getContext()).getCustomers().enqueue(new Callback<List<Customer>>() {
                    @Override
                    public void onResponse(Response<List<Customer>> response, Retrofit retrofit) {
                        if (response.body() != null && GuestDetailsFragment.this.isVisible()) {
                            try {
                                List<Customer> customers = response.body();

                                new DatabaseHandler(getContext()).addToCustomers(customers);
                                prepareCustomerNames(false, id);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

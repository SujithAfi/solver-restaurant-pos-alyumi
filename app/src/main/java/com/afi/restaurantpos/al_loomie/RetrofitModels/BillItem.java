package com.afi.restaurantpos.al_loomie.RetrofitModels;


public class BillItem {
     public String Itm_cd;
     public String Menu_SideDish;
     public String Cmd_ItmName;
     public String BarCode;
     public String Unit_Fraction;
     public String Cmd_Qty;
     public String Unit_Cd;
     public String Cmd_MuUnit;
     public String Cmd_MuQty;
     public String Cmd_MuFraction;
     public String Cmd_MuConvToBase;
     public String Cmd_MuUcost;
     public String Cmd_Rate;
     public String Cmd_DiscPer;
     public String Cmd_DiscAmt;
     public String Cmd_Amt;
     public String Cmd_Ctr;
     public String Cmd_SubCtr;
     public String Cmd_RowType;
     public String Cmd_NotCosted;
     public String Cmd_Cancelled;
     public String Cmd_IsLzQty;
     public String Sft_No;
     public String Sft_Dt;
     public String Cmd_Status;
     public String Cm_ThruDo;
     public String IsModified;
     public String Cm_Ref;
     public String Cmd_DiscReason;
     public String Kot_No;
     public String Rcp_Cd;
     public String Cmd_IsBuffet;
     public String Cmd_Ucost;
}

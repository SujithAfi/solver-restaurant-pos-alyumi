package com.afi.restaurantpos.al_loomie;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by afi-mac-001 on 19/08/16.
 */
public class ShowPdfActivity extends AppCompatActivity implements OnPageChangeListener, OnLoadCompleteListener {

    private Toolbar toolbar;
    private ProgressDialog mProgressDialog;
    private PDFView pdfView;
    private String itemName;
    private String itemCode;
    private static String pdfFolderName = "Solver Restaurant";
    private String pdfUrl = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        pdfView = (PDFView) findViewById(R.id.pdfView);


        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        pdfUrl = Utilities.getWebUrl(this).replace("api" , "");

        Bundle b = getIntent().getBundleExtra("bundle");

        if(b != null){

            itemName = b.getString("itemName");
            itemCode = b.getString("itemCode");


            if(itemName != null)
                toolbar.setTitle(itemName + "  Recipe");

            if(itemCode != null){

                File sdCard = Environment.getExternalStorageDirectory();
                File fileDir = new File(sdCard.getAbsolutePath() + "/" + pdfFolderName);
                if (fileDir.exists()) {

                    File file = new File(sdCard.getAbsolutePath() + "/" + pdfFolderName + "/" + itemCode + ".pdf");
                    if (file.exists())
                        showPdfFromStorage();
                    else
                        downloadItemPdf();

                } else {
                    fileDir.mkdirs();
                    downloadItemPdf();
                }

            }
            else{

                Toast.makeText(ShowPdfActivity.this, "Item Not Exist", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    private void downloadItemPdf() {

        mProgressDialog = new ProgressDialog(ShowPdfActivity.this);
        mProgressDialog.setMessage("Downloading Recipe");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(true);

        final DownloadTask downloadTask = new DownloadTask(ShowPdfActivity.this);
        if(pdfUrl != null && itemCode != null)
            downloadTask.execute(pdfUrl + itemCode + ".pdf");
        else {
            Toast.makeText(ShowPdfActivity.this, "Item Not Found", Toast.LENGTH_SHORT).show();
            finish();
        }

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                downloadTask.cancel(true);
            }
        });
    }

    @Override
    public void onPageChanged(int page, int pageCount) {

    }

    @Override
    public void loadComplete(int nbPages) {

    }

    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }


                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                File sdCard = Environment.getExternalStorageDirectory();
                File futureStudioIconFile = new File(sdCard.getAbsolutePath() + "/" + pdfFolderName + "/" + itemCode + ".pdf");
                output = new FileOutputStream(futureStudioIconFile);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            if (result != null) {
                Toast.makeText(context, "Item Not Found", Toast.LENGTH_LONG).show();
                finish();
            }
            else
                showPdfFromStorage();

        }
    }


    private void showPdfFromStorage() {

        File sdCard = Environment.getExternalStorageDirectory();
        File filedir = new File(sdCard.getAbsolutePath() + "/" + pdfFolderName + "/" + itemCode + ".pdf");

        pdfView.fromFile(filedir)
                .defaultPage(0)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }


}

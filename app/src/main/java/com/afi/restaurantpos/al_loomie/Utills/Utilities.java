package com.afi.restaurantpos.al_loomie.Utills;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import java.text.DecimalFormat;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitControllers.WebApiEndPoint;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by AFI on 9/28/2015.
 */
public class Utilities {


    /**
     * This method displays a Snackbar with the message passed to the method
     * @param messsage message to be displayed
     * @param activity current activity instance
     * @return void
     */
    public static void showSnackBar(String messsage , Activity activity){
        try {
            Snackbar snack = Snackbar.make(activity.findViewById(android.R.id.content), messsage, Snackbar.LENGTH_LONG);
            View view = snack.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)view.getLayoutParams();
            params.gravity = Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM;
            view.setLayoutParams(params);
            snack.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * Returns the Shared prefrence object for data persistence.
     * @param context Current application context
     * @return SharedPreferences Shared prefrence object
     */
    public static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }
    public static SharedPreferences.Editor getSharedPreferencesEditor(Context context){
        return context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE).edit();
    }

    /**
     * Hides softkeyboard focused to a View
     * @param activity current activity
     */
    public static void hideKeybord(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Show a Toast message
     * @param message message to be displayed
     * @param context current Application context
     */
    public static void showToast(String message , Context context){
        Toast.makeText(context , message , Toast.LENGTH_LONG).show();
    }

    /**
     * Get default shared preferences for data persistence.
     * <p>
     *     The shared preferences which is accessing by the application settings
     * </p>
     * @param context current Application context
     * @return SharedPreferences object
     */
    public static SharedPreferences getDefaultSharedPref(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Get url to load the image of items.
     * <p>
     *     Url will used by picasso image loader
     * </p>
     * @param itemcode
     * @param barcode
     * @return
     */
    public static String getImageUrl(String itemcode , String barcode){
        return ApplicationSingleton.getInstance().get_API_BASE_URL_() + "/image_path?itemcode=" + itemcode + "&Barcode=" + barcode;
    }

    /**
     * Convert the integer string to default currency format using by the application
     * <p>
     *     The default currency format is #,###.000
     * </p>
     * @param value value to be converted to currency format
     * @param context current Application context
     * @return String formatted currency value
     */
    public static String getDefaultCurrencyFormat(String value , Context context){

       return new DecimalFormat(context.getSharedPreferences(Constants.SHARED_PREF_NAME , Context.MODE_PRIVATE)
               .getString(Constants.SHARED_PREF_KEY_CURRENCY_FORMAT, "#,###.000"))
               .format(Double.parseDouble(value));
    }

    /**
     * Convert the Double value to default currency format using by the application
     * <p>
     *     The default currency format is #,###.000
     * </p>
     * @param value value to be converted to currency format
     * @param context current Application context
     * @return String formatted currency value
     */
    public static String getDefaultCurrencyFormat(Double value, Context context){
        return new DecimalFormat(context.getSharedPreferences(Constants.SHARED_PREF_NAME , Context.MODE_PRIVATE)
                .getString(Constants.SHARED_PREF_KEY_CURRENCY_FORMAT, "#,###.000"))
                .format(value);
    }

    /**
     * Convert the number to the kot format.
     * <p>
     *     The kot format is be ## followed by integers
     * </p>
     * @param kotNumber unformated number
     * @return String formated kot number string
     */
    public static String getKotFormat(String kotNumber){
        return  "" + new DecimalFormat("##000000").format(Double.parseDouble(kotNumber + 1));
    }

    /**
     * Remove decimal fraction from a double value
     * @param value value to be converted
     * @return String with removed decimal fraction
     */
    public static String getItemQuantityFormat(Double value){
        return new DecimalFormat("####0").format(value);
    }

    /**
     * Remove decimal fraction from a String value
     * @param value value to be converted
     * @return String with removed decimal fraction
     */
    public static String getItemQuantityFormat(String value){
        return new DecimalFormat("####0").format(Double.parseDouble(value));
    }

    /**
     * Escpe all characters on a string
     * @param string to be esacped
     * @return escaped String
     */
    public static String quote(String string) {
        if (string == null || string.length() == 0) {
            return "\"\"";
        }

        char         c = 0;
        int          i;
        int          len = string.length();
        StringBuilder sb = new StringBuilder(len + 4);
        String       t;

        sb.append('"');
        for (i = 0; i < len; i += 1) {
            c = string.charAt(i);
            switch (c) {
                case '\\':
                case '"':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '/':
                    //                if (b == '<') {
                    sb.append('\\');
                    //                }
                    sb.append(c);
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                default:
                    if (c < ' ') {
                        t = "000" + Integer.toHexString(c);
                        sb.append("\\u" + t.substring(t.length() - 4));
                    } else {
                        sb.append(c);
                    }
            }
        }
        sb.append('"');
        return sb.toString();
    }

    /**
     * Returns Percentage of the  total value
     * @param total total value
     * @param percentage percentage
     * @return percentage of the total value
     * @see
     */
    public static double getPercentage(double total , double percentage){
        if(percentage == 0)
            return 0;
        if(total == 0)
            return 0;
        return  (percentage * total) / 100;
    }

    /**
     * Method to find Tax1
     * @param context current Application context
     * @param total total value
     * @return tax1 value of the toatal amount
     */
    public static double findTax1(Context context , double total ){

        SharedPreferences preferences = getSharedPreferences(context);
        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_1_ENABLED , false)){
            double val = Double.parseDouble(preferences.getString(Constants.SHARED_PREF_KEY_TAX_1, "0"));
            return val == 0 ? 0 : getPercentage(total , val);
        }
        else {
            return  0;
        }
    }

    /**
     * Method to get tax1 fraction from SharedPreferences
     * @param context current Application context
     * @return tax1 percentage
     */
    public static double getTaxFraction1(Context context){
        SharedPreferences preferences = getSharedPreferences(context);
        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_1_ENABLED , false)){
            return Double.parseDouble(preferences.getString(Constants.SHARED_PREF_KEY_TAX_1, "0"));
        }
        else {
            return  0;
        }
    }

    /**
     * Method to find Tax2
     * @param context current Application context
     * @param total total value
     * @return tax2 value of the toatal amount
     */
    public static double findTax2(Context context , double total ){
        SharedPreferences preferences = getSharedPreferences(context);
        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_2_ENABLED , false)){
            double val = Double.parseDouble(preferences.getString(Constants.SHARED_PREF_KEY_TAX_2, "0"));
            return val == 0 ? 0 : getPercentage(total , val);
        }
        else {
            return  0;
        }
    }

    /**
     * Method to get tax2 fraction from SharedPreferences
     * @param context current Application context
     * @return tax2 percentage
     */
    public static double getTaxFraction2(Context context){
        SharedPreferences preferences = getSharedPreferences(context);
        if(preferences.getBoolean(Constants.SHARED_PREF_KEY_TAX_2_ENABLED , false)){
            return Double.parseDouble(preferences.getString(Constants.SHARED_PREF_KEY_TAX_2, "0"));
        }
        else {
            return  0;
        }
    }


    /**
     * Get unique Device Id of the current device
     * @param context current Application context
     * @return device id
     */
    public static String getDeviceId(Context context){
        return android.provider.Settings.Secure.getString(context.getContentResolver(),
                android.provider.Settings.Secure.ANDROID_ID);
    }


    /**
     * Blur an Image
     *
     * @param sentBitmap image t be buared
     * @param scale scale fraction from 0f to 1f
     * @param radius radius to be bured
     * @return blured image according to the variables passed
     */
    public static Bitmap fastblur(Bitmap sentBitmap, float scale, int radius) {

        int width = Math.round(sentBitmap.getWidth() * scale);
        int height = Math.round(sentBitmap.getHeight() * scale);
        sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = ( 0xff000000 & pix[yi] ) | ( dv[rsum] << 16 ) | ( dv[gsum] << 8 ) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }


    /**
     * Save base URL on the <a href="http://www.javaworld.com/article/2073352/core-java/simply-singleton.html">Singleton</a> class
     * @param context current application context
     * @see <a href="http://www.javaworld.com/article/2073352/core-java/simply-singleton.html">Navigate the deceptively simple Singleton pattern
    </a>
     *
     */
    public static void initURL( Context context ) {

        SharedPreferences preferences = getSharedPreferences(context);
        if (preferences.getBoolean(Constants.SHARED_PREF_WEB_API_URL_CONFIGURED , false))
            ApplicationSingleton.getInstance().set_API_BASE_URL_( preferences.getString(Constants.SHARED_PREF_WEB_API_URL , Constants.API_BASE_URL_1));
        else
            ApplicationSingleton.getInstance().set_API_BASE_URL_( Constants.API_BASE_URL_1 );
    }

    /**
     * Customize statusbar for devices running with Android kitkat version
     * @param activity Current Activity
     */
    public static void initKitkatStatusbarTransparancy( Activity activity ) {
        if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {

            SystemBarTintManager tintManager = new SystemBarTintManager(activity);
            // enable status bar tint
            tintManager.setStatusBarTintEnabled(true);
            // enable navigation bar tint
            tintManager.setNavigationBarTintEnabled(true);
            tintManager.setTintColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        }
    }


    /**
     * This method checks whether mobile is connected to internet and returns true if connected.
     *
     * @param context Application Context
     * @return true if connected
     */
    public static boolean isNetworkConnected(Context context) {
        return ((ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    /**
     * Create the alertdialog for no network available
     * @param context
     * @return
     */
    public static void createNoNetworkDialog (Context context) {
        try {
            new AlertDialog.Builder(context)
                    .setTitle("Network not connected")
                    .setMessage("Please check your Network connection")
                    .setPositiveButton("OK" , null)
                    .setCancelable(true)
                    .create()
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method returns the Retrofit client for HTTP requests.
     *
     * @return RestAdapter retrofit HTTP client
     * @see <a href="http://square.github.io/retrofit/">Retrofit</a>
     */
    public static WebApiEndPoint getRetrofitWebService(Context context) {

        String apiUrl = "";
        SharedPreferences preferences = getSharedPreferences(context);
        if (preferences.getBoolean(Constants.SHARED_PREF_WEB_API_URL_CONFIGURED , false))
            apiUrl=  preferences.getString(Constants.SHARED_PREF_WEB_API_URL , Constants.API_BASE_URL_1);
        else
            apiUrl =  Constants.API_BASE_URL_1 ;
        return new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebApiEndPoint.class);
    }

    /**
     * Replace the fragment with the animation if needed
     *
     * @param fragment
     * @param tag
     * @param fragmentManager
     */
    public static void replaceFragmentWithAnimation(Fragment fragment, String tag, FragmentManager fragmentManager) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(tag);
        if (fragmentManager.getBackStackEntryCount() != 0)
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(R.id.frameContainer, fragment, tag).commit();
    }

    public static void replaceFragmentWithAnimation1(Fragment fragment, String tag, FragmentManager fragmentManager , int id) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(tag);
        if (fragmentManager.getBackStackEntryCount() != 0)
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(id, fragment, tag).commit();
    }

    public static String getWebUrl (Context context){

        String apiUrl = "";
        SharedPreferences preferences = getSharedPreferences(context);

        if (preferences.getBoolean(Constants.SHARED_PREF_WEB_API_URL_CONFIGURED , false))
            apiUrl=  preferences.getString(Constants.SHARED_PREF_WEB_API_URL , Constants.API_BASE_URL_1);
        else
            apiUrl =  Constants.API_BASE_URL_1 ;

        return  apiUrl;
    }

}

package com.afi.restaurantpos.al_loomie.Models;

import java.io.Serializable;

/**
 * Created by afi-mac-001 on 13/08/16.
 */
public class CreditAccount implements Serializable {

    private String Id;
    private String Name;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}

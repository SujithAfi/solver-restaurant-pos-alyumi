package com.afi.restaurantpos.al_loomie.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.PayAdapter;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by afi on 10/22/2015.
 */
public class PayFragment  extends Fragment {

    private RecyclerView rVBills;
    private PayAdapter PayAdapter;
    private TextView txtTotalCost;

    private TextView txtTax1;
    private TextView txtTax2;
    private TextView txtGrandTotal;

    private boolean isTemporaryMode;

    public static PayFragment newInstance( ) {
        PayFragment fragment = new PayFragment();
        return fragment;
    }

    public PayFragment() {
        PayAdapter = new PayAdapter();
        PayAdapter.setSelectedItemDetailses(ApplicationSingleton.getInstance().getSelectedItemDetails());
    }


    /**
     * Set data
     * @param data
     */
    public void setData(List<SelectedItemDetails> data){
        PayAdapter.setSelectedItemDetailses(data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pay, container, false);
        rVBills = (RecyclerView)view.findViewById(R.id.rVBill);
        txtTotalCost = (TextView) view.findViewById(R.id.txtBillTotal);
        txtGrandTotal = (TextView) view.findViewById(R.id.txtBillGrandTotal);
        txtTax1 = (TextView) view.findViewById(R.id.txtBillTax);
        txtTax2 = (TextView) view.findViewById(R.id.txtBillTax2);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rVBills.setHasFixedSize(true);
        rVBills.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rVBills.setAdapter(PayAdapter);
        if(isTemporaryMode) {
            txtTotalCost.setText(Utilities.getDefaultCurrencyFormat(ApplicationSingleton.getInstance().getTotalOrderCostTemp(), getContext()));
            try {
                txtTax1.setText(Utilities.getDefaultCurrencyFormat(ApplicationSingleton.getInstance().getTax1Temp(), getContext()));
                txtTax2.setText(Utilities.getDefaultCurrencyFormat(ApplicationSingleton.getInstance().getTax2Temp(), getContext()));
                txtGrandTotal.setText(Utilities.getDefaultCurrencyFormat(ApplicationSingleton.getInstance().getGrandTotalTemp(), getContext()));
            } catch (Exception e) {

            }
        }
        else{
            txtTotalCost.setText(Utilities.getDefaultCurrencyFormat(ApplicationSingleton.getInstance().getTotalOrderCost(), getContext()));
            try {
                txtTax1.setText(Utilities.getDefaultCurrencyFormat(ApplicationSingleton.getInstance().getTax1(), getContext()));
                txtTax2.setText(Utilities.getDefaultCurrencyFormat(ApplicationSingleton.getInstance().getTax2(), getContext()));
                txtGrandTotal.setText(Utilities.getDefaultCurrencyFormat(ApplicationSingleton.getInstance().getGrandTotal(), getContext()));
            } catch (Exception e) {

            }
        }
    }

    public void setIsTemporaryMode(boolean isTemporaryMode) {
        this.isTemporaryMode = isTemporaryMode;
    }

    public List<SelectedItemDetails> getAllSelectedItemDetailses(){
        return this.PayAdapter.getSelectedItemDetailses();
    }

}
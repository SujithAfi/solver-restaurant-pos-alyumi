package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.CallBacks.ItemClickCallback;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by AFI on 10/7/2015.
 */
public class BillAdapter extends RecyclerView.Adapter<BillAdapter.BillAdapterVH>{

    private List<SelectedItemDetails> selectedItemDetailses;
    private ItemClickCallback itemClickCallback;
    private onItemChange itemChange;
    private Context context;
    private boolean isNewOrderBill = false;

    public BillAdapter(Context context) {
        this.context = context;
        selectedItemDetailses = new ArrayList<>();
    }

    public interface onItemChange{
        void onItemInvalidate();
    }
    public BillAdapter(List<SelectedItemDetails> selectedItemDetailses , ItemClickCallback callback) {
        this.selectedItemDetailses = selectedItemDetailses;
        this.itemClickCallback = callback;
    }

    @Override
    public BillAdapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.child_bill_fragment_list, viewGroup, false);
        BillAdapterVH billAdapterVH = new BillAdapterVH(v);
        return billAdapterVH;
    }

    @Override
    public void onBindViewHolder(BillAdapterVH holder, int position) {
        holder.itemView.setOnClickListener(new ItemClickListner(position, selectedItemDetailses.get(position).itemName));
        holder.txtItemName.setText(selectedItemDetailses.get(position).itemName);
        holder.txtItemQuantity.setText(Utilities.getItemQuantityFormat(selectedItemDetailses.get(position).itemQuantity));
        holder.txtItemRate.setText(Utilities.getDefaultCurrencyFormat(selectedItemDetailses.get(position).itemUnitCost + "", holder.txtItemAmount.getContext()));
        holder.txtItemAmount.setText(Utilities.getDefaultCurrencyFormat(selectedItemDetailses.get(position).itemTotalCost + "", holder.txtItemAmount.getContext()));
        holder.position = position;

    }

    @Override
    public int getItemCount() {
        if(selectedItemDetailses == null)
            return  0;
        return selectedItemDetailses.size();
    }


    public void addToBillDetails(SelectedItemDetails selectedItemDetails){
        selectedItemDetailses.add(selectedItemDetails);
        this.notifyDataSetChanged();
    }

    public static class BillAdapterVH extends RecyclerView.ViewHolder{
        public int position;
        private TextView txtItemName;
        private TextView txtItemQuantity;
        private TextView txtItemRate;
        private TextView txtItemAmount;
        private TableRow layoutt;

        public BillAdapterVH(View itemView) {
            super(itemView);
            txtItemName = (TextView) itemView.findViewById(R.id.txtItemName);
            txtItemQuantity = (TextView) itemView.findViewById(R.id.txtItemQuantity);
            txtItemRate = (TextView) itemView.findViewById(R.id.txtItemRate);
            txtItemAmount = (TextView) itemView.findViewById(R.id.txtItemAmount);
            layoutt= (TableRow) itemView.findViewById(R.id.row);
        }
    }

    public void setSelectedItemDetailses(List<SelectedItemDetails> selectedItemDetailses) {
        this.selectedItemDetailses = selectedItemDetailses;
    }

    public List<SelectedItemDetails> getSelectedItemDetailses(){
        return this.selectedItemDetailses;
    }

    private class ItemClickListner implements View.OnClickListener{
        private int position ;

        private String Id;

        public ItemClickListner(int position, String id) {
            this.position = position;
            Id = id;

        }

        @Override
        public void onClick(View v) {
            if (itemClickCallback != null)
                itemClickCallback.onItemSelected(position, Id);
            if(isNewOrderBill){

                final SelectedItemDetails details = selectedItemDetailses.get(position);

                new MaterialDialog.Builder(v.getContext())
                        .title("Do you want to change quantity of " + details.itemName + " ?")
                        .content("Input new quantity")
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .cancelable(false)
                        .inputRange(1, 3)
                        .positiveText("Ok")
                        .negativeText("Cancel")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {

                                EditText e = materialDialog.getInputEditText();
                            }
                        })
                        .input("Input new quantity", Utilities.getItemQuantityFormat(details.itemQuantity), new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // Do something
                                EditText e = dialog.getInputEditText();
                                double value = Double.parseDouble(input.toString());
                                if (value != details.itemQuantity) {
                                    details.itemQuantity = value;
                                    details.itemTotalCost = value * details.itemUnitCost;
                                    ApplicationSingleton.getInstance().calculateTotalCost();
                                    notifyDataSetChanged();
                                    if (itemChange != null)
                                        itemChange.onItemInvalidate();


                                }

                            }
                        }).show();
            }

        }
    }

    public void removeItemAt(int position){
        if (isNewOrderBill) {


            selectedItemDetailses.remove(position);
            ApplicationSingleton.getInstance().calculateTotalCost();
            notifyDataSetChanged();
            if (itemChange != null) {
                itemChange.onItemInvalidate();
            }

        }
    }
    public void setItemChange(onItemChange itemChange) {
        this.itemChange = itemChange;
    }

    public void setIsNewOrderBill1(boolean isNewOrderBill) {
        this.isNewOrderBill = isNewOrderBill;
    }

    public double getTotalCost(){
        double total = 0;
        for( SelectedItemDetails details : selectedItemDetailses){
            total += details.itemTotalCost;
        }
        return total;
    }
}

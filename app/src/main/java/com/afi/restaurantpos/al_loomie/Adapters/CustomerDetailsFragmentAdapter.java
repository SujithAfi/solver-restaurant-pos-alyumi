package com.afi.restaurantpos.al_loomie.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.afi.restaurantpos.al_loomie.Fragments.CustomerFragment;
import com.afi.restaurantpos.al_loomie.Fragments.CustomerHistoryFragment;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;

/**
 * Created by afi-mac-001 on 13/06/16.
 */
public class CustomerDetailsFragmentAdapter extends FragmentPagerAdapter {

    private Boolean isEditable = false;
    private Customer customer;

    public CustomerDetailsFragmentAdapter(FragmentManager fm, Customer customer , Boolean isEditable) {
        super(fm);
        this.customer = customer;
        this.isEditable = isEditable;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new CustomerFragment().setCustomer(customer , isEditable);
            case 1:
                return new CustomerHistoryFragment().setCustomer(customer);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Details";
            case 1:
                return "History";
            default:
                return super.getPageTitle(position);
        }
    }
}

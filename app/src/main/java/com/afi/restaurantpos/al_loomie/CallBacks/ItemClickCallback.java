package com.afi.restaurantpos.al_loomie.CallBacks;

/**
 * Created by AFI on 10/5/2015.
 * interface for the ItemClickCallback
 */
public interface ItemClickCallback {

    void onItemSelected(int position, String id);

}

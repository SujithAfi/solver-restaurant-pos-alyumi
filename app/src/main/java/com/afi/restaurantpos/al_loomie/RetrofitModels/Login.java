package com.afi.restaurantpos.al_loomie.RetrofitModels;

import java.util.List;

/**
 * Created by AFI on 9/29/2015.
 */
public class Login {
    public int loginResponse;
    public String loginResponseString;
    public String sessionId;
    public String Usr_Name;
    public String Id;
    public String format;
    public String tax1;
    public String tax2;
    public String tax1_en;
    public String tax2_en;
    public int shift_id;
    public int shift_no;
    public String shift_date;
    public String shift_time;
    public String SlaesId;
    public String Shift;
    public String Counter;
    public String Remarks ;
    public String float_cash ;
    public boolean user_type;
    public List<User_Rights> UserRights;

}

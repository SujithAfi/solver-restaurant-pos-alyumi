package com.afi.restaurantpos.al_loomie.Utills;

/**
 * Created by AFI on 9/28/2015.
 * Exeption class to describe validation error
 */
public class ValidateExeption extends Exception {

    /**
     * Constructor to create exception message
     * @param detailMessage details about the validation error
     */
    public ValidateExeption(String detailMessage) {
        super(detailMessage);
    }
}

package com.afi.restaurantpos.al_loomie.RetrofitModels;

/**
 * Created by afi-mac-001 on 27/09/16.
 */

public class User_Rights {

    public String Rights_Caption ;
    public String Rights_Text;

    public String getRights_Caption() {
        return Rights_Caption;
    }

    public void setRights_Caption(String rights_Caption) {
        Rights_Caption = rights_Caption;
    }

    public String getRights_Text() {
        return Rights_Text;
    }

    public void setRights_Text(String rights_Text) {
        Rights_Text = rights_Text;
    }


}

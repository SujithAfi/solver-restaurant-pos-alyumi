package com.afi.restaurantpos.al_loomie;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


import com.afi.restaurantpos.al_loomie.Fragments.PayFragment;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotOnTable;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by afi on 10/22/2015.
 */
public class PaymentDetailsActivity extends AppCompatActivity {

    PayFragment PayFragment;

    private boolean modifyPayMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_details);
        Utilities.initURL(this);
        Utilities.initKitkatStatusbarTransparancy(this);
        try {
            modifyPayMode = getIntent().getBooleanExtra("modifyPayMode" , false);
        }
        catch (Exception e){

        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        PayFragment = new PayFragment();
        processPaydata();
        getSupportFragmentManager().beginTransaction().replace(R.id.pay_container, PayFragment, "bill").commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);




    }

    private void processPaydata() {


        if(!modifyPayMode) {
            PendingKotOnTable pendingKot = ApplicationSingleton.getInstance().getPendingKot();
            List<SelectedItemDetails> selectedItemDetailses = new ArrayList<>();
            for (PendingKotItem item : pendingKot.details) {
                try {
                    SelectedItemDetails details = new SelectedItemDetails();
                    details.itemCode = item.Itm_Cd;
                    details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                    details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                    details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                    details.Itm_UCost = item.KotD_UCost;
                    details.itemName = item.Itm_Name;
                    details.Unit_Fraction = item.Unit_Fraction;
                    details.Unit_cd = item.Kot_MuUnit;
                    details.barcode = item.Barcode;
                    selectedItemDetailses.add(details);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            PayFragment.setData(selectedItemDetailses);
            PayFragment.setIsTemporaryMode(false);
        }
        else {
            List<SelectedItemDetails> selectedItemDetailses = new ArrayList<>();
            for (PendingKotItem item :ApplicationSingleton.getInstance().getTemporaryPendingKotItems()) {
                try {
                    SelectedItemDetails details = new SelectedItemDetails();
                    details.itemCode = item.Itm_Cd;
                    details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                    details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                    details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                    details.Itm_UCost = item.KotD_UCost;
                    details.itemName = item.Itm_Name;
                    details.Unit_Fraction = item.Unit_Fraction;
                    details.Unit_cd = item.Kot_MuUnit;
                    details.barcode = item.Barcode;
                    selectedItemDetailses.add(details);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
            PayFragment.setData(selectedItemDetailses);
            PayFragment.setIsTemporaryMode(true);
        }
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            finish();
        return true;
    }
}

package com.afi.restaurantpos.al_loomie.Dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.CreateUser;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by AFI on 10/1/2015.
 * Dialog to create the customer
 */
public class CreateCustomerDialog extends DialogFragment{

    private ViewGroup mContainer;
    private View mLayout;
    private Toolbar toolbar;

    private EditText edtTxtTelephone , edtTxtName , edtTxtAddress , edtTxtDiscount , edtTxtTravelTime;

    private CreateCustomerListner createCustomerListner;

    public CreateCustomerDialog() {
    }

    public CreateCustomerDialog setCreateCustomerListner(CreateCustomerListner createCustomerListner) {
        this.createCustomerListner = createCustomerListner;
        return this;
    }

    @Override
    public void onStart()
    {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (!Utilities.isNetworkConnected(getContext()))
                        Utilities.createNoNetworkDialog(getContext());
                    else
                        validateFormData();


                }
            });
        }
    }

    /**
     * Validate the data on the form
     */
    private void validateFormData() {

        String telephone  = edtTxtTelephone.getText().toString();
        String name = edtTxtName.getText().toString();
        String address = edtTxtAddress.getText().toString();
        String discount = edtTxtDiscount.getText().toString();
        String trael_distance = edtTxtTravelTime.getText().toString();

        if(telephone.length() == 0) {
            edtTxtTelephone.setError(getResources().getString(R.string.enter_the_telephone));
            edtTxtTelephone.requestFocus();
        }
        else if(name.length() == 0) {
            edtTxtName.setError(getResources().getString(R.string.enter_the_name));
            edtTxtName.requestFocus();
        }
        else if(address.length() == 0) {
            edtTxtAddress.setError(getResources().getString(R.string.enter_the_address));
            edtTxtAddress.requestFocus();
        }
        else if(discount.length() == 0) {
            edtTxtDiscount.setError(getResources().getString(R.string.enter_the_discount));
            edtTxtDiscount.requestFocus();
        }
        else if(trael_distance.length() == 0) {
            edtTxtTravelTime.setError(getResources().getString(R.string.enter_the_travel_time));
            edtTxtTravelTime.requestFocus();
        }
        else {

            try {
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage(getContext().getResources().getString(R.string.creating_user));
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();

                Utilities.getRetrofitWebService(getContext()).createCustomer(telephone, name, address, ";", discount, trael_distance).enqueue(new Callback<CreateUser>() {
                    @Override
                    public void onResponse(Response<CreateUser> response, Retrofit retrofit) {
                        try {
                            progressDialog.cancel();
                            if (response.body() != null && CreateCustomerDialog.this.isVisible()) {
                                CreateUser createUser = response.body();
                                if (createUser.response_code) {
                                    Utilities.showToast("User created succesfully", getContext());
                                    createCustomerListner.onCustomerSelection(true, edtTxtTelephone.getText().toString());
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            CreateCustomerDialog.this.dismiss();
                                        }
                                    }, 1000 * 1);
                                } else {
                                    Utilities.showToast("User already exists", getContext());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        try {
                            progressDialog.cancel();
                            Utilities.showToast("Network error...!", getContext());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }

        }


    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.dialog_create_customer, container, false);
        toolbar = (Toolbar)mLayout.findViewById(R.id.toolbar);
        toolbar.setTitle("Create Customer");
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CreateCustomerDialog.this.dismiss();
            }
        });
        edtTxtName = (EditText)mLayout.findViewById(R.id.EditTextName);
        edtTxtTelephone = (EditText)mLayout.findViewById(R.id.EditTextTelephone);
        edtTxtAddress = (EditText)mLayout.findViewById(R.id.EditTextAddress);
        edtTxtDiscount = (EditText)mLayout.findViewById(R.id.EditTextDiscount);
        edtTxtTravelTime = (EditText)mLayout.findViewById(R.id.EditTextTravelelTime);

        return mLayout;
    }



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setPositiveButton("OK" , null)
                .setNegativeButton("Cancel", null)
                .create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(true);
    }

    public interface CreateCustomerListner {
        void onCustomerSelection(boolean isInternal, String id);
    }
}

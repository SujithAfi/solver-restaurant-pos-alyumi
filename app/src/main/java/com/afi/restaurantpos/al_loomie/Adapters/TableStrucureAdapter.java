package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.CallBacks.RecyclerViewCallBack;


import com.afi.restaurantpos.al_loomie.Dialogs.TableActionDetailsFragment;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;

import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import org.w3c.dom.Text;

/**
 * Created by AFI on 10/2/2015.
 */
public class TableStrucureAdapter extends RecyclerView.Adapter<TableStrucureAdapter.TableAdpapterVH>{


    private Context context;

    private android.support.v4.app.FragmentManager fragmentManager;

    private List<Table> tables;
    private List<Reservation> reservations;
    private Map<Table, Integer> reservationMap = new HashMap<>();

    private boolean clickLock = false;


    private Map<Integer , Table> tableMap;
    private TableSelectListner tableSelectListner;
    private Handler clickHandler = new Handler();
    private Runnable clickLockRunnable = new Runnable() {
        @Override
        public void run() {
            clickLock = false;
        }
    };


    public TableStrucureAdapter(List<Table> tables,  List<Reservation> reservations , TableSelectListner tableSelectListner, android.support.v4.app.FragmentManager supportFragmentManager) {
        this.tables = tables;
        tableMap = new HashMap<>();
        this.tableSelectListner = tableSelectListner;
        this.fragmentManager = supportFragmentManager;
        this.reservations = reservations;
        initTableMap(tables, reservations);

        processTableDetils();
    }

    private void initTableMap(List<Table> tables, List<Reservation> reservations) {

        tableMap.clear();

        for (Table table : tables) {
            tableMap.put(table.Type_Index, table);
        }

        for (Reservation reservation : reservations) {
            for (Table table : tables) {
                if (reservation.getTableCount().equals(table.getTab_Cd())) {
                    if (reservationMap.containsKey(table)) {
                        Integer count = reservationMap.get(table);
                        count++;
                        reservationMap.put(table, count);
                    } else {
                        reservationMap.put(table, 1);
                    }
                }
            }
        }
    }

    private void processTableDetils() {
        tableMap.clear();

        if(tables == null)
            return;
        for(Table table : tables)
            tableMap.put(table.Type_Index , table);

    }

    @Override
    public TableAdpapterVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.table_view, viewGroup, false);
        TableAdpapterVH vh = new TableAdpapterVH(v);
        if (context == null)
            context = viewGroup.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(TableAdpapterVH holder, int position) {


        if(tableMap.containsKey(position)){
            holder.itemView.setVisibility(View.VISIBLE);

            Table table = tableMap.get(position);
            holder.tvTableName.setText(table.Caption);
            holder.tvChairs.setText(table.chairs);
            if(Utilities.getSharedPreferences(context).getBoolean(Constants.SHARED_PRE_UR_TABLELAYOUT_ADDORSAVE, false))
                holder.cv_container.setOnClickListener(new ItemSelectListner(position));
            else
                Toast.makeText(context , "You don't have permission to access" , Toast.LENGTH_LONG).show();

            if (tableMap.get(position).book == 1 && tableMap.get(position).bill == 1)
                holder.cv_container.setCardBackgroundColor(ContextCompat.getColor(context, R.color.tableStatusPinkEnd));
            else if (tableMap.get(position).book == 1 && tableMap.get(position).bill != 1)
                holder.cv_container.setCardBackgroundColor(ContextCompat.getColor(context, R.color.tableStatusRedEnd));
            else if (tableMap.get(position).book != 1 && tableMap.get(position).bill == 1)
                holder.cv_container.setCardBackgroundColor(ContextCompat.getColor(context, R.color.tableStatusYelloStart));
            else
                holder.cv_container.setCardBackgroundColor(ContextCompat.getColor(context, R.color.table_normal));

            if (reservationMap.containsKey(table)) {
                if (reservationMap.get(table).intValue() == 1) {
                    holder.txtReservation.setVisibility(View.VISIBLE);
                    holder.txtMoreReservation.setVisibility(View.INVISIBLE);
                } else if (reservationMap.get(table).intValue() > 1) {
                    holder.txtReservation.setVisibility(View.VISIBLE);
                    holder.txtMoreReservation.setVisibility(View.VISIBLE);
                } else {
                    throw new RuntimeException("Bad logic");
                }




            } else {
                holder.txtReservation.setVisibility(View.INVISIBLE);
                holder.txtMoreReservation.setVisibility(View.INVISIBLE);
            }


        }
        else {
            holder.itemView.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return 81;
    }

    public interface TableSelectListner {
        void onTableSelected(Table table, boolean needToClearSelectedItems);

        void onKotRequest(String id, int option, Table table);
    }

    public static class TableAdpapterVH extends RecyclerView.ViewHolder {

        private final CardView cv_container;
        private final TextView tvChairs;
        private TextView tvTableName;
        private TextView txtMoreReservation;
        private TextView txtReservation;

        public TableAdpapterVH(View itemView) {
            super(itemView);

            tvTableName = (TextView) itemView.findViewById(R.id.tv_name);
            txtMoreReservation = (TextView) itemView.findViewById(R.id.txtMoreReservation);
            txtReservation = (TextView) itemView.findViewById(R.id.txtReservation);
            cv_container = (CardView) itemView.findViewById(R.id.card_view);
            tvChairs = (TextView) itemView.findViewById(R.id.tv_chairs);

        }
    }

    private class ItemSelectListner extends RecyclerViewCallBack{
        public ItemSelectListner(int position) {
            super(position);
        }

        @Override
        public void onItemClick(int position) {
            if (!Utilities.isNetworkConnected(context))
                Utilities.createNoNetworkDialog(context);
            else {
                if (!clickLock) {
                    clickLock = true;
                    clickHandler.postDelayed(clickLockRunnable, 1000);
                    final Table table = tableMap.get(position);
                    if (table != null) {
                        ApplicationSingleton.getInstance().clearSelectedItemDetails();
                        TableActionDetailsFragment.newInstance(table, reservations , tables).show(fragmentManager, "Thsjkd");


                    }
                }
            }


        }
    }



}

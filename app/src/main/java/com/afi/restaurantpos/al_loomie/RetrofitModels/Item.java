package com.afi.restaurantpos.al_loomie.RetrofitModels;

import com.afi.restaurantpos.al_loomie.Models.Ingredients;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AFI on 10/2/2015.
 * <p>
 *     POJO class for ITEM
 * </p>
 */
public class Item implements Serializable {
    public String ItemCd;
    public String BarCd;
    public String Item_Name;
    public String Rate;
    public boolean Cmd_IsBuffet;
    public boolean Is_MultiCourse;
    public ItemDetails itemDetails;
    public String Remarks;
    public int itemCount = -1;
    public List<Ingredients> Ingredients = new ArrayList<Ingredients>();
    public  String modifiedItemDetails;
    public boolean Is_TempItem;
    public String modificationRemarks;

    public String getModificationRemarks() {
        return modificationRemarks;
    }

    public void setModificationRemarks(String modificationRemarks) {
        this.modificationRemarks = modificationRemarks;
    }

    public List<Ingredients> getIngredients() {
        return Ingredients;
    }

    public void setIngredients(List<Ingredients> Ingredients) {
        this.Ingredients = Ingredients;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }


    public boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }



}

package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Models.StartShift;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Login;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 *
 * Fragment to show Shift management
 */
public class ShiftManagementDialog extends DialogFragment implements View.OnClickListener {

    private Login login;
    private ViewGroup mContainer;
    private View mLayout;

    private Toolbar toolbar;
    private Button btnStartShift;
    private Button btnEndShift;
    private TextView txtShiftId;
    private TextView txtShiftNo;
    private TextView txtStartDateAndTime;
    private TextView txtshiftDate;
    private TextView txtCashier;
    private EditText txtRemarks;
    private EditText txtFloatCash;

    private ShiftManagementDialogCallback dialogCallback;

    private String siftId;

    public static ShiftManagementDialog newInstance(ShiftManagementDialogCallback callback , Login login , String shiftId) {
        ShiftManagementDialog fragment = new ShiftManagementDialog();
        fragment.dialogCallback = callback;
        fragment.login = login;
        fragment.siftId = shiftId;
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ShiftManagementDialog() {
        // Required empty public constructor
    }

    public interface ShiftManagementDialogCallback {
        void onShiftStarted();
        void onCanceled();
        void onShiftEnded(ShiftManagementDialog shiftManagementDialog);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.dialog_shift_management, container, false);
        toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);

        txtShiftId = (TextView) mLayout.findViewById(R.id.txtShiftid);
        txtShiftNo = (TextView) mLayout.findViewById(R.id.txtShiftNo);
        txtStartDateAndTime = (TextView) mLayout.findViewById(R.id.txtStartDateAndTime);
        txtshiftDate = (TextView) mLayout.findViewById(R.id.txtshiftDate);
        txtCashier = (TextView) mLayout.findViewById(R.id.txtCashier);
        txtRemarks = (EditText) mLayout.findViewById(R.id.txtRemarks);
        txtFloatCash = (EditText) mLayout.findViewById(R.id.txtFloatCasha);
        btnStartShift = (Button) mLayout.findViewById(R.id.btnStartShift);
        btnEndShift = (Button) mLayout.findViewById(R.id.btnEndShift);

        return mLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(false);
        toolbar.setTitle(R.string.shift_management);
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShiftManagementDialog.this.dismiss();
            }
        });
        txtFloatCash.setText(login.float_cash + "");
        txtShiftId.setText(login.shift_id + "");
        txtShiftNo.setText(login.shift_no == 0 ? "1" : login.shift_no + "");
        txtStartDateAndTime.setText(login.shift_time + "");
        txtshiftDate.setText(login.shift_date.toString());
        txtCashier.setText(login.SlaesId + login.Usr_Name);
        txtRemarks.setText("");


        btnStartShift.setOnClickListener(this);
        btnEndShift.setOnClickListener(this);

        if(login.Shift.equals("CLOSED") || login.Shift.equals("CLOSE") || login.Shift.length() == 0) {
            btnEndShift.setEnabled(false);
            btnStartShift.setEnabled(true);
        }
        else {
            btnEndShift.setEnabled(true);
            btnStartShift.setEnabled(false);
        }

        toolbar.setSubtitle("Counter : " + siftId);

        txtRemarks.setEnabled(!login.Shift.equals("OPEN"));
        txtRemarks.setText( login.Remarks == null ? "" : login.Remarks);


        try {

            DateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            Date shiftDate = inputDateFormat.parse(login.shift_date);
            Date shiftTime = inputDateFormat.parse(login.shift_time);

            DateFormat shiftDateFormat = new SimpleDateFormat("dd-MMM-yyyy" , Locale.ENGLISH);
            DateFormat shiftTimeFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss" , Locale.ENGLISH);

            txtStartDateAndTime.setText(shiftTimeFormat.format(shiftTime));
            txtshiftDate.setText(shiftDateFormat.format(shiftDate));

        }
        catch (Exception e) {

        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setCancelable(false)
                .create();
    }

    @Override
    public void onClick(View v) {

        if ( v == btnStartShift ) {

            if (!Utilities.isNetworkConnected(getContext()))
                Utilities.createNoNetworkDialog(getContext());
            else {


                try {
                    login.Counter = siftId;
                    login.Shift = "OPEN";
                    login.Remarks = txtRemarks.getText().toString();
                    ApplicationSingleton.getInstance().setLogin(login);
                    final AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                            .setTitle(R.string.starting_shift)
                            .setMessage(R.string.please_wait)
                            .setCancelable(false)
                            .create();
                    StartShift startShift = new StartShift();
                    startShift.id = siftId + "";
                    startShift.shift_No = login.shift_no == 0 ? "1" : login.shift_no + "";
                    startShift.cashier = login.SlaesId + "";
                    startShift.emp_cd = login.SlaesId + "";
                    startShift.emp_name = login.Usr_Name + "";
                    startShift.remarks = txtRemarks.getText().toString();
                    startShift.float_cash = txtFloatCash.getText().toString();

                    alertDialog.show();
                    Utilities.getRetrofitWebService(getContext()).startShift(startShift).enqueue(new Callback<Map<String, Object>>() {
                        @Override
                        public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                            alertDialog.dismiss();
                            if ((Boolean) response.body().get("response_code") && ShiftManagementDialog.this.isVisible()){
                                try {
                                    AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                            .setTitle(R.string.shift_started)
                                            .setCancelable(false)
                                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialogCallback.onShiftStarted();
                                                }
                                            })
                                            .create();
                                    alertDialog.show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            else {
                                try {
                                    AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                            .setTitle(R.string.error)
                                            .setCancelable(false)
                                            .setMessage("Shift can't started")
                                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialogCallback.onShiftStarted();
                                                }
                                            })
                                            .create();
                                    alertDialog.show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            alertDialog.dismiss();
                            AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.error)
                                    .setCancelable(false)
                                    .setMessage("Network error...!")
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialogCallback.onShiftStarted();
                                        }
                                    })
                                    .create();
                            alertDialog.show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }
        else if ( v == btnEndShift ) {

            if (!Utilities.isNetworkConnected(getContext()))
                Utilities.createNoNetworkDialog(getContext());
            else
                closeShift();


        }

    }

    /**
     * Close the shift
     */
    private void closeShift() {

        try {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setIndeterminate(true);
            progressDialog.setTitle(R.string.closing_shift);
            progressDialog.setMessage(getResources().getString(R.string.please_wait));
            progressDialog.setCancelable(false);

            progressDialog.show();
            Utilities.getRetrofitWebService(getContext()).stopShift(ApplicationSingleton.getInstance().getCounterNumber(),txtFloatCash.getText().toString()).enqueue(new Callback<Map<String, Object>>() {
                @Override
                public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                    progressDialog.dismiss();

                    if ((Boolean) response.body().get("response_code") && ShiftManagementDialog.this.isVisible()) {
                        try {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.success)
                                    .setMessage(R.string.shift_ended)
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Utilities.getSharedPreferences(getContext())
                                                    .edit()
                                                    .putBoolean(Constants.SHARED_PREF_KEY_AUTHENTICATED, false)
                                                    .apply();
                                            dialogCallback.onShiftEnded(ShiftManagementDialog.this);

                                        }
                                    }).create().show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.error)
                                    .setMessage((String) response.body().get("insertResponse"))
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, null).create().show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    progressDialog.dismiss();

                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.error)
                            .setMessage("Network error...!")
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, null).create().show();
                }
            });
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

    }



}

package com.afi.restaurantpos.al_loomie.Fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import com.afi.restaurantpos.al_loomie.Adapters.SearchResultAdapter;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.SearchResult;
import com.afi.restaurantpos.al_loomie.SelectItemsActivity;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Fragment to show Serch results
 */
public class SearchFragment extends Fragment implements  SearchResultAdapter.onClickListner {

    private ProgressBar searchRunningProgress;

    private FloatingActionButton fabSaveBill;

    public boolean clickLock = false;

    private RecyclerView rVSearchResult;

    private SearchResultAdapter searchResultAdapter;

    /**
     * factory method to create instance of SearchFragment
     * @return
     */
    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        searchRunningProgress = (ProgressBar) view.findViewById(R.id.searchRunningProgress);
        rVSearchResult = (RecyclerView) view.findViewById(R.id.rVSearchResult);
        fabSaveBill = (FloatingActionButton) view.findViewById(R.id.fabSaveBill);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rVSearchResult.setHasFixedSize(true);
        rVSearchResult.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        if(searchResultAdapter == null) {
            searchResultAdapter = new SearchResultAdapter();
            searchResultAdapter.setOnClickListner(this);
        }
        rVSearchResult.setAdapter(searchResultAdapter);
        fabSaveBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ((SelectItemsActivity)getActivity()).saveKot();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void searchWithQuery(String query){

        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {

            if (query.length() > 0) {
                try {
                    searchRunningProgress.setVisibility(View.VISIBLE);
                    Utilities.getRetrofitWebService(getContext()).searchWithKey(query, ApplicationSingleton.getInstance().getCounterNumber()).enqueue(new Callback<List<SearchResult>>() {
                        @Override
                        public void onResponse(Response<List<SearchResult>> response, Retrofit retrofit) {
                            if(SearchFragment.this.isVisible()) {
                                searchRunningProgress.setVisibility(View.GONE);
                                List<SearchResult> searchResults = response.body();

                                if (searchResultAdapter == null) {
                                    searchResultAdapter = new SearchResultAdapter();
                                    searchResultAdapter.setOnClickListner(SearchFragment.this);
                                }
                                searchResultAdapter.setSearchResults(searchResults);
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            searchRunningProgress.setVisibility(View.GONE);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }



    @Override
    public void onClick(SearchResult searchResult) {

        if ( !clickLock ) {
            clickLock = true;
            try {
                ((SelectItemsActivity) getActivity()).showItemDetailsFragment(searchResult.ItemCd, searchResult.BarCd , this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

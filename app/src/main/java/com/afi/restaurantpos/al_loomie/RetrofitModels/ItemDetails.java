package com.afi.restaurantpos.al_loomie.RetrofitModels;

import java.io.Serializable;
import java.util.List;

/**
 * Created by AFI on 10/5/2015.
 * <p>
 *     POJO class for ItemDetails
 * </p>
 */
public class ItemDetails implements Serializable{

    public String ItemCd;
    public String Item_Name;
    public String path;
    public String price;
    public List<ItemModifiers> modify;

    public String Itm_UCost;
    public String Unit_Fraction;
    public String Unit_cd;
    public String Cmd_IsBuffet;
    public String Menu_SideDish;

}

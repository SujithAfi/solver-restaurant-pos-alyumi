package com.afi.restaurantpos.al_loomie.Fragments;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.afi.restaurantpos.al_loomie.Models.CancelKotOrBill;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.Models.ReservationContainer;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;
import com.afi.restaurantpos.al_loomie.SelectItemsActivity;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationDetailsFragment extends Fragment implements View.OnClickListener {


    private TextView txtreservationNumber;
    private TextView txtdate;
    private TextView txtTime;

    private EditText edtTxtGuestNumber;
    private TextView txtTableName;
    private TextView txtCustomerCode;
    private TextView txtCustomerName;
    private Reservation reservation;
    private Handler uiHandler = new Handler();

    private Date selectedReservationDate = null;
    private Date selectedReservationTime = null;
    private Button btnCancel;
    private Button btnEdit;
    private ProgressDialog progressDialog;
    private Customer customer;
    private OnHeadlineSelectedListener mCallback;
    private Button btnItems;
    private boolean fromResvAct = false;
    private Button btnMakeKot;
    private TextView tvHabits;
    private CardView cvHabits;
    private TextView tvDate3Desc;
    private TextView tvdate3;
    private LinearLayout lldate3;
    private TextView tvDate2Desc;
    private TextView tvdate2;
    private LinearLayout lldate2;
    private TextView tvDate1Desc;
    private TextView tvdate1;
    private LinearLayout lldate1;
    private CardView cvImptDates;
    private TextView tvFav3;
    private LinearLayout llFav3;
    private TextView tvFav2;
    private LinearLayout llFav2;
    private TextView tvFav1;
    private LinearLayout llFav1;
    private CardView cvFavorites;
    private TextView tvAllergy;
    private CardView cvAllergy;


    // Container Activity must implement this interface
    public interface OnHeadlineSelectedListener {
        public void onArticleSelected();
    }

    public ReservationDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnHeadlineSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            fromResvAct  = getArguments().getBoolean("ResvAct" , false);
            reservation = (Reservation) getArguments().getSerializable("reservation");
            if(reservation != null){
                if(reservation.getCustomer() != null)
                    customer = reservation.getCustomer();

            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.reservation_details, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        txtreservationNumber = (TextView) getView().findViewById(R.id.txtreservationNumber);
        txtdate = (TextView) getView().findViewById(R.id.txtdate);
        txtTime = (TextView) getView().findViewById(R.id.txtTime);

        txtTableName = (TextView) getView().findViewById(R.id.txtTableName);
        txtCustomerCode = (TextView) getView().findViewById(R.id.txtCustomerCode);
        txtCustomerName = (TextView) getView().findViewById(R.id.txtCustomerName);

        edtTxtGuestNumber = (EditText) getView().findViewById(R.id.edtTxtGuestNumber);

        btnCancel = (Button) getView().findViewById(R.id.btn_cancel);
        btnEdit = (Button) getView().findViewById(R.id.btn_edit);
        btnItems = (Button) getView().findViewById(R.id.btn_items);
        btnMakeKot = (Button) getView().findViewById(R.id.btn_makekot);

        tvHabits = (TextView) getView().findViewById(R.id.tv_habit);
        cvHabits = (CardView) getView().findViewById(R.id.cv_habits);

        tvAllergy = (TextView) getView().findViewById(R.id.tv_allergy);
        cvAllergy = (CardView) getView().findViewById(R.id.cv_allergy);

        tvDate3Desc = (TextView) getView().findViewById(R.id.tv_date3desc);
        tvdate3 = (TextView) getView().findViewById(R.id.tv_date3);
        lldate3 = (LinearLayout) getView().findViewById(R.id.ll_date3);
        tvDate2Desc = (TextView) getView().findViewById(R.id.tv_date2desc);
        tvdate2 = (TextView) getView().findViewById(R.id.tv_date2);
        lldate2 = (LinearLayout) getView().findViewById(R.id.ll_date2);
        tvDate1Desc = (TextView) getView().findViewById(R.id.tv_date1disc);
        tvdate1 = (TextView) getView().findViewById(R.id.tv_date1);
        lldate1 = (LinearLayout) getView().findViewById(R.id.ll_date1);
        cvImptDates = (CardView) getView().findViewById(R.id.cv_impdates);


        tvFav3 = (TextView) getView().findViewById(R.id.tv_favorite3);
        llFav3 = (LinearLayout) getView().findViewById(R.id.ll_fav3);
        tvFav2 = (TextView) getView().findViewById(R.id.tv_favorite2);
        llFav2 = (LinearLayout) getView().findViewById(R.id.ll_fav2);
        tvFav1 = (TextView) getView().findViewById(R.id.tv_favorite1);
        llFav1 = (LinearLayout) getView().findViewById(R.id.ll_fav1);
        cvFavorites = (CardView) getView().findViewById(R.id.cv_favourites);


        btnMakeKot.setVisibility(View.INVISIBLE);

        // Check Permission to Edit details
        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_RESERVATION_EDIT, false))
            btnEdit.setVisibility(View.VISIBLE);

        // Check Permission to delete reservation
        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_RESERVATION_DELETE, false))
            btnCancel.setVisibility(View.VISIBLE);

        btnCancel.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnItems.setOnClickListener(this);
        btnMakeKot.setOnClickListener(this);

        getView().findViewById(R.id.btnAdd).setOnClickListener(this);
        getView().findViewById(R.id.btnSubstract).setOnClickListener(this);

        Toolbar toolbar = (Toolbar) getView().findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


        txtdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view1) {
                final Calendar calendar = Calendar.getInstance();

                String date = txtdate.getText().toString();
                if (!date.isEmpty()) {
                    try {
                        String[] dateParts = date.split("-");
                        calendar.set(Calendar.YEAR, Integer.parseInt(dateParts[2]));
                        calendar.set(Calendar.MONTH, Integer.parseInt(dateParts[1]) - 1);
                        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateParts[0]));
                    } catch (Exception e) {

                    }
                }


                new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                        calendar.set(Calendar.YEAR, i);
                        calendar.set(Calendar.MONTH, i1);
                        calendar.set(Calendar.DAY_OF_MONTH, i2);
                        txtdate.setText(new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime()));

                        selectedReservationDate = calendar.getTime();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)).show();
            }
        });

        txtTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeBundle timeBundle = null;
                final Calendar calendar = Calendar.getInstance();
                if (view.getTag() == null) {
                    timeBundle = new TimeBundle();

                    timeBundle.hour = calendar.get(Calendar.HOUR_OF_DAY);
                    timeBundle.minute = calendar.get(Calendar.MINUTE);
                } else {
                    timeBundle = (TimeBundle) view.getTag();
                }

                final TimeBundle finalTimeBundle = timeBundle;
                new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {

                        finalTimeBundle.hour = i;
                        finalTimeBundle.minute = i1;
                        txtTime.setTag(finalTimeBundle);
                        calendar.set(Calendar.HOUR_OF_DAY, i);
                        calendar.set(Calendar.MINUTE, i1);
                        txtTime.setText(new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
                        selectedReservationTime = calendar.getTime();
                    }
                }, timeBundle.hour, timeBundle.minute, false).show();
            }
        });



        if(reservation != null){

            Calendar calDate = Calendar.getInstance();
            Calendar cal1Time = Calendar.getInstance();
            SimpleDateFormat sdfDate = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());
            SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm:ss a");

            if(reservation.getDocNumber() != null)
                txtreservationNumber.setText(reservation.getDocNumber().replace("##" , ""));
            if(reservation.getResDate() != null);
            {
                List<String> items = Arrays.asList(reservation.getResDate().split("\\s+"));

                if(items.size() > 0) {
                    try {

                        calDate.setTime(sdfDate.parse(items.get(0)));
                        txtdate.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()));
                        selectedReservationDate = calDate.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if(new SimpleDateFormat("dd-MM-yyyy").format(calDate.getTime()).equals(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()))) {
                        ViewCompat.animate(btnMakeKot)
                                .scaleX(0)
                                .scaleY(0)
                                .setDuration(100)
                                .start();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                btnMakeKot.setVisibility(View.VISIBLE);
                                btnMakeKot.clearAnimation();
                                ViewCompat.animate(btnMakeKot)
                                        .scaleX(1f)
                                        .scaleY(1f)
                                        .setDuration(300)
                                        .setInterpolator(new DecelerateInterpolator())
                                        .start();

                            }
                        }, 1000);
                    }

                }

            }
            if(reservation.getReservationTimeFrom() != null) {


                List<String> items = Arrays.asList(reservation.getReservationTimeFrom().split("\\s+"));

                if(items.size() > 0) {

                    try {
                        cal1Time.setTime(sdfTime.parse(items.get(1) + " " + items.get(2)));
                        txtTime.setText(new SimpleDateFormat("hh:mm a").format(cal1Time.getTime()));
                        selectedReservationTime = cal1Time.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

            }
            if(reservation.getGuestCount() != null)
                edtTxtGuestNumber.setText(reservation.getGuestCount());
            if(reservation.getTableCount() != null)
                txtTableName.setText(reservation.getTableCount());
            if(reservation.getCustomer() != null){

                if(reservation.getCustomer().getName() != null)
                    txtCustomerName.setText(reservation.getCustomer().getName());
                if(reservation.getCustomer().getId() != null)
                    txtCustomerCode.setText(reservation.getCustomer().getId());
                /*if(reservation.getCustomer().getCus_Phone() != null)
                    txtCustomerCode.setText(reservation.getCustomer().getCus_Phone());*/
                if(reservation.getCustomer().getCusFavorite1() != null && !reservation.getCustomer().getCusFavorite1().matches("")){

                    cvFavorites.setVisibility(View.VISIBLE);
                    tvFav1.setText(reservation.getCustomer().getCusFavorite1());

                    if(reservation.getCustomer().getCusFavorite2() != null && !reservation.getCustomer().getCusFavorite2().matches("")){

                        llFav2.setVisibility(View.VISIBLE);
                        tvFav2.setText(reservation.getCustomer().getCusFavorite2());
                    }

                    if(reservation.getCustomer().getCusFavorite3() != null && !reservation.getCustomer().getCusFavorite3().matches("")){

                        llFav3.setVisibility(View.VISIBLE);
                        tvFav3.setText(reservation.getCustomer().getCusFavorite3());
                    }



                }

                if(reservation.getCustomer().getImpDate1() != null && !reservation.getCustomer().getImpDate1().matches("") && !reservation.getCustomer().getImpDate1().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                    Calendar calDateImp = Calendar.getInstance();
                    SimpleDateFormat sdfDateImp = new SimpleDateFormat("M/d/yyyy", Locale.getDefault());

                    cvImptDates.setVisibility(View.VISIBLE);

                    List<String> items = Arrays.asList(reservation.getCustomer().getImpDate1().split("\\s+"));

                    if(items.size() > 0) {
                        try {

                            calDateImp.setTime(sdfDateImp.parse(items.get(0)));
                            tvdate1.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                    tvDate1Desc.setText(reservation.getCustomer().getImpDate1Desc());

                    if(reservation.getCustomer().getImpDate2() != null && !reservation.getCustomer().getImpDate2().matches("") && !reservation.getCustomer().getImpDate2().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                        lldate2.setVisibility(View.VISIBLE);
                        List<String> items1 = Arrays.asList(reservation.getCustomer().getImpDate2().split("\\s+"));

                        if(items1.size() > 0) {
                            try {

                                calDateImp.setTime(sdfDateImp.parse(items1.get(0)));
                                tvdate2.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                        tvDate2Desc.setText(reservation.getCustomer().getImpDate2Desc());
                    }

                    if(reservation.getCustomer().getImpDate3() != null && !reservation.getCustomer().getImpDate3().matches("") && !reservation.getCustomer().getImpDate3().equalsIgnoreCase("1/1/1900 12:00:00 AM")){

                        lldate3.setVisibility(View.VISIBLE);

                        List<String> items2 = Arrays.asList(reservation.getCustomer().getImpDate3().split("\\s+"));

                        if(items2.size() > 0) {
                            try {

                                calDateImp.setTime(sdfDateImp.parse(items2.get(0)));
                                tvdate3.setText(new SimpleDateFormat("dd-MM-yyyy").format(calDateImp.getTime()));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }

                        tvDate3Desc.setText(reservation.getCustomer().getImpDate3Desc());
                    }



                }

                if(reservation.getCustomer().getCusHabits() != null && !reservation.getCustomer().getCusHabits().matches("")){

                    cvHabits.setVisibility(View.VISIBLE);
                    tvHabits.setText(reservation.getCustomer().getCusHabits());
                }

                if(reservation.getCustomer().getAllergicInfo() != null && !reservation.getCustomer().getAllergicInfo().matches("")){

                    cvAllergy.setVisibility(View.VISIBLE);
                    tvAllergy.setText(reservation.getCustomer().getAllergicInfo());
                }
            }




        }

        txtdate.setEnabled(false);
        txtTime.setEnabled(false);


    }

    private void addReservation() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    final ReservationContainer reservationContainer = new ReservationContainer();
                    reservationContainer.setDocumentId("0");
                    reservationContainer.setDocumentDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " 00:00:00.000");
                    reservationContainer.setDocumentYear(new SimpleDateFormat("yyyy").format(new Date()));
                    reservationContainer.setCounter(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_COUNTER, ""));
                    reservationContainer.setCashierId(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_ID, ""));
                    if(reservation.getCustomer() != null){

                        if(reservation.getCustomer().getName() != null)
                            reservationContainer.setCustomerName(reservation.getCustomer().getName());
                        if(reservation.getCustomer().getId() != null)
                            reservationContainer.setCustomerCode(reservation.getCustomer().getId());

                    }

                    reservationContainer.setGuestCount(edtTxtGuestNumber.getText().toString());
                    reservationContainer.setReservationDate(new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate) + " 00:00:00.000");
                    reservationContainer.setReservationTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(selectedReservationTime));
                    reservationContainer.setTableCode(reservation.getTableCount());
                    reservationContainer.setUserId(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_KEY_ID, ""));
                    final Response<String> response = Utilities.getRetrofitWebService(getContext()).addReservation(reservationContainer).execute();
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if (response != null && response.body() != null) {
                                if (!response.body().equals("error")) {
                                    Utilities.showSnackBar("Reservation Updated" , getActivity());

                                    new AlertDialog.Builder(getContext())
                                            .setTitle("Reservation successfully Updated")
                                            .setMessage("Do you want to add KOT for this reservation ?")
                                            .setPositiveButton("Add KOT", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    ((OrderDetails) getActivity()).onAddItemForReservation(response.body(), customer , new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                                                }
                                            })
                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    mCallback.onArticleSelected();
                                                    getActivity().getSupportFragmentManager().popBackStackImmediate();

                                                }
                                            })
                                            .setCancelable(false)
                                            .create().show();
                                } else {
                                    Utilities.showSnackBar("Failed to Update Reservation" , getActivity());


                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error" , getActivity());

                        }
                    });
                }
            }
        }).start();
    }

    private boolean validateForm() {

        boolean result = true;

        if( txtdate.getText().toString().trim().length() == 0 ){
            Utilities.showSnackBar("Select Reservation Date" , getActivity());
            result =  false;
        }
        else if( txtTime.getText().toString().trim().length() == 0 ){
            Utilities.showSnackBar("Select Reservation Time" , getActivity());
            result =  false;
        }

        return result; //TODO
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_cancel) {

            new AlertDialog.Builder(getContext())
                    .setTitle("Info")
                    .setMessage("Cancel Reservation")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            try {
                                onCancel();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    })
                    .setNegativeButton("cancel" , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();

        }

        else if(v.getId() == R.id.btn_edit){

            if(btnEdit.getText().toString().equalsIgnoreCase("Edit")){

                btnEdit.setText("Save");
                txtdate.setEnabled(true);
                txtTime.setEnabled(true);
                getView().findViewById(R.id.btnAdd).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.btnSubstract).setVisibility(View.VISIBLE);
                edtTxtGuestNumber.setEnabled(true);


            }
            else if(btnEdit.getText().toString().equalsIgnoreCase("Save")){

                btnEdit.setText("Edit");
                txtdate.setEnabled(false);
                txtTime.setEnabled(false);
                getView().findViewById(R.id.btnAdd).setVisibility(View.GONE);
                getView().findViewById(R.id.btnSubstract).setVisibility(View.GONE);
                edtTxtGuestNumber.setEnabled(false);

                if (validateForm()) {
                    UpdateReservation();
                }


            }
        }
        else if(v.getId() == R.id.btn_items){

            try {
                if(!fromResvAct)
                    ((OrderDetails) getActivity()).onViewItemForReservation(reservation, customer , false , new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                else
                {
                    Intent intent = new Intent(getActivity(), SelectItemsActivity.class);
                    intent.putExtra("customer", customer);
                    intent.putExtra("isReservationKot", true);
                    intent.putExtra("isReservationItemEdit", true);
                    intent.putExtra("makeKOT", false);
                    intent.putExtra("reservation", reservation);
                    intent.putExtra("table", ApplicationSingleton.getInstance().getTable());
                    intent.putExtra("hasCustomer", true);
                    intent.putExtra("resvDate", new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                    startActivityForResult(intent, 67);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        else if(v.getId() == R.id.btn_makekot){

            try {
                if(!fromResvAct)
                    ((OrderDetails) getActivity()).onViewItemForReservation(reservation, customer , true , new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                else
                {
                    Intent intent = new Intent(getActivity(), SelectItemsActivity.class);
                    intent.putExtra("customer", customer);
                    intent.putExtra("isReservationKot", true);
                    intent.putExtra("isReservationItemEdit", true);
                    intent.putExtra("makeKOT", true);
                    intent.putExtra("reservation", reservation);
                    intent.putExtra("table", ApplicationSingleton.getInstance().getTable());
                    intent.putExtra("hasCustomer", true);
                    intent.putExtra("resvDate", new SimpleDateFormat("yyyy-MM-dd").format(selectedReservationDate));
                    startActivityForResult(intent, 67);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else if (v.getId() == R.id.btnAdd) {
            try {
                edtTxtGuestNumber.setText((Integer.parseInt((edtTxtGuestNumber.getText().toString())) + 1) + "");
            } catch (Exception e) {

            }
        } else if (v.getId() == R.id.btnSubstract) {
            try {
                edtTxtGuestNumber.setText(Integer.parseInt((edtTxtGuestNumber.getText().toString())) > 1 ? ((Integer.parseInt((edtTxtGuestNumber.getText().toString()))) - 1) + "" : edtTxtGuestNumber.getText().toString());
            } catch (Exception e) {

            }
        }
    }

    private void UpdateReservation() {

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Updating Reservation");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        progressDialog.show();


        CancelKotOrBill cancelKotOrBill = new CancelKotOrBill();
        cancelKotOrBill.id = reservation.getDocNumber() ;
        cancelKotOrBill.date = reservation.getResDate();
        cancelKotOrBill.reson = "";

        Utilities.getRetrofitWebService(getContext()).cancelReservation(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
            @Override
            public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {

                if (response != null) {
                    if ((Boolean) response.body().get("response_code")) {

                        loadResrvationNumber();


                    } else {

                        progressDialog.dismiss();

                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

                progressDialog.dismiss();

            }
        });

    }

    private void loadResrvationNumber() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final Response<Map<String, String>> res = Utilities.getRetrofitWebService(getContext()).getNewBillNumberN(Utilities.getSharedPreferences(getActivity()).getString(Constants.SHARED_PREF_COUNTER, "")).execute();

                    if (res != null && res.body() != null) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                onReservationDetailsLoaded(res.body());

                            }
                        });


                    }

                } catch (Exception e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error" , getActivity());
                        }
                    });
                }
            }
        }).start();

    }

    private void onReservationDetailsLoaded(Map<String, String> reservationDetails) {

        txtreservationNumber.setText(new DecimalFormat("##000000").format(Double.parseDouble(reservationDetails.get("bill_no"))));

        addReservation();
    }

    public void onCancel(){

        CancelKotOrBill cancelKotOrBill = new CancelKotOrBill();
        cancelKotOrBill.id = reservation.getDocNumber() ;
        cancelKotOrBill.date = reservation.getResDate();
        cancelKotOrBill.reson = "";

        Utilities.getRetrofitWebService(getContext()).cancelReservation(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
            @Override
            public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {

                if (response != null) {
                    if ((Boolean) response.body().get("response_code")) {
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.success)
                                .setMessage("Reservation Cancelled")
                                .setCancelable(false)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(getActivity() != null) {
                                            mCallback.onArticleSelected();
                                            getActivity().getSupportFragmentManager().popBackStackImmediate();
                                        }

                                    }
                                })
                                .create().show();
                    } else {
                        new AlertDialog.Builder(getContext())
                                .setTitle(R.string.error)
                                .setMessage("Canot delete")
                                .setCancelable(false)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.dismiss();
                                    }
                                })
                                .create().show();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    private class TimeBundle {
        int hour;
        int minute;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intent = new Intent("refreshTable");
        getActivity().sendBroadcast(intent);
    }
}

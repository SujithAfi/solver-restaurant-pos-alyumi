package com.afi.restaurantpos.al_loomie.RetrofitModels;

/**
 * Created by afi on 10/21/2015.
 */
public class PendingBill {
    public String Cm_No;
    public String Cm_TotAmt;
    public String Cm_Counter;
    public String Cus_Cd;
    public String Cm_DiscAmt;
    public String Cm_NetAmt;
    public String Cus_name;
    public String Tab_Cd;
}

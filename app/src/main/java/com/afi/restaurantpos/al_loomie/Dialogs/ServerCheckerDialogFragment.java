package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Map;

import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServerCheckerDialogFragment extends DialogFragment {

    private ViewGroup mContainer;
    private View mLayout;
    private Toolbar toolbar;

    private TextView txtAPiUrl;
    private TextView txtDateTime;
    private TextView txtServerConnection;
    private TextView txtServerDbConfiguration;

    private ProgressBar progressBarLoading;

    private LinearLayout contanierLayout;

    public ServerCheckerDialogFragment() {
        // Required empty public constructor
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_server_checker_dialog, container, false);
        toolbar = (Toolbar) mLayout.findViewById(R.id.toolbar);
        toolbar.setTitle("Check server");
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ServerCheckerDialogFragment.this.dismiss();
            }
        });

        txtAPiUrl = (TextView) mLayout.findViewById(R.id.txtAPiUrl);
        txtDateTime = (TextView) mLayout.findViewById(R.id.txtDateTime);
        txtServerConnection = (TextView) mLayout.findViewById(R.id.txtServerConnection);
        txtServerDbConfiguration = (TextView) mLayout.findViewById(R.id.txtServerDbConfiguration);
        contanierLayout = (LinearLayout) mLayout.findViewById(R.id.contanierLayout);
        progressBarLoading = (ProgressBar) mLayout.findViewById(R.id.progressBarLoading);
        return mLayout;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setPositiveButton("Test server", null)
                .setNegativeButton("Cancel" , null)
                .create();
    }

    @Override
    public void onResume() {
        super.onResume();

        getServerDetils();

    }

    private void getServerDetils() {

        try {
            contanierLayout.setVisibility(View.INVISIBLE);
            progressBarLoading.setVisibility(View.VISIBLE);

            Utilities.getRetrofitWebService(getContext()).getDatabaseDetails().enqueue(new Callback<Map<String , Object>>() {
                @Override
                public void onResponse(Response<Map<String , Object>> response, Retrofit retrofit) {
                    try {
                        contanierLayout.setVisibility(View.VISIBLE);
                        progressBarLoading.setVisibility(View.GONE);
                        if (response.body() != null && ServerCheckerDialogFragment.this.isVisible()) {
                            String apiUrl = "";
                            SharedPreferences preferences = Utilities.getSharedPreferences(getContext());
                            if (preferences.getBoolean(Constants.SHARED_PREF_WEB_API_URL_CONFIGURED , false))
                                apiUrl=  preferences.getString(Constants.SHARED_PREF_WEB_API_URL , Constants.API_BASE_URL_1);
                            else
                                apiUrl =  Constants.API_BASE_URL_1 ;
                            txtAPiUrl.setText(apiUrl);
                            txtDateTime.setText((String) response.body().get("datetime"));
                            txtServerConnection.setText((Boolean) response.body().get("response_code") ? "OK" : "Error in url");
                            txtServerDbConfiguration.setText((String) response.body().get("insertResponse"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                @Override
                public void onFailure(Throwable t) {
                    try {
                        contanierLayout.setVisibility(View.VISIBLE);
                        progressBarLoading.setVisibility(View.GONE);
                        txtDateTime.setText("");
                        txtAPiUrl.setText("");
                        txtServerConnection.setText("");
                        txtServerDbConfiguration.setText("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            final AlertDialog d = (AlertDialog) getDialog();
            if (d != null) {
                Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getServerDetils();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

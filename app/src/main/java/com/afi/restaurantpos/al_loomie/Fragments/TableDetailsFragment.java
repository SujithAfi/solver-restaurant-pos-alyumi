package com.afi.restaurantpos.al_loomie.Fragments;


import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.TableStrucureAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.BillFragmentDialog;
import com.afi.restaurantpos.al_loomie.Models.Reservation;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillContainer;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.ItemModifiers;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotOnTable;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.GridSpacingItemDecoration;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class TableDetailsFragment extends Fragment implements TableStrucureAdapter.TableSelectListner, SwipeRefreshLayout.OnRefreshListener {

    final Handler handler = new Handler();
    List<SelectedItemDetails> selectedItemDetailses;
    String newBillNumber;
    IntentFilter intentFilter = new IntentFilter("floorChanged");
    private boolean onPauseFlag = false;
    private LinearLayout btnRefresh;
    private String floorId = "1";
    private RecyclerView tablesRecyclerView;
    private Handler uiHandler = new Handler();
    private int tabPosition = -1;

    private BroadcastReceiver refreshActionReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            reFreshTableDetails(false, floorId , calendar.getTime());
        }
    };
    private BroadcastReceiver floorChangedReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String floor = intent.getStringExtra("floor");
            reFreshTableDetails(false, floor , calendar.getTime());
        }
    };
    private TextView txtOrder;
    private TextView txtOrderType;
    public static String selectedReservationDate;
    private Calendar calendar;

    public TableDetailsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         calendar = Calendar.getInstance();
         ApplicationSingleton.getInstance().setCurrentDate(calendar.getTime());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_table_details, container, false);
        tablesRecyclerView = (RecyclerView) view.findViewById(R.id.rv_tables);
        btnRefresh = (LinearLayout) view.findViewById(R.id.btnRefresh);
        txtOrder = (TextView) view.findViewById(R.id.txtOrder);
        txtOrderType = (TextView) view.findViewById(R.id.txtOrderType);

        txtOrderType.setText(new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime()));
        txtOrderType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String date = txtOrderType.getText().toString();
                if (!date.isEmpty()) {
                    try {
                        String[] dateParts = date.split("-");
                        calendar.set(Calendar.YEAR, Integer.parseInt(dateParts[2]));
                        calendar.set(Calendar.MONTH, Integer.parseInt(dateParts[1]) - 1);
                        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dateParts[0]));
                    } catch (Exception e) {

                    }
                }


                new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                        calendar.set(Calendar.YEAR, i);
                        calendar.set(Calendar.MONTH, i1);
                        calendar.set(Calendar.DAY_OF_MONTH, i2);
                        txtOrderType.setText(new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime()));

                        ApplicationSingleton.getInstance().setCurrentDate(calendar.getTime());
                        reFreshTableDetails(true, floorId , calendar.getTime());


                    }
                },  calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE)).show();


            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tablesRecyclerView.setHasFixedSize(true);
        tablesRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 7, LinearLayoutManager.VERTICAL, false));

        int spanCount = 9; // 3 columns
        int spacing = 2; // 50px
        boolean includeEdge = false;
        tablesRecyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));



        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String floorId = ApplicationSingleton.getInstance().getFloor();
                if (!Utilities.isNetworkConnected(getContext()))
                    Utilities.createNoNetworkDialog(getContext());
                else {
                    if (floorId != null)
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                reFreshTableDetails(true, floorId , calendar.getTime());
                            }
                        }, 500);
                }
            }
        });

        Runnable runnable = new Runnable() {

            private boolean runLock = false;

            @Override
            public void run() {
                try {
                    //do your code here
                    reFreshTableDetails(true, floorId , calendar.getTime());
                } catch (Exception e) {
                    // TODO: handle exception
                } finally {
                    //also call the same runnable to call it at regular interval
                    if (onPauseFlag)
                        handler.postDelayed(this, 1000 * 3);
                }
            }
        };
        handler.postDelayed(runnable, 1000 * 3);
    }



    public void reFreshTableDetails(final boolean isInternal, final String floorId , final Date date) {

        this.floorId = floorId;
        new Thread(new Runnable() {
            @Override
            public void run() {


                List<Table> tables = new ArrayList<Table>();
                List<Reservation> reservations = new ArrayList<Reservation>();

                try {
                    if (getActivity() != null) {
                        final Response<List<Table>> respAllTables = Utilities.getRetrofitWebService(getActivity()).getTablesData(floorId , new SimpleDateFormat("yyyy-MM-dd").format(date) ).execute();
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (getActivity() != null) {
                                    btnRefresh.setVisibility(View.INVISIBLE);
                                }
                            }
                        });
                        if (respAllTables.body() != null) {
                            tables.addAll(respAllTables.body());
                            String reservationDate = new SimpleDateFormat("yyyy-MM-dd").format(date) + " 00:00:00.000";

                            if (getActivity() != null) {

                                final Response<List<Reservation>> reservationsData = Utilities.getRetrofitWebService(getContext()).getReservation(reservationDate).execute();

                                if (reservationsData.body() != null) {
                                    reservations.addAll(reservationsData.body());
                                }
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (getActivity() != null) {
                            }
                        }
                    });
                }

                if (getActivity() != null)
                    initTableView(tables, reservations);

            }
        }).start();

    }

    private void initTableView(final List<Table> tables, final List<Reservation> reservations) {


        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    if(getActivity() != null)
                        tablesRecyclerView.setAdapter(new TableStrucureAdapter(tables , reservations , TableDetailsFragment.this, getActivity().getSupportFragmentManager()));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onTableSelected(Table table, boolean needToClear) {
        try {
            if (needToClear)
                ApplicationSingleton.getInstance().clearSelectedItemDetails();
            ApplicationSingleton.getInstance().setTable(table);
            ((OrderDetails) getActivity()).onTableSelected(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onKotRequest(String id, final int option, final Table table) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getContext())
                .content("Loading KOT")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            try {
                dialog.show();
                Utilities.getRetrofitWebService(getContext()).getPendingKotsOnTable(id).enqueue(new Callback<List<PendingKotOnTable>>() {
                    @Override
                    public void onResponse(Response<List<PendingKotOnTable>> response, Retrofit retrofit) {
                        dialog.cancel();
                        final List<PendingKotOnTable> pendingKots = response.body();
                        for (PendingKotOnTable kotOnTable : pendingKots) {
                            for (PendingKotItem item : kotOnTable.details) {
                                item.Cm_Covers = kotOnTable.Kot_Covers;
                                item.Tab_Cd = kotOnTable.Tab_cd;
                            }
                        }
                        String[] kotNames = new String[pendingKots.size()];

                        for (int i = 0; i < pendingKots.size(); i++)
                            kotNames[i] = String.format("%s (Customer : %s)", pendingKots.get(i).Kot_No, pendingKots.get(i).Cus_Name);


                        new MaterialDialog.Builder(getContext())
                                .titleColorRes(R.color.colorAccentDark)
                                .title("Select a KOT")
                                .items(kotNames)
                                .cancelable(true)
                                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                                    @Override
                                    public boolean onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                                        switch (option) {
                                            case 0://TODO Billl KOT
                                                List<String> kot = new ArrayList<String>();
                                                kot.add(pendingKots.get(i).Kot_No);
                                                loadKotDetails(kot);

                                                break;
                                            case 1://TODO Modify KOT
                                                ApplicationSingleton.getInstance().convertPendingKotsToSelectedItemDetails(pendingKots.get(i));
                                                onTableSelected(table, false);
                                                break;
                                            case 2://TODO New KOT

                                                break;
                                        }
                                        return false;
                                    }
                                })
                                .show();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dialog.cancel();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onRefresh() {
        reFreshTableDetails(false, floorId , calendar.getTime());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    private void loadKotDetails(final List<String> kotStringList) {

        final MaterialDialog dialog = new MaterialDialog.Builder(getContext())
                .content("Loading Selected Kot's")
                .progress(true, 1)
                .titleColorRes(R.color.colorAccentDark)
                .cancelable(false).build();

        if (!Utilities.isNetworkConnected(getContext()))
            Utilities.createNoNetworkDialog(getContext());
        else {
            try {
                dialog.show();
                Utilities.getRetrofitWebService(getContext()).getKotDetailsFromKotNumber(kotStringList).enqueue(new Callback<Map<String, List<PendingKotItem>>>() {
                    @Override
                    public void onResponse(Response<Map<String, List<PendingKotItem>>> response, Retrofit retrofit) {
                        dialog.dismiss();
                        if ((response.body() != null)) {
                            Map<String, List<PendingKotItem>> listMap = response.body();

                            //prepare bill from here
                            try {
                                ApplicationSingleton.getInstance().setSelectedKotMap(listMap);
                                ApplicationSingleton.getInstance().setTemporaryPendingKotItems(listMap.get(kotStringList.get(0)));
                            } catch (Exception e) {
                                String t = e.getMessage();
                            }


                            //Pocess bill data
                            selectedItemDetailses = new ArrayList<>();
                            for (PendingKotItem item : ApplicationSingleton.getInstance().getTemporaryPendingKotItems()) {
                                SelectedItemDetails details = new SelectedItemDetails();
                                details.itemCode = item.Itm_Cd;
                                details.itemQuantity = Double.parseDouble(item.KotD_Qty);
                                details.itemUnitCost = Double.parseDouble(item.KotD_Rate);
                                details.itemTotalCost = Double.parseDouble(item.KotD_Amt);
                                details.Itm_UCost = item.KotD_UCost;
                                details.itemName = item.Itm_Name;
                                details.Unit_Fraction = item.Unit_Fraction;
                                details.Unit_cd = item.Kot_MuUnit;
                                details.barcode = item.Barcode;
                                details.Kot_No = item.Kot_No;
                                details.Kot_No = item.Kot_No;
                                details.Menu_SideDish = item.menu_SideDish;
                                details.Cmd_IsBuffet = "0";

                                details.Cus_Name = item.Cus_Name;
                                details.Cus_Cd = item.Cus_Cd;
                                details.Disc_Per = item.Disc_Per;

                                details.Cm_Type = item.Cm_Type;
                                details.Cm_Covers = item.Cm_Covers;
                                details.Sman_Cd = item.Sman_Cd;
                                details.Tab_Cd = item.Tab_Cd;
                                details.CmD_Amt = item.CmD_Amt;
                                selectedItemDetailses.add(details);
                            }


                            final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                    .content("Creating new Bill")
                                    .progress(true, 1)
                                    .titleColorRes(R.color.colorAccentDark)
                                    .cancelable(false).build();

                            if (!Utilities.isNetworkConnected(getContext()))
                                Utilities.createNoNetworkDialog(getContext());
                            else {
                                dialog.show();
                                Utilities.getRetrofitWebService(getContext()).getNewBillNumber(ApplicationSingleton.getInstance().getCounterNumber()).enqueue(new Callback<Map<String, String>>() {
                                    @Override
                                    public void onResponse(Response<Map<String, String>> response, Retrofit retrofit) {
                                        dialog.cancel();

                                        Map<String, String> stringStringMap = response.body();
                                        if (stringStringMap.get("bill_no") != null) {
                                            if (stringStringMap.get("bill_no").length() > 0) {
                                                ApplicationSingleton.getInstance().setNewBillNumber(Utilities.getKotFormat(stringStringMap.get("bill_no")));
                                            }
                                        }
                                        newBillNumber = String.format("Bill No : %s", Utilities.getKotFormat(response.body().get("bill_no")));

                                        if (selectedItemDetailses != null) {
                                            if (selectedItemDetailses.size() > 0) {
                                                final BillContainer saveBillData = prepareData(selectedItemDetailses);
                                                new BillFragmentDialog()
                                                        .setBillItems(saveBillData.details)
                                                        .setNewBillNumber(newBillNumber)
                                                        .setBillListner(new BillFragmentDialog.BillListner() {
                                                            @Override
                                                            public void onPositive() {

                                                                if (!Utilities.isNetworkConnected(getContext()))
                                                                    Utilities.createNoNetworkDialog(getContext());
                                                                else {

                                                                    final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                                                            .content("Saving Bill")
                                                                            .progress(true, 1)
                                                                            .titleColorRes(R.color.colorAccentDark)
                                                                            .cancelable(false).build();
                                                                    dialog.show();

                                                                    Utilities.getRetrofitWebService(getContext()).saveBill(saveBillData).enqueue(new Callback<Map<String, Object>>() {
                                                                        @Override
                                                                        public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                                                                            dialog.dismiss();
                                                                            final boolean response_code = (Boolean) response.body().get("response_code");
                                                                            if (response_code) {
                                                                                new MaterialDialog.Builder(getActivity())
                                                                                        .titleColorRes(R.color.colorAccentDark)
                                                                                        .title(response_code ? "Bill Saved" : "Can't save bill")
                                                                                        .positiveText("Ok")
                                                                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                                            @Override
                                                                                            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                                                                if (response_code) {

                                                                                                    String floorId = ApplicationSingleton.getInstance().getFloor();
                                                                                                    reFreshTableDetails(true, floorId , calendar.getTime());
                                                                                                }
                                                                                            }
                                                                                        }).build().show();
                                                                            }

                                                                        }

                                                                        @Override
                                                                        public void onFailure(Throwable t) {
                                                                            dialog.dismiss();
                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            @Override
                                                            public void onNegative() {

                                                            }
                                                        })
                                                        .show(getActivity().getSupportFragmentManager(), "Billu");
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Throwable t) {
                                        dialog.cancel();
                                    }
                                });

                            }

                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dialog.dismiss();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private BillContainer prepareData(List<SelectedItemDetails> selectedItemDetailses) {
        String Tab_Cd = "";
        String Cm_Covers = "";

        Context context = getContext();
        ApplicationSingleton singleton = ApplicationSingleton.getInstance();
        SharedPreferences preferences = Utilities.getSharedPreferences(context);
        int index = 0;
        double Cm_TotAmt = 0;
        String Cus_Name = "";
        String Cus_Cd = "";
        String Disc_Per = "";
        BillContainer billContainer = new BillContainer();
        List<BillItem> billItems = new ArrayList<>();
        for (SelectedItemDetails details : selectedItemDetailses) {

            Cus_Name = details.Cus_Name;
            Cus_Cd = details.Cus_Cd;
            Disc_Per = details.Disc_Per;

            Tab_Cd = details.Tab_Cd;
            Cm_Covers = details.Cm_Covers;
            BillItem billItem = new BillItem();
            billItem.Itm_cd = details.itemCode;
            //put modifiers
            billItem.Menu_SideDish = "";
            if (details.modify != null) {
                for (int i = 0; i < details.modify.size(); i++) {
                    ItemModifiers modifier = details.modify.get(i);
                    if (i + 1 == details.modify.size())
                        billItem.Menu_SideDish += modifier.name;
                    else if (i == 0)
                        billItem.Menu_SideDish += modifier.name;
                    else
                        billItem.Menu_SideDish += "," + modifier.name;
                }
            }
            billItem.Cmd_ItmName = details.itemName;
            billItem.BarCode = details.barcode;
            billItem.Unit_Fraction = details.Unit_Fraction;
            billItem.Cmd_Qty = details.itemQuantity + "";
            billItem.Unit_Cd = details.Unit_cd;
            billItem.Cmd_MuUnit = "1";//TODO
            billItem.Cmd_MuQty = details.itemQuantity + "";
            billItem.Cmd_MuFraction = "1";
            billItem.Cmd_MuConvToBase = "1";
            billItem.Cmd_MuUcost = "1";
            billItem.Cmd_Rate = details.itemUnitCost + "";//// TODO: 10/21/2015
            double Cmd_Amta = (details.itemUnitCost) * (details.itemQuantity);
            String Cmd_DiscPers = billItem.Cmd_DiscPer;
            billItem.Cmd_DiscPer = details.Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
            if (Cmd_DiscPers != null) {
                billItem.Cmd_DiscAmt = Utilities.getPercentage(Cmd_Amta, Double.parseDouble(billItem.Cmd_DiscPer)) + "";
            } else {
                billItem.Cmd_DiscAmt = "0";
            }
            billItem.Cmd_Amt = (Cmd_Amta - Double.parseDouble(billItem.Cmd_DiscAmt)) + "";
            billItem.Cmd_Ctr = index++ + "";
            billItem.Cmd_SubCtr = "0";
            billItem.Cmd_RowType = "A";
            billItem.Cmd_NotCosted = "0";
            billItem.Cmd_Cancelled = "0";
            billItem.Cmd_IsLzQty = "1";
            billItem.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO, 1) + "";
            billItem.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
            billItem.Cmd_Status = "1";
            billItem.Cm_ThruDo = "0";
            billItem.IsModified = "0";
            billItem.Cm_Ref = "1";
            billItem.Cmd_DiscReason = "No";
            billItem.Kot_No = details.Kot_No == null ? "0" : details.Kot_No;
            billItem.Rcp_Cd = "1";
            billItem.Cmd_Ucost = details.itemUnitCost + "";
            billItem.Cmd_IsBuffet = details.Cmd_IsBuffet;//// TODO: 10/21/2015
            billItems.add(billItem);
            Cm_TotAmt += Double.parseDouble(billItem.Cmd_Amt);
        }
        billContainer.details = billItems;
        billContainer.Usr_id = preferences.getString(Constants.SHARED_PREF_KEY_ID, "");
        billContainer.Cm_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_No = singleton.getNewBillNumber();
        billContainer.Sman_Cd = singleton.getSalesman(context) == null ? "0" : singleton.getSalesman(context).Id + "";
        billContainer.Cus_Cd = Cus_Cd;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Id + "";
        billContainer.Cus_Name = Cus_Name;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Name + "";billContainer.Cm_TotAmt =
        billContainer.Cm_DiscPer = Disc_Per;//singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().Discount + "";
        billContainer.Cm_DiscAmt = Utilities.getPercentage(Cm_TotAmt, Double.parseDouble(billContainer.Cm_DiscPer)) + "";

        billContainer.Cm_TotAmt = Cm_TotAmt + "";

        Cm_TotAmt -= Double.parseDouble(billContainer.Cm_DiscAmt);

        billContainer.Cm_Tax1_Per = Utilities.getTaxFraction1(context) + "";
        billContainer.Cm_Tax1_Amt = Utilities.findTax1(context, Cm_TotAmt) + "";
        billContainer.Cm_Tax2_Per = Utilities.getTaxFraction2(context) + "";
        billContainer.Cm_Tax2_Amt = Utilities.findTax2(context, Cm_TotAmt) + "";
        billContainer.Cm_NetAmt = (Cm_TotAmt + Double.parseDouble(billContainer.Cm_Tax1_Amt) + Double.parseDouble(billContainer.Cm_Tax2_Amt)) + "";
        billContainer.Cm_Tips_Amt = "0";
        billContainer.Sft_No = preferences.getInt(Constants.SHARED_PREF_KEY_SHIFT_NO, 0) + "";
        billContainer.Sft_Dt = preferences.getString(Constants.SHARED_PREF_KEY_SHIFT_DATE, "");
        billContainer.Cm_BC_Settle_Amt = "0.000";
        billContainer.Cm_Paid_Amt = "0.000";
        billContainer.Cm_Cancel_Reason = "";
        billContainer.Cm_Cancelled = "0";
        billContainer.Cm_Cus_Phone = singleton.getCustomerDetails() == null ? "0" : singleton.getCustomerDetails().getId() + "";
        billContainer.Cm_Type = "1";//TODO get order type
        billContainer.Tab_Cd = Tab_Cd;//singleton.getTable() == null ? "" : singleton.getTable().Tab_Cd;//TODO
        billContainer.Cm_Covers = Cm_Covers;//singleton.getGuestCount();
        billContainer.Srvr_Cd = "1";
        billContainer.Cm_Collected_Counter = preferences.getString(Constants.COUNTER_NAME, "");
        billContainer.Cm_Collected_User = preferences.getString(Constants.SHARED_PREF_KEY_USERNAME, "0");
        int i = 0;
        i++;
        billContainer.Srvr_Cd = i + "";

        return billContainer;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            getActivity().registerReceiver(refreshActionReciever, new IntentFilter("refreshTable"));
            String floorId = ApplicationSingleton.getInstance().getFloor();
            if (!Utilities.isNetworkConnected(getContext())) {
                Utilities.createNoNetworkDialog(getContext());
                btnRefresh.setVisibility(View.VISIBLE);
            } else {
                if (floorId != null)
                    reFreshTableDetails(true, floorId , calendar.getTime());
            }
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(refreshActionReciever, intentFilter);
            onPauseFlag = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            onPauseFlag = false;
            getActivity().unregisterReceiver(refreshActionReciever);
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(floorChangedReciever);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

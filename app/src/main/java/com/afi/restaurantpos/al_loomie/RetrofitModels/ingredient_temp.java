package com.afi.restaurantpos.al_loomie.RetrofitModels;

/**
 * Created by afi-mac-001 on 20/09/16.
 */
public class ingredient_temp {

    private String ingredients;
    private int count;

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}

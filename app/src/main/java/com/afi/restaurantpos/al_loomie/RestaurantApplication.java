package com.afi.restaurantpos.al_loomie;

import android.app.Application;
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by afi-mac-001 on 13/07/16.
 */
@ReportsCrashes(formKey = "", // will not be used
        mailTo = "sujith@afiinfotech.com", mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast)
public class RestaurantApplication extends Application {

    public void onCreate() {
        super.onCreate();

        ACRA.init(this);
    }

}

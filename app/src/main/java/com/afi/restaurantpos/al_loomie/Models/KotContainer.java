package com.afi.restaurantpos.al_loomie.Models;

import java.util.List;

/**
 * Created by AFI on 10/14/2015.
 */
public class KotContainer {

    private String Usr_Id;
    private String Kot_Counter;
    private String Kot_No;
    private String Kot_Type;
    private String Tab_cd;
    private String Kot_TotAmt;
    private String Cus_Cd;
    private String Cus_Name;
    private String Sman_Cd;
    private String Kot_Covers;
    private String Srvr_Cd;
    private List<KotItem> details;
    private String Resv_no;
    private Boolean isReservation;

    public Boolean getSubitem() {
        return isSubitem;
    }

    public void setSubitem(Boolean subitem) {
        isSubitem = subitem;
    }

    private Boolean isSubitem;

    public Boolean getReservation() {
        return isReservation;
    }

    public void setReservation(Boolean reservation) {
        isReservation = reservation;
    }

    public String getResv_no() {
        return Resv_no;
    }

    public void setResv_no(String resv_no) {
        Resv_no = resv_no;
    }

    public String getUsr_Id() {
        return Usr_Id;
    }

    public void setUsr_Id(String usr_Id) {
        Usr_Id = usr_Id;
    }

    public String getKot_Counter() {
        return Kot_Counter;
    }

    public void setKot_Counter(String kot_Counter) {
        Kot_Counter = kot_Counter;
    }

    public String getKot_No() {
        return Kot_No;
    }

    public void setKot_No(String kot_No) {
        Kot_No = kot_No;
    }

    public String getKot_Type() {
        return Kot_Type;
    }

    public void setKot_Type(String kot_Type) {
        Kot_Type = kot_Type;
    }

    public String getTab_cd() {
        return Tab_cd;
    }

    public void setTab_cd(String tab_cd) {
        Tab_cd = tab_cd;
    }

    public String getKot_TotAmt() {
        return Kot_TotAmt;
    }

    public void setKot_TotAmt(String kot_TotAmt) {
        Kot_TotAmt = kot_TotAmt;
    }

    public String getCus_Cd() {
        return Cus_Cd;
    }

    public void setCus_Cd(String cus_Cd) {
        Cus_Cd = cus_Cd;
    }

    public String getCus_Name() {
        return Cus_Name;
    }

    public void setCus_Name(String cus_Name) {
        Cus_Name = cus_Name;
    }

    public String getSman_Cd() {
        return Sman_Cd;
    }

    public void setSman_Cd(String sman_Cd) {
        Sman_Cd = sman_Cd;
    }

    public String getKot_Covers() {
        return Kot_Covers;
    }

    public void setKot_Covers(String kot_Covers) {
        Kot_Covers = kot_Covers;
    }

    public String getSrvr_Cd() {
        return Srvr_Cd;
    }

    public void setSrvr_Cd(String srvr_Cd) {
        Srvr_Cd = srvr_Cd;
    }

    public List<KotItem> getDetails() {
        return details;
    }

    public void setDetails(List<KotItem> details) {
        this.details = details;
    }
}

package com.afi.restaurantpos.al_loomie.RetrofitModels;

import java.util.List;

/**
 * Created by AFI on 10/16/2015.
 */
public class PendingKotOnTable {
    public String Usr_Id;
    public String Kot_Counter;
    public String Kot_No;
    public String Kot_Type;
    public String Tab_cd;
    public String Kot_TotAmt;
    public String Cus_Cd;
    public String Cus_Name;
    public String Sman_Cd;
    public String Kot_Covers;
    public String Srvr_Cd;
    public List<PendingKotItem> details;
}

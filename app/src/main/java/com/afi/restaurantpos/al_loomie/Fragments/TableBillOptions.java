package com.afi.restaurantpos.al_loomie.Fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Adapters.TableKotOptionsListAdapter;
import com.afi.restaurantpos.al_loomie.Dialogs.CancelBillAndKotDialog;
import com.afi.restaurantpos.al_loomie.Dialogs.PayBillDialog;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * fragments to display Bills
 */
public class TableBillOptions extends android.support.v4.app.Fragment {

    public List<PendingKotItem> bills;
    private RecyclerView rvListKots;

    private TextView txtNoPendingBillsOrKot;

    public static TableBillOptions newInstance(List<PendingKotItem> bills) {
        TableBillOptions fragment = new TableBillOptions();
        fragment.bills = bills;
        return fragment;
    }

    public TableBillOptions() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_table_bill_options, container, false);
        rvListKots = (RecyclerView) v.findViewById(R.id.rvListKots);
        txtNoPendingBillsOrKot = (TextView) v.findViewById(R.id.txtNoPendingBillsOrKot);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rvListKots.setHasFixedSize(true);
        /*rvListKots.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));*/
        rvListKots.setLayoutManager(new GridLayoutManager(getContext() , 2));

        String selectedDate = null;
        if(ApplicationSingleton.getInstance().getCurrentDate() != null)
            selectedDate =  new SimpleDateFormat("dd-MM-yyyy").format(ApplicationSingleton.getInstance().getCurrentDate());
        final String currDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());

        if(ApplicationSingleton.getInstance().getCurrentDate() != null)
            if(selectedDate.equalsIgnoreCase(currDate)){
                rvListKots.setAdapter(new TableKotOptionsListAdapter(bills, "BILL" ,new TableKotOptionsListAdapter.OnItemCLickListner() {
                    @Override
                    public void onItemClicked(int position) {

                        if (!Utilities.isNetworkConnected(getContext()))
                            Utilities.createNoNetworkDialog(getContext());
                        else
                            loadBillDetails(bills.get(position).Kot_No);

                    }

                    @Override
                    public void onDeleteItem(int position) {


                        try {
                            PendingBill pendingBill = new PendingBill();
                            pendingBill.Cm_No = bills.get(position).Kot_No;

                            CancelBillAndKotDialog.newInstance(CancelType.CANCEL_BILL)
                                    .setPendingBill(pendingBill)
                                    .setDialogListner(new CancelBillAndKotDialog.CancelBillAndKotDialogListner() {
                                        @Override
                                        public void onFinishedAction(CancelBillAndKotDialog cancelBillAndKotDialog) {
                                            //TODO refresh items
                                            cancelBillAndKotDialog.dismiss();
                                            getActivity().sendBroadcast(new Intent("reloadBillAndKot"));
                                        }
                                    })
                                    .show(getChildFragmentManager(), "Pending KOT");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onAlertClicked(int position) {

                    }

                    @Override
                    public void onReorderClicked(int position) {

                    }
                }));
    }

    }

    private void loadBillDetails(String bill) {
        try {
            Map<String, String> stringMap = new HashMap<>();
            stringMap.put("bill", bill);
            Utilities.getRetrofitWebService(getContext()).getPendingBillFromBillNumber(stringMap)
                    .enqueue(new Callback<List<PendingBill>>() {
                        @Override
                        public void onResponse(Response<List<PendingBill>> response, Retrofit retrofit) {
                            if (response.body() != null && TableBillOptions.this.isVisible()) {
                                try {
                                    List<PendingBill> pendingBills = response.body();

                                    PayBillDialog.newInstance(CancelType.CANCEL_BILL)
                                            .setPendingBill(pendingBills.get(0))
                                            .setDialogListner(new PayBillDialog.PayBillDialogListner() {
                                                @Override
                                                public void onFinishedAction(PayBillDialog PayBillDialog) {
                                                    //TODO refresh items
                                                    PayBillDialog.dismiss();
                                                    //loadPendingBills();
                                                    //tableOptionsDialog.loadKotAndBills();
                                                    getActivity().sendBroadcast(new Intent("reloadBillAndKot"));

                                                }
                                            })
                                            .show(getFragmentManager(), "Pay bill KOT");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            String selectedDate = null;
            if(ApplicationSingleton.getInstance().getCurrentDate() != null)
                selectedDate =  new SimpleDateFormat("dd-MM-yyyy").format(ApplicationSingleton.getInstance().getCurrentDate());
            final String currDate = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());

            if(ApplicationSingleton.getInstance().getCurrentDate() != null)
                if(selectedDate.equalsIgnoreCase(currDate)) {
                    getActivity().registerReceiver(refreshActionReciever, new IntentFilter("refreshTableOptionsLists"));
                    txtNoPendingBillsOrKot.setVisibility(bills.size() == 0 ? View.VISIBLE : View.GONE);

                }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            getActivity().unregisterReceiver(refreshActionReciever);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver refreshActionReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                TableKotOptionsListAdapter tableKotOptionsListAdapter = (TableKotOptionsListAdapter) rvListKots.getAdapter();
                tableKotOptionsListAdapter.setKotNumbers(bills);
                tableKotOptionsListAdapter.notifyDataSetChanged();

                txtNoPendingBillsOrKot.setVisibility(bills.size() == 0 ? View.VISIBLE : View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


}

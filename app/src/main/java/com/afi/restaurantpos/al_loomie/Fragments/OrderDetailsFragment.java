package com.afi.restaurantpos.al_loomie.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import com.afi.restaurantpos.al_loomie.Adapters.OrderDetailsFragmentPagerAdapter;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.PendingBillsActivity;
import com.afi.restaurantpos.al_loomie.PendingKotActivity;
import com.afi.restaurantpos.al_loomie.PreviousBillActivity;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A placeholder fragment containing a simple view.
 */
public class OrderDetailsFragment extends Fragment {

    private static final int BTN_DINE_IN_INDEX = 0;
    private static final int BTN_TAKE_AWAY_INDEX = 1;
    private static final int BTN_DELIVERY_INDEX = 2;

    private static final int BTN_KOT_INDEX = 0;
    private static final int BTN_BILL_INDEX = 1;
    private static final int BTN_PAY_INDEX = 2;

    private TextView txtWaiterName;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    private LinearLayout dine_in_container;
    private LinearLayout takeAwayContainer;
    private LinearLayout homeDeliveryContainer;
    private LinearLayout billContainer;
    private LinearLayout payContainer;
    private LinearLayout historyContainer;

    private Button[] orderTypeButtons = new Button[3];

    private Button[] transactionModeButtons = new Button[3];

    private OrderDetailsFragmentListner orderDetailsFragmentListner;

    private TextView kotCount;
    private TextView billCount;

    private boolean clickLock = false;
    private Handler clickHandler = new Handler();
    private Runnable clickLockRunnable = new Runnable() {
        @Override
        public void run() {
            clickLock = false;
        }
    };
    private View.OnClickListener orderTypeListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            try {
                if (!clickLock) {
                    clickLock = true;
                    clickHandler.postDelayed(clickLockRunnable, 1000);
                    updateOtherViews(v);
                    ApplicationSingleton.getInstance().setKotType(
                            v.getId() == R.id.dine_in_container ? Constants.ORDER_TYPE_DINE_IN
                                    : v.getId() == R.id.takeAwayContainer ? Constants.ORDER_TYPE_TAKE_AWAY :
                                    Constants.ORDER_TYPE_HOME_DELIVERY
                    );

                    ((OrderDetails) getActivity()).changeOrderType(
                            v.getId() == R.id.dine_in_container ? OrderDetailsFragmentPagerAdapter.OrderType.DINE_IN
                                    : v.getId() == R.id.takeAwayContainer ? OrderDetailsFragmentPagerAdapter.OrderType.TAKE_AWAY
                                    : OrderDetailsFragmentPagerAdapter.OrderType.HOME_DELIVERY);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        private void updateOtherViews(View v) {
            dine_in_container.setSelected(dine_in_container.getId() == v.getId());
            takeAwayContainer.setSelected(takeAwayContainer.getId() == v.getId());
            homeDeliveryContainer.setSelected(homeDeliveryContainer.getId() == v.getId());
        }
    };
    private View.OnClickListener transactionModeListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            try {
                if (!clickLock) {
                    clickLock = true;
                    clickHandler.postDelayed(clickLockRunnable, 1000);
                    updateOtherButtons(v);

                    if (v.getId() == R.id.billContainer) {
                        Intent intent = new Intent(getContext(), PendingKotActivity.class);
                        startActivity(intent);
                    } else if (v.getId() == R.id.payContainer) {
                        Intent intent = new Intent(getContext(), PendingBillsActivity.class);
                        startActivity(intent);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void updateOtherButtons(View v) {
            billContainer.setSelected(v == billContainer);
            payContainer.setSelected(v == payContainer);

        }
    };

    public OrderDetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_order_details, container, false);
        dine_in_container = (LinearLayout) v.findViewById(R.id.dine_in_container);
        takeAwayContainer = (LinearLayout) v.findViewById(R.id.takeAwayContainer);
        homeDeliveryContainer = (LinearLayout) v.findViewById(R.id.homeDeliveryContainer);
        billContainer = (LinearLayout) v.findViewById(R.id.billContainer);
        payContainer = (LinearLayout) v.findViewById(R.id.payContainer);
        historyContainer = (LinearLayout) v.findViewById(R.id.historyContainer);


        txtWaiterName = (TextView) v.findViewById(R.id.txtWaiterName);
        kotCount = (TextView) v.findViewById(R.id.kotCount);
        billCount = (TextView) v.findViewById(R.id.billCount);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        Utilities.getRetrofitWebService(getContext()).getBillAndKotCounts().enqueue(new Callback<List<Map<String, String>>>() {
            @Override
            public void onResponse(Response<List<Map<String, String>>> response, Retrofit retrofit) {
                if ( response.body() != null && OrderDetailsFragment.this.isVisible()){
                    try {
                        Map<String, String> cuntData = response.body().get(0);

                        kotCount.setText(cuntData.get("kot_count") == null ? "0" : cuntData.get("kot_count"));
                        billCount.setText(cuntData.get("bill_count") == null ? "0" : cuntData.get("bill_count"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

        ((OrderDetails) getActivity()).changeOrderType(OrderDetailsFragmentPagerAdapter.OrderType.MAIN_PAGE);

    }

    /**
     * Initilize all order type listners
     */
    private void initilizeOrderTypeListners(){
        dine_in_container.setOnClickListener(orderTypeListner);
        takeAwayContainer.setOnClickListener(orderTypeListner);
        homeDeliveryContainer.setOnClickListener(orderTypeListner);
    }

    private void initilizeTransactionModeListner() {

        billContainer.setOnClickListener(transactionModeListner);
        payContainer.setOnClickListener(transactionModeListner);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initilizeOrderTypeListners();
        initilizeTransactionModeListner();

        ApplicationSingleton.getInstance().setOrderType("DINE-IN");
        ApplicationSingleton.getInstance().setKotType(Constants.ORDER_TYPE_DINE_IN);
        dine_in_container.setSelected(true);


        historyContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(getActivity(), PreviousBillActivity.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public OrderDetailsFragment setOrderDetailsFragmentListner(OrderDetailsFragmentListner orderDetailsFragmentListner) {
        this.orderDetailsFragmentListner = orderDetailsFragmentListner;
        return this;
    }

    public interface OrderDetailsFragmentListner {
        void onFloorChanged(String floorId);
    }


}

package com.afi.restaurantpos.al_loomie.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by afi-mac-001 on 27/06/16.
 */
public class ReservationContainer {


    @SerializedName("Doc_No")
    private String documentId;

    @SerializedName("Doc_Dt")
    private String documentDate;

    @SerializedName("Doc_Year")
    private String documentYear;

    @SerializedName("Doc_Counter")
    private String counter;

    @SerializedName("Cashier_Id")
    private String cashierId;

    @SerializedName("Cus_Cd")
    private String customerCode;

    @SerializedName("Cus_Name")
    private String customerName;

    @SerializedName("Guest_Count")
    private String guestCount;

    @SerializedName("Res_Date")
    private String reservationDate;

    @SerializedName("Res_Time")
    private String reservationTime;

    @SerializedName("Table_Cd")
    private String tableCode;
    @SerializedName("Usr_Id")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getDocumentYear() {
        return documentYear;
    }

    public void setDocumentYear(String documentYear) {
        this.documentYear = documentYear;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(String guestCount) {
        this.guestCount = guestCount;
    }

    public String getReservationDate() {
        return reservationDate;
    }

    public void setReservationDate(String reservationDate) {
        this.reservationDate = reservationDate;
    }

    public String getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(String reservationTime) {
        this.reservationTime = reservationTime;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }
}

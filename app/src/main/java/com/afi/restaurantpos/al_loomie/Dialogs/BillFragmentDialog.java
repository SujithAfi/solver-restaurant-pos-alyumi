package com.afi.restaurantpos.al_loomie.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afi.restaurantpos.al_loomie.Adapters.BillingAdapter;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.BillItem;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.List;


/**
 * Created by afi on 10/21/2015.
 */
public class BillFragmentDialog extends DialogFragment {
    private ViewGroup mContainer;
    private View mLayout;
    private Toolbar toolbar;
    private RecyclerView rVBilling;
    private BillingAdapter billAdapter;
    private BillListner billListner;
    private String newBillNumber = "";

    private TextView txtTotal;
    private TextView txtTax1;
    private TextView txtTax2;
    private TextView txtNetAmout;

    //payment details

    private RadioGroup radioPaymentMode;
    private RadioButton radioCash;
    private RadioButton radioCreditCard;
    private  RadioButton radioCreditAccount;

    private EditText et_cardNumber;

    private EditText et_accountId;
    private EditText et_accountName;
    private EditText et_RoomNumber;
    private EditText tv_checkInDate;



    public interface BillListner{
        void onPositive();
        void onNegative();
    }

    public BillFragmentDialog() {
    }

    public BillFragmentDialog setBillListner(BillListner billListner) {
        this.billListner = billListner;
        return this;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(billListner != null) {
                        billListner.onPositive();
                        BillFragmentDialog.this.dismiss();
                    }
                }
            });

            Button negativeButton = d.getButton(Dialog.BUTTON_NEGATIVE);
            negativeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(billListner != null) {
                        billListner.onNegative();
                        BillFragmentDialog.this.dismiss();
                    }
                }
            });

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }


    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_billling_dialog, container, false);
        toolbar = (Toolbar)mLayout.findViewById(R.id.bill_toolbar);
        toolbar.setTitle("Bill");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BillFragmentDialog.this.dismiss();
            }
        });
        rVBilling = (RecyclerView)mLayout.findViewById(R.id.rVBilling);
        rVBilling.setHasFixedSize(true);
        rVBilling.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        txtTotal = (TextView) mLayout.findViewById(R.id.txtBillTotal);
        txtTax1 = (TextView) mLayout.findViewById(R.id.txtBillTax);
        txtTax2 = (TextView) mLayout.findViewById(R.id.txtBillTax2);
        txtNetAmout = (TextView) mLayout.findViewById(R.id.txtBillGrandTotal);

        radioPaymentMode = (RadioGroup) mLayout.findViewById(R.id.rg_payment_mode);
        radioCash = (RadioButton) mLayout.findViewById(R.id.rb_cash_payment);
        radioCreditCard = (RadioButton) mLayout.findViewById(R.id.rb_credit_card);
        radioCreditAccount = (RadioButton) mLayout.findViewById(R.id.rb_credit_account);


        et_cardNumber=(EditText) mLayout.findViewById(R.id.et_card_number);

        et_accountId=(EditText) mLayout.findViewById(R.id.et_account_id);
        et_accountName=(EditText) mLayout.findViewById(R.id.et_account_name);
        et_RoomNumber=(EditText) mLayout.findViewById(R.id.et_room_numb);
        tv_checkInDate=(EditText)mLayout.findViewById(R.id.tv_check_In);
radioCreditCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {mLayout.findViewById(R.id.ll_card_number).setVisibility(isChecked ? View.VISIBLE : View.GONE);

    }
});
        radioCreditAccount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mLayout.findViewById(R.id.ll_paymentmode).setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
        return mLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.setTitle(newBillNumber);
        rVBilling.setAdapter(billAdapter);
        setAmountDetails();
        this.setCancelable(true);
    }

    /**
     * Calculate and display total amunt in the bill
     */
    private void setAmountDetails() {
        ApplicationSingleton singleton = ApplicationSingleton.getInstance();
        double cost = billAdapter.getTotalAmount();
        //double cost =Double.parseDouble(Utilities.getDefaultCurrencyFormat(singleton.getTotalOrderCostTemp(), getContext()));
        double tax1 = Utilities.findTax1(getContext(), cost);
        double tax2 = Utilities.findTax2(getContext(), cost);
        double totCost = cost + tax1 + tax2;

        txtTotal.setText(Utilities.getDefaultCurrencyFormat(cost  , getContext()));
        txtTax1.setText(Utilities.getDefaultCurrencyFormat(tax1, getContext()));
        txtTax2.setText(Utilities.getDefaultCurrencyFormat(tax2 , getContext()));
        txtNetAmout.setText(Utilities.getDefaultCurrencyFormat(totCost , getContext()));

    }

    /**
     * Set items to be addded on the bills
     * @param billItems
     * @return
     */
    public BillFragmentDialog setBillItems(List<BillItem> billItems) {
        billAdapter = new BillingAdapter(billItems);

        return this;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(Utilities.getSharedPreferences(getActivity()).getBoolean(Constants.SHARED_PRE_UR_BILLING_ADDORSAVE, false))
            return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setPositiveButton("Save and Print Bill" , null)
                .setNegativeButton("Cancel", null)
                .create();
        else
            return new AlertDialog.Builder(getActivity())
                    .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
//                    .setPositiveButton("Save and Print Bill" , null)
                    .setNegativeButton("Cancel", null)
                    .create();
    }

    /**
     * Set a new Bill Number
     * @param newBillNumber
     * @return
     */
    public BillFragmentDialog setNewBillNumber(String newBillNumber) {
        this.newBillNumber = newBillNumber;
        return this;
    }
}

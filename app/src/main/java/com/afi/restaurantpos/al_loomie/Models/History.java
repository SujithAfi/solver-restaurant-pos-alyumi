package com.afi.restaurantpos.al_loomie.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by afi-mac-001 on 14/06/16.
 */
public class History {

    @SerializedName("Kot_Counter")
    private String counter;

    @SerializedName("Kot_Dt")
    private String kotDate;

    @SerializedName("Kot_No")
    private String kotNumber;

    @SerializedName("Kot_Time")
    private String kotTime;

    @SerializedName("Kot_TotAmt")
    private String totalAmount;

    @SerializedName("Tab_cd")
    private String table;

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getKotDate() {
        return kotDate;
    }

    public void setKotDate(String kotDate) {
        this.kotDate = kotDate;
    }

    public String getKotNumber() {
        return kotNumber;
    }

    public void setKotNumber(String kotNumber) {
        this.kotNumber = kotNumber;
    }

    public String getKotTime() {
        return kotTime;
    }

    public void setKotTime(String kotTime) {
        this.kotTime = kotTime;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}

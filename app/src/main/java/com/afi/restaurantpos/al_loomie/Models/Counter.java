package com.afi.restaurantpos.al_loomie.Models;

/**
 * Created by AFI on 11/4/2015.
 */
public class Counter {

    public String counter;
    public String name;
    public String type;


    public Counter setCounter(String counter) {
        this.counter = counter;
        return this;
    }

    public Counter setName(String name) {
        this.name = name;
        return this;
    }

    public Counter setType(String type) {
        this.type = type;
        return this;
    }
}

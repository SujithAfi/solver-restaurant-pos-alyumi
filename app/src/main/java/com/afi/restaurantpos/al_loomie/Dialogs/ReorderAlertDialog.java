package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afi.restaurantpos.al_loomie.Adapters.ReorderItemsAdapter;
import com.afi.restaurantpos.al_loomie.Models.KotContainer;
import com.afi.restaurantpos.al_loomie.Models.KotItem;
import com.afi.restaurantpos.al_loomie.Models.SelectedItemDetails;
import com.afi.restaurantpos.al_loomie.OrderDetails;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.KotSaveResponse;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKotItem;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Table;
import com.afi.restaurantpos.al_loomie.Utills.ApplicationSingleton;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Constants;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Cancel BILL or KOT dialog
 */
public class ReorderAlertDialog extends DialogFragment implements View.OnClickListener {

    private PendingKot pendingKot;
    private ViewGroup mContainer;
    private View mLayout;
    private CancelType cancelType;
    private ReorderKotDialogListner dialogListner;

    //View components
    private TextView tvCustomerName;
    private TextView tvKotNumber;
    private TextView tvDate;
    private TextView tvGuestNumber;
    private Button   btnRemoveGuest;
    private Button   btnAddGuest;
    private Spinner  spTableNumber;
    private int guestCount;
    private List<Table> tables;
    private ReorderItemsAdapter reorderKotAdapter;
    private RecyclerView rvItems;
    private String tableCode;
    private KotContainer kotContainer;
    private SharedPreferences sharedPreferences;
    List<SelectedItemDetails> reorderItems;
    private Handler uiHandler = new Handler();

    public interface ReorderKotDialogListner {
        void onFinishedAction(ReorderAlertDialog cancelBillAndKotDialog);
    }

    public static ReorderAlertDialog newInstance(CancelType cancelType) {
        ReorderAlertDialog fragment = new ReorderAlertDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.cancelType = cancelType;
        return fragment;
    }

    public ReorderAlertDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        sharedPreferences = Utilities.getSharedPreferences(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.kot_reorder_dialog, container, false);
        initializeViewComponents();
        return mLayout;
    }

    private void initializeViewComponents() {
        tvCustomerName = (TextView) mLayout.findViewById(R.id.tv_customer_name);
        tvKotNumber = (TextView) mLayout.findViewById(R.id.tv_kot_number);
        tvDate = (TextView) mLayout.findViewById(R.id.tv_date);
        tvGuestNumber = (TextView) mLayout.findViewById(R.id.tv_guest_num);
        btnRemoveGuest = (Button) mLayout.findViewById(R.id.btn_guest_down);
        btnAddGuest = (Button) mLayout.findViewById(R.id.btn_guest_up);
        spTableNumber = (Spinner) mLayout.findViewById(R.id.spinner_table_number);
        rvItems = (RecyclerView) mLayout.findViewById(R.id.rv_items);

        //set click listner to view components
        registerClickListner();

        List<String> datas = new ArrayList<>();

        for(Table table :  tables){

            datas.add(table.getCaption());

        }
        if(tables != null)
            tableCode = tables.get(0).getTab_Cd();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, datas);
        spTableNumber.setAdapter(adapter);

        spTableNumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    tableCode = tables.get(position).getTab_Cd();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        loadNewKotNo();
        populateMainDetails();
        getKotItems(pendingKot.Kot_No.replace("##" , ""));

    }

    private void populateMainDetails() {

        if(pendingKot != null){

            if(pendingKot.Cus_Name != null && !pendingKot.Cus_Name.matches("")) {
                tvCustomerName.setVisibility(View.VISIBLE);
                tvCustomerName.setText("Customer: " + pendingKot.Cus_Name);
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        tvDate.setText("Date: " + sdf.format(Calendar.getInstance().getTime()));

    }

    private void registerClickListner() {
        btnRemoveGuest.setOnClickListener(this);
        btnAddGuest.setOnClickListener(this);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setPositiveButton("OK" , null)
                .setNegativeButton("Cancel", null)
                .create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(false);
    }

    public ReorderAlertDialog setPendingKot(PendingKot pendingKot) {
        this.pendingKot = pendingKot;
        return this;
    }

    public ReorderAlertDialog setTables(List<Table> tables) {
        this.tables = tables;
        return this;
    }


    @Override
    public void onStart()
    {
        super.onStart();
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!Utilities.isNetworkConnected(getContext()))
                        Utilities.createNoNetworkDialog(getContext());
                    else {
                        if(validate())
                            saveKot();
                    }

                }
            });
        }
    }

    private boolean validate() {

        if(tableCode == null) {
            Toast.makeText(getActivity() , "Select a Table" , Toast.LENGTH_LONG).show();
            return false;
        }
        else if(Integer.parseInt(tvGuestNumber.getText().toString()) == 0){

            Toast.makeText(getActivity() , "Enter Valid Guest count" , Toast.LENGTH_LONG).show();
            return false;

        }

        return true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btn_guest_down:
                guestCount = Integer.parseInt(tvGuestNumber.getText().toString());
                if((guestCount)!=0) {
                    tvGuestNumber.setText(Integer.toString(guestCount - 1));
                }
                break;

            case R.id.btn_guest_up:
                guestCount = Integer.parseInt(tvGuestNumber.getText().toString());
                tvGuestNumber.setText(Integer.toString(guestCount+1));
                break;

            default:
                break;
        }

    }



    public ReorderAlertDialog setDialogListner(ReorderKotDialogListner dialogListner) {
        this.dialogListner = dialogListner;
        return this;
    }

    public void getKotItems(final String kotNo) {


        new Thread(new Runnable() {
            @Override
            public void run() {


                List<PendingKotItem> items = new ArrayList<PendingKotItem>();

                try {
                    if (getActivity() != null) {
                        final Response<List<PendingKotItem>> responseItems = Utilities.getRetrofitWebService(getActivity()).getKotItemsForReorder(kotNo).execute();

                        if (responseItems.body() != null) {
                            items.addAll(responseItems.body());
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }

                if (getActivity() != null)
                    populateItems(items);

            }
        }).start();

    }

    private void populateItems(List<PendingKotItem> kotItems) {


         reorderItems = new ArrayList<SelectedItemDetails>();
        for(PendingKotItem kotItem : kotItems){

            SelectedItemDetails details = new SelectedItemDetails();
            details.itemName = kotItem.Itm_Name;
            details.barcode = kotItem.Itm_Cd;
            details.itemCode = kotItem.Itm_Cd;
            details.Unit_Fraction = "1";
            details.Itm_UCost = kotItem.KotD_UCost;
            details.itemUnitCost = Double.parseDouble(kotItem.KotD_UCost);
            details.itemExtraRate = 0;
            details.itemTotalCost = 0;
            details.itemQuantity = Double.parseDouble(kotItem.KotD_Qty);
            details.modify = new ArrayList<>();
            details.Cm_Covers = "1";
            details.isSubitem = kotItem.is_Subitem;
            details.isMultiC = true;
            if(kotItem.KotD_Remarks != null && !kotItem.KotD_Remarks.matches(""))
                details.Remarks = kotItem.KotD_Remarks;
            else
                details.Remarks = "";
            details.Is_TempItem = true;
            details.itemTotalCost = Double.parseDouble(kotItem.KotD_Amt);
            reorderItems.add(details);

        }

        try {
            reorderKotAdapter = new ReorderItemsAdapter(getContext());
            reorderKotAdapter.setSelectedItemDetailses(kotItems);
            reorderKotAdapter.setItemChange(new ReorderItemsAdapter.onItemChange() {
                @Override
                public void onItemInvalidate() {
//                    caluculateCosts();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        uiHandler.post(new Runnable() {
            @Override
            public void run() {

                rvItems.setHasFixedSize(true);
                rvItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                rvItems.setAdapter(reorderKotAdapter);

            }
        });


    }

    public void saveKot() {

        try {
            if(reorderItems.isEmpty())
            {
                Utilities.showSnackBar("No Items" , getActivity());
                return;
            }
            switch (ApplicationSingleton.getInstance().getKotOrderType()){
                case NEW_KOT:
                    final MaterialDialog dialogSave = new MaterialDialog.Builder(getActivity())
                            .content(getResources().getString(R.string.saving_kot))
                            .progress(true, 0)
                            .cancelable(false)
                            .build();


                    if (!Utilities.isNetworkConnected(getActivity()))
                        Utilities.createNoNetworkDialog(getActivity());
                    else {
                        sharedPreferences = Utilities.getSharedPreferences(getActivity().getApplicationContext());
                        kotContainer = new KotContainer();
                        processItemDetaiils();

                        dialogSave.show();
                        Utilities.getRetrofitWebService(getActivity().getBaseContext()).saveKot(kotContainer).enqueue(new Callback<KotSaveResponse>() {
                            @Override
                            public void onResponse(Response<KotSaveResponse> response, Retrofit retrofit) {
                                dialogSave.dismiss();
                                KotSaveResponse kotSaveResponse = response.body();
                                if (kotSaveResponse.response_code) {
                                    new MaterialDialog.Builder(getActivity())
                                            .content(getResources().getString(R.string.kot_saved))
                                            .cancelable(false)
                                            .positiveText("Ok")
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                                    dismiss();
                                                    if (!(ApplicationSingleton.getInstance().getKotType() == Constants.ORDER_TYPE_DINE_IN)) {
                                                        Intent i = new Intent(getActivity(), OrderDetails.class);
                                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        startActivity(i);
                                                    }
                                                }
                                            })
                                            .build().show();
                                } else
                                    Utilities.showToast(getResources().getString(R.string.kot_not_saved), getActivity());
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                dialogSave.dismiss();
                            }
                        });

                    }

                    break;
                case MODIFY_KOT:// TODO: 10/19/2015
                    final MaterialDialog dialogModify = new MaterialDialog.Builder(getActivity())
                            .content(getResources().getString(R.string.modifying_kot))
                            .progress(true, 0)
                            .cancelable(false)
                            .build();


                    if (!Utilities.isNetworkConnected(getActivity()))
                        Utilities.createNoNetworkDialog(getActivity());
                    else {

                        dialogModify.show();
                        Utilities.getRetrofitWebService(getActivity().getApplicationContext()).updateKot(null).enqueue(new Callback<KotSaveResponse>() {
                            @Override
                            public void onResponse(Response<KotSaveResponse> response, Retrofit retrofit) {
                                dialogModify.cancel();

                                resetOrderDetails();

                                KotSaveResponse kotSaveResponse = response.body();
                                if (kotSaveResponse.response_code)
                                    Utilities.showToast(getResources().getString(R.string.kot_saved), getActivity());
                                else
                                    Utilities.showToast(getResources().getString(R.string.kot_not_saved), getActivity());
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                dialogModify.cancel();
                            }
                        });

                    }
                    break;
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void processItemDetaiils(){
        try {
            if(reorderItems != null){
                int i = 0;
                List<KotItem> kotItems = new ArrayList<>();
                for(SelectedItemDetails selectedItemDetails : reorderItems){
                    KotItem kotItem = new KotItem();
                    kotItem.setItm_Cd(selectedItemDetails.itemCode);
                    kotItem.setKotD_Qty(selectedItemDetails.itemQuantity + "");
                    kotItem.setKotD_Rate(selectedItemDetails.itemUnitCost + "");
                    kotItem.setKotD_Amt(selectedItemDetails.itemTotalCost + "");
                    kotItem.setKotD_UCost(selectedItemDetails.Itm_UCost);
                    kotItem.setKotD_Ctr(i++ + "");
                    kotItem.setKotD_Counter(sharedPreferences.getString(Constants.COUNTER_NAME , ""));
                    kotItem.setItm_Name(selectedItemDetails.itemName);
                    kotItem.setUnit_Fraction(selectedItemDetails.Unit_Fraction);
                    kotItem.setKot_MuUnit(selectedItemDetails.Unit_cd + "");
                    kotItem.setKotD_Remarks(selectedItemDetails.Remarks);
                    kotItem.setBarcode(selectedItemDetails.barcode);
                    kotItem.setMenu_SideDish("");
                    if(selectedItemDetails.modify != null){
                        for(int j = 0 ; j < selectedItemDetails.modify.size() ; j++){
                            String modifiers = selectedItemDetails.modify.get(j).name;
                            if( j == 0)
                                kotItem.setMenu_SideDish(modifiers);
                            else
                                kotItem.setMenu_SideDish(kotItem.getMenu_SideDish() + " , " + modifiers);
                        }
                    }
                    kotItem.setCmd_IsBuffet(selectedItemDetails.Cmd_IsBuffet);
                    kotItem.setMultic(selectedItemDetails.isMultiC);
                    kotItem.setIsSubitem(selectedItemDetails.isSubitem);
                    kotItems.add(kotItem);


                }
                kotContainer.setDetails(kotItems);
                kotContainer.setUsr_Id(sharedPreferences.getString(Constants.SHARED_PREF_KEY_USERNAME, ""));
                kotContainer.setKot_Counter(sharedPreferences.getString(Constants.COUNTER_NAME , ""));
                kotContainer.setKot_No("");
                kotContainer.setKot_Type(ApplicationSingleton.getInstance().getKotType() + "");
                try {
                    if(tables != null)
                        kotContainer.setTab_cd(tableCode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                kotContainer.setKot_TotAmt(ApplicationSingleton.getInstance().getGrandTotalWithoutTax() + "");
                kotContainer.setCus_Cd(pendingKot.Cus_Cd == null ? "" : this.pendingKot.Cus_Cd);
                kotContainer.setCus_Name(pendingKot.Cus_Name == null ? "" : pendingKot.Cus_Name);
                kotContainer.setSman_Cd(ApplicationSingleton.getInstance().getSalesman(getActivity().getApplicationContext()) == null ? "" : ApplicationSingleton.getInstance().getSalesman(getActivity().getApplicationContext()).Id);
                kotContainer.setSrvr_Cd("1");
                kotContainer.setKot_Covers(tvGuestNumber.getText().toString());
                kotContainer.setReservation(false);
                kotContainer.setResv_no("");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetOrderDetails() {

        ApplicationSingleton.getInstance().clearSelectedItemDetails();

    }

    private void loadNewKotNo() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    final Response<Map<String, String>> response = Utilities.getRetrofitWebService(getContext()).getNewKotNumber(sharedPreferences.getString(Constants.COUNTER_NAME , "")).execute();

                    if (response != null && response.body() != null) {
                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                onKotnoLoaded(response.body());

                            }
                        });


                    }

                } catch (Exception e) {
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
//                            progressDialog.dismiss();
                            Utilities.showSnackBar("Network error" , getActivity());
                        }
                    });
                }
            }
        }).start();

    }

    private void onKotnoLoaded(Map<String, String> kotNo) {

        tvKotNumber.setText("KOT: " + new DecimalFormat("##000000").format(Double.parseDouble(kotNo.get("kot_no"))));

    }
}

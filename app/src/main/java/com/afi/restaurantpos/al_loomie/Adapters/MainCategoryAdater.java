package com.afi.restaurantpos.al_loomie.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.afi.restaurantpos.al_loomie.CallBacks.ItemClickCallback;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.Maincategory;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

/**
 * Created by AFI on 10/2/2015.
 */
public class MainCategoryAdater extends RecyclerView.Adapter<MainCategoryAdater.MainCategoryViewHoulder> {

    private List<Maincategory> maincategories;
    private ItemClickCallback itemClickCallback;

    private int selectedPosition;
    private Context context;
    public MainCategoryAdater(List<Maincategory> maincategories , ItemClickCallback callback) {

        this.maincategories = maincategories;
        this.itemClickCallback = callback;
    }

    @Override
    public MainCategoryViewHoulder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.chilld_main_category, viewGroup, false);
        context = v.getContext();
        MainCategoryViewHoulder mainCategoryViewHoulder = new MainCategoryViewHoulder(v);
        return mainCategoryViewHoulder;
    }

    @Override
    public void onBindViewHolder(MainCategoryViewHoulder holder, int position) {
        holder.textView.setText(maincategories.get(position).Grp_Name);
        holder.textView.setOnClickListener(new ItemClickListner(position, maincategories.get(position).GrpCd));
        holder.textView.setSelected(position == selectedPosition);
    }

    @Override
    public int getItemCount() {
       if(maincategories == null)
           return 0;
        else
           return maincategories.size();
    }

    public static class MainCategoryViewHoulder extends RecyclerView.ViewHolder{

        private TextView textView;

        public MainCategoryViewHoulder(View itemView) {
            super(itemView);
            textView = (TextView)itemView.findViewById(R.id.txtName);
        }
    }

    private class ItemClickListner implements View.OnClickListener{
        private int position ;

        private String Id;

        public ItemClickListner(int position, String id) {
            this.position = position;
            Id = id;
        }

        @Override
        public void onClick(View v) {
            if (!Utilities.isNetworkConnected(context))
                Utilities.createNoNetworkDialog(context);
            else {
                if (itemClickCallback != null)
                    itemClickCallback.onItemSelected(position, Id);
                selectedPosition = position;
                notifyDataSetChanged();
            }
        }
    }

}

package com.afi.restaurantpos.al_loomie.Dialogs;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afi.restaurantpos.al_loomie.Models.CancelKotOrBill;
import com.afi.restaurantpos.al_loomie.R;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingBill;
import com.afi.restaurantpos.al_loomie.RetrofitModels.PendingKot;
import com.afi.restaurantpos.al_loomie.Utills.CancelType;
import com.afi.restaurantpos.al_loomie.Utills.Utilities;

import java.util.Map;


import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Cancel BILL or KOT dialog
 */
public class CancelBillAndKotDialog extends DialogFragment {

    private PendingKot pendingKot;
    private PendingBill pendingBill;
    private Toolbar toolbar;

    private ViewGroup mContainer;
    private View mLayout;

    private EditText edtTxtReason;

    private CancelType cancelType;

    private CancelBillAndKotDialogListner dialogListner;

    public interface CancelBillAndKotDialogListner {
        void onFinishedAction(CancelBillAndKotDialog cancelBillAndKotDialog);
    }

    public static CancelBillAndKotDialog newInstance(CancelType cancelType) {
        CancelBillAndKotDialog fragment = new CancelBillAndKotDialog();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.cancelType = cancelType;
        return fragment;
    }

    public CancelBillAndKotDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContainer = container;
        if (getShowsDialog()) {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        return getLayout(inflater, container);
    }

    private View getLayout(LayoutInflater inflater, ViewGroup container) {
        mLayout = inflater.inflate(R.layout.fragment_cancel_bill_and_kot_dialog, container, false);
        toolbar = (Toolbar)mLayout.findViewById(R.id.toolbar);
        edtTxtReason = (EditText) mLayout.findViewById(R.id.EditTextReason);
        switch (cancelType) {
            case CANCEL_BILL:
                toolbar.setTitle(R.string.cancel_bill);
                toolbar.setSubtitle(String.format("BILL number %s" , pendingBill.Cm_No.replace("##" , "")));
                break;
            case CANCEL_KOT:
                toolbar.setTitle(R.string.cancel_kot);
                toolbar.setSubtitle(String.format("KOT number %s", pendingKot.Kot_No.replace("##" , "")));
                break;
        }
        return mLayout;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setView(getLayout(LayoutInflater.from(getContext()), mContainer))
                .setPositiveButton("OK" , null)
                .setNegativeButton("Cancel", null)
                .create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setCancelable(false);
    }

    public CancelBillAndKotDialog setPendingKot(PendingKot pendingKot) {
        this.pendingKot = pendingKot;
        return this;
    }

    public CancelBillAndKotDialog setPendingBill(PendingBill pendingBill) {
        this.pendingBill = pendingBill;
        return this;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!Utilities.isNetworkConnected(getContext()))
                        Utilities.createNoNetworkDialog(getContext());
                    else
                        sendCancelAction();

                }
            });
        }
    }

    /**
     * Send the cancel action to previous activity
     */
    private void sendCancelAction() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(true);
        progressDialog.setIndeterminate(true);
        progressDialog.setTitle(String.format("Canceling %s", cancelType == CancelType.CANCEL_KOT ? "Kot" : "Bill"));
        progressDialog.setMessage(getContext().getResources().getString(R.string.please_wait));
        progressDialog.show();


        CancelKotOrBill cancelKotOrBill = new CancelKotOrBill();

        cancelKotOrBill.id = cancelType == CancelType.CANCEL_KOT ? pendingKot.Kot_No :  pendingBill.Cm_No ;

        cancelKotOrBill.reson = edtTxtReason.getText().toString();

        if ( cancelType == CancelType.CANCEL_KOT) {
            Utilities.getRetrofitWebService(getContext()).cancelKot(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
                @Override
                public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                    progressDialog.dismiss();
                    if (response != null) {
                        if ((Boolean) response.body().get("response_code")) {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.success)
                                    .setMessage(String.format(String.format("Canceled %s", cancelType == CancelType.CANCEL_KOT ? "Kot" : "Bill")))
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (dialogListner != null)
                                                dialogListner.onFinishedAction(CancelBillAndKotDialog.this);
                                        }
                                    })
                                    .create().show();
                        } else {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.error)
                                    .setMessage("Canot delete")
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (dialogListner != null)
                                                dialogListner.onFinishedAction(CancelBillAndKotDialog.this);
                                        }
                                    })
                                    .create().show();
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    progressDialog.dismiss();
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.error)
                            .setMessage("Network error")
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dialogListner != null)
                                        dialogListner.onFinishedAction(CancelBillAndKotDialog.this);
                                }
                            })
                            .create().show();
                }
            });
        }
        else if (cancelType == CancelType.CANCEL_BILL){

            Utilities.getRetrofitWebService(getContext()).cancelBill(cancelKotOrBill).enqueue(new Callback<Map<String, Object>>() {
                @Override
                public void onResponse(Response<Map<String, Object>> response, Retrofit retrofit) {
                    progressDialog.dismiss();
                    if (response != null) {
                        if ((Boolean) response.body().get("response_code")) {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.success)
                                    .setMessage(String.format(String.format("Canceled %s", cancelType == CancelType.CANCEL_KOT ? "Kot" : "Bill")))
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (dialogListner != null)
                                                dialogListner.onFinishedAction(CancelBillAndKotDialog.this);
                                        }
                                    })
                                    .create().show();
                        } else {
                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.error)
                                    .setMessage("Canot delete")
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (dialogListner != null)
                                                dialogListner.onFinishedAction(CancelBillAndKotDialog.this);
                                        }
                                    })
                                    .create().show();
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    progressDialog.dismiss();
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.error)
                            .setMessage("Network error")
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (dialogListner != null)
                                        dialogListner.onFinishedAction(CancelBillAndKotDialog.this);
                                }
                            })
                            .create().show();
                }
            });

        }

    }

    public CancelBillAndKotDialog setDialogListner(CancelBillAndKotDialogListner dialogListner) {
        this.dialogListner = dialogListner;
        return this;
    }
}

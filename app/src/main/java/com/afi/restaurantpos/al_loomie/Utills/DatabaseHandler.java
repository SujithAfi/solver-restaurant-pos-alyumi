package com.afi.restaurantpos.al_loomie.Utills;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import com.afi.restaurantpos.al_loomie.RetrofitModels.Customer;

/**
 * Created by AFI on 11/2/2015.
 * Class to manage all database related operations
 */
public class DatabaseHandler extends SQLiteOpenHelper {


    private static final String TABLE_CUSTOMERS = "customers";

    /**
     * Custructor to initilize the DatabaseHandler class
     * @param context current application context
     */
    public DatabaseHandler(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.CURRENT_DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createCustomerQuery = "CREATE TABLE " + TABLE_CUSTOMERS  + "( " +
                "Id TEXT, " +
                "Name TEXT, " +
                "Address1 TEXT, " +
                "Discount TEXT, " +
                "Address2 TEXT, " +
                "Distance TEXT )";
        db.execSQL(createCustomerQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMERS );
        this.onCreate(db);
    }

    /**
     * Add a list of customers to the database
     * @param customers Generic List of customers
     */
    public void addToCustomers(List<Customer> customers){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        if(customers != null)
            if( customers.size() > 0 ) {
                sqLiteDatabase.execSQL("delete from " + TABLE_CUSTOMERS);
                synchronized (this) {
                    for (Customer customer : customers) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("Id", customer.getId());
                        contentValues.put("Name", customer.getName());
                        contentValues.put("Address1", customer.getAddress1());
                        contentValues.put("Discount", customer.getDiscount() + "");
                        contentValues.put("Address2", customer.getAddress2());
                        contentValues.put("Distance", customer.getDistance() + "");
                        sqLiteDatabase.insert(TABLE_CUSTOMERS, null, contentValues);
                    }
                }
            }
        sqLiteDatabase.close();
    }

    /**
     *
     * Returns the list of customers stored on the databse
     * @return Generic List of customers
     */
    public List<Customer> getCustomers(){
        List<Customer> customers = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        synchronized (this) {
            Cursor cursor = sqLiteDatabase.query(TABLE_CUSTOMERS , null , null , null , null , null , null);
            if(cursor.moveToFirst()) {
                do {
                    Customer customer = new Customer();
                    customer.setId(cursor.getString(cursor.getColumnIndex("Id")));
                    customer.setName(cursor.getString(cursor.getColumnIndex("Name")));
                    customer.setAddress1(cursor.getString(cursor.getColumnIndex("Address1")));
                    customer.setAddress2(cursor.getString(cursor.getColumnIndex("Address2")));
                    try {

                        customer.setDiscount(Double.parseDouble(cursor.getString(cursor.getColumnIndex("Discount"))));
                        customer.setDistance(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Distance"))));
                    }
                    catch (Exception e) {
                    }
                    customers.add(customer);
                }
                while (cursor.moveToNext());
            }
        }
        return customers;
    }

    /**
     * Get customer from customer id
     * @param id id of the customer
     * @return Customer object with id
     */
    public Customer getCustomerById(String id){
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        synchronized (this) {
            Cursor cursor = sqLiteDatabase.query(TABLE_CUSTOMERS , null , "Id = ?" , new String[]{id} , null , null , null);
            if(cursor.moveToFirst()) {
                Customer customer = new Customer();
                customer.setId(cursor.getString(cursor.getColumnIndex("Id")));
                customer.setName(cursor.getString(cursor.getColumnIndex("Name")));
                customer.setAddress1(cursor.getString(cursor.getColumnIndex("Address1")));
                customer.setAddress2(cursor.getString(cursor.getColumnIndex("Address2")));
                try {

                    customer.setDiscount(Double.parseDouble(cursor.getString(cursor.getColumnIndex("Id"))));
                    customer.setDistance(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Distance"))));
                }
                catch (Exception e) {
                }
                return customer;
            }
            else {
                return null;
            }
        }
    }
}
